<?php

apc_clear_cache();
  apc_clear_cache('user');
  apc_clear_cache('opcode');
// Include Magento
require_once dirname(__FILE__).'/app/Mage.php';
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
// Set user admin session
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);
// Call Magento clean cache action
Mage::app()->cleanCache();
// Enable all cache types
$enable = array();
foreach(Mage::helper('core')->getCacheTypes() as $type => $label){
    $enable[$type] = 1;
}
Mage::app()->saveUseCache($enable);
// Refresh cache's
echo 'Refreshing cache...';