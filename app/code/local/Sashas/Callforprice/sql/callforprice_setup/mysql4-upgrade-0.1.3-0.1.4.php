<?php
/**
 * @author		Sashas
 * @category    Sashas
 * @package     Sashas_Callforprice
 * @copyright   Copyright (c) 2013 Sashas IT Support Inc. (http://www.sashas.org)
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)

 */

$installer = $this;
$installer->startSetup();		 
$installer->getConnection()->addColumn($installer->getTable('sashas_callforprice_product'), 'customer_groups', 'varchar(255) NOT NULL DEFAULT -1');
$installer->endSetup();