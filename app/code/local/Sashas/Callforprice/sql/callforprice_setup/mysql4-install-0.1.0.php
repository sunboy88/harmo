<?php
/**
 * @author		Sashas
 * @category    Sashas
 * @package     Sashas_Callforprice
 * @copyright   Copyright (c) 2013 Sashas IT Support Inc. (http://www.sashas.org)
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)

 */

$installer = $this;

$installer->startSetup();

$installer->run("
		CREATE TABLE IF NOT EXISTS  {$this->getTable('sashas_callforprice_product')} (
		`value_id` int(11) unsigned NOT NULL auto_increment,
		`product_id`  int(11) NOT NULL,
		`value` varchar(255) NOT NULL,
		`status` smallint(6) NOT NULL default '0',
		`addtocart_enabled` smallint(6) NOT NULL default '0',		 
		`update_time` TIMESTAMP  DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`value_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
		 

$installer->endSetup();