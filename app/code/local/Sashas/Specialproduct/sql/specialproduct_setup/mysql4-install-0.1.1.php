<?php
/**
 * @author		Sashas
 * @category    Sashas
 * @package     Sashas_Specialproduct
 * @copyright   Copyright (c) 2013 Sashas IT Support Inc. (http://www.sashas.org)
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)

 */

$installer = $this;
$installer->startSetup();

 
$attributeCode='special_product_image';
 
$installer->addAttribute( Mage_Catalog_Model_Product::ENTITY, $attributeCode, array(
    'group'         => 'General',
    'input'         => 'select',
    'type'          => 'int',
    'label'         => 'Product Special Image',
	'sort_order' => 1000,
    'visible'       => true,
    'required'      => false,
    'user_defined' => true,
    'searchable' => false,
    'filterable' => false,
    'comparable'    => false,
    'visible_on_front' => false,
	'used_in_product_listing' => true,
    'visible_in_advanced_search'  => false,
    'is_html_allowed_on_front' => true, 	
	'option'     	=> array('values' => array('deal.png', 'special.png')) ,			 
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
)); 
 
//$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY,$attributeCode,'used_in_product_listing',1);

$model = Mage::getModel('catalog/resource_eav_attribute');
$model_attr=Mage::getModel('eav/entity_setup','core_setup');
 
$allAttributeSetIds=$model->getAllAttributeSetIds('catalog_product');
$attribute=$model->getAttribute('catalog_product',$attributeCode);
 

foreach ($allAttributeSetIds as $attributeSetId) {
	try{
			$attributeGroupId=$model_attr->getAttributeGroup('catalog_product',$attributeSetId,'General');
	}
	catch(Exception $e) {
 		 $attributeGroupId=$model_attr->getDefaultArrtibuteGroupId('catalog/product',$attributeSetId);
	}
	 $model_attr->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId,$attribute);
}
                            
$installer->endSetup();