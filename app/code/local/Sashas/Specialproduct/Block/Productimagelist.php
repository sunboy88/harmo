<?php
/**
 * @author		Sashas
 * @category    Sashas
 * @package     Sashas_Specialproduct
 * @copyright   Copyright (c) 2013 Sashas IT Support Inc. (http://www.sashas.org)
 * @license     http://opensource.org/licenses/GPL-3.0  GNU General Public License, version 3 (GPL-3.0)

 */

class Sashas_Specialproduct_Block_Productimagelist extends Mage_Catalog_Block_product_List {
	
	public function __construct()
	{
		$this->setTemplate('specialproduct/list.phtml');
	}
 
}