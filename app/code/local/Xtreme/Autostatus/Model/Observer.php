<?php

class Xtreme_Autostatus_Model_Observer
{
    /**
     * Mage::dispatchEvent($this->_eventPrefix.'_save_after', $this->_getEventData());
     * protected $_eventPrefix = 'sales_order';
     * protected $_eventObject = 'order';
     * event: sales_order_save_after
     */
    public $log = "autoinvoice.log";
    
    public function automaticallyPendingShipOrder($observer)
    {
        Mage::log("START -----------", null, $this->log);
        
        //$invoice = $observer->getEvent()->getInvoice();
        //$order = $invoice->getOrder();
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getId();
        Mage::log("orderId: ${orderId}", null, $this->log);
	Mage::log($order->getIncrementId(), null, $this->log);
	Mage::log("current state: ".$order->getState(), null, $this->log);
	Mage::log("current status: ".$order->getStatus(), null, $this->log);
	try{
	    if(!$order->getId())
	    {
    		Mage::log("Error: No order", null, $this->log);
    		Mage::log("END -----------", null, $this->log);
	        return $this;
	    }
	    /*if($order->getPayment()->getMethod() == "paypal_express")
	    {
    		Mage::log("Error: payment method is PayPal", null, $this->log);
    		Mage::log("END -----------", null, $this->log);
	        return $this;
	    }*/
	}
	catch(Exception $e){
		Mage::log($e->getMessage());
		return $this;	
	}
	
	Mage::log($order->getPayment()->getMethod(), null, $this->log);

        if ($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING
				 && $order->getStatus() == 'processing'	
				 && $order->canShip() 
				 && $order->getTotalDue() == 0) 
		{
			$comment = 'Changed status to Pending Shipping.';
			Mage::log($comment, null, $this->log);
			$order->addStatusHistoryComment($comment, "pending_shipping")
				  ->setIsVisibleOnFront(1)
				  ->setIsCustomerNotified(1);
			$order->save();
			$order->sendOrderUpdateEmail(1, $comment);			
		}
		if(!$order->canShip())
		    Mage::log("Order can not ship", null, $this->log);
		    
		Mage::log("current state: ".$order->getState(), null, $this->log);
		Mage::log("current status: ".$order->getStatus(), null, $this->log);
    		Mage::log("END -----------", null, $this->log);
		return $this;
    }
}