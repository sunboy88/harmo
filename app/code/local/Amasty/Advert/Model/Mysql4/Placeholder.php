<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Model_Mysql4_Placeholder extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('amadvert/placeholder', 'entity_id');
    }
    
    /**
    * Remove all assigned adverts
    * 
    * @param integer $placeholderId
    */
    public function clearAdverts($placeholderId)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $sql = 'DELETE FROM `' . $this->getTable('amadvert/advert_place') . '` WHERE `placeholder_id` = "' . $placeholderId . '" ' ;
        $connection->query($sql);
    }
    
    /**
    * Assign array of adverts to specified placeholder
    * 
    * @param integer $placeholderId
    * @param array $advertIds
    */
    public function assignAdverts($placeholderId, $advertIds)
    {
        foreach ($advertIds as $advertId) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $sql = ' INSERT INTO `' . $this->getTable('amadvert/advert_place') . '` (`advert_id`, `placeholder_id`) VALUES ("' . $advertId . '", "' . $placeholderId . '") ' ;
            $connection->query($sql);
        }
    }
    
    public function getAdverts($placeholderId)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = ' SELECT advert_id FROM `' . $this->getTable('amadvert/advert_place') . '` WHERE `placeholder_id` = "' . $placeholderId . '" ' ;
        $advertIds = $connection->fetchCol($sql);
        return $advertIds;
    }
    
    public function getAdvertId($placeholderId, $loadLogic = Amasty_Advert_Model_Placeholder::LOGIC_LOAD_RANDOM)
    {
        $adverts  = $this->getAdverts($placeholderId);
        if (empty($adverts))
        {
            return 0;
        }
        switch ($loadLogic)
        {
            case Amasty_Advert_Model_Placeholder::LOGIC_LOAD_RANDOM:
                $advertId = $adverts[rand(0, count($adverts) - 1)];
            break;
            default:
                $advertId = $adverts[0];
            break;
        }
        return $advertId;
    }
}