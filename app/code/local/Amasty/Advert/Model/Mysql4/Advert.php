<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Model_Mysql4_Advert extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('amadvert/advert', 'entity_id');
    }
    
    public function clearPlacedAdverts($advertId)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $sql = 'DELETE FROM `' . $this->getTable('amadvert/advert_place') . '` WHERE `advert_id` = "' . $advertId . '" ' ;
        $connection->query($sql);
    }
}