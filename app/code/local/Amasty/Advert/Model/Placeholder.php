<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Model_Placeholder extends Mage_Core_Model_Abstract
{
    const LOGIC_LOAD_RANDOM = 1;
    
    protected function _construct()
    {
        $this->_init('amadvert/placeholder');
    }
    
    protected function _afterSave()
    {
        parent::_afterSave();
        
        /**
        * Saving related adverts, if any selected
        */
        if ($this->getData('should_save_selected_adverts'))
        {
            $this->getResource()->clearAdverts($this->getId());
            if ($this->getSelectedAdverts())
            {
                $advertIds = explode('&', $this->getSelectedAdverts());
                if (!is_array($advertIds)) {
                    $advertIds = array($advertIds);
                }
                $this->getResource()->assignAdverts($this->getId(), $advertIds);
            }
        }
        
        return $this;
    }
    
    public function getAdverts()
    {
        return $this->getResource()->getAdverts($this->getId());
    }
    
    public function populateContent()
    {
        $advertId = $this->getResource()->getAdvertId($this->getId(), self::LOGIC_LOAD_RANDOM);
        if ($advertId)
        {
            $advert = Mage::getModel('amadvert/advert')->load($advertId);
            if ($advert->getId())
            {
                $processor = Mage::helper('cms')->getPageTemplateProcessor();
                $html = $processor->filter($advert->getContent());
                $this->setAdvertContent($html);
                $this->setAdvertId($advertId);
            }
        }
        return $this;
    }
    
    /**
    * Will check if we should display this placeholder on the current page.
    */
    public function shouldDisplay()
    {
        // first checking store view
        $storeIdsAllowed = explode(',', $this->getStoreIds());
        if ($storeIdsAllowed)
        {
            if (!in_array(0, $storeIdsAllowed)) // 0 means All Store Views, so if 0, we should allow
            {
                $currentStoreId = Mage::app()->getStore()->getId();
                if (!in_array($currentStoreId, $storeIdsAllowed))
                {
                    return false;
                }
            }
        }
        
        // checking category condition for placeholders that are assigned to category
        if (Mage::helper('amadvert')->checkConditionType($this->getPlace(), 'category'))
        {
            if (Mage::registry('current_category') && !Mage::registry('curent_product'))
            {
                $categoriesAllowed = explode(',', $this->getCategoryIds());
                if (in_array(Mage::registry('current_category')->getId(), $categoriesAllowed))
                {
                    return true;
                }
            }
            // we are not on a category page, or category does not match
            return false;
        }
        
        return true;
    }
}
