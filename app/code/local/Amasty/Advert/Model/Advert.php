<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Model_Advert extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('amadvert/advert');
    }
    
    protected function _beforeSave()
    {
        if (!$this->getId())
        {
            $this->setCreatedAt(date('Y-m-d H:i:s', Mage::app()->getLocale()->date()->get()));
        }
        $this->setUpdatedAt(date('Y-m-d H:i:s', Mage::app()->getLocale()->date()->get()));
        
        return parent::_beforeSave();
    }
    
    protected function _beforeDelete()
    {
        parent::_beforeDelete();
        $this->getResource()->clearPlacedAdverts($this->getId());
        return $this;
    }
}
