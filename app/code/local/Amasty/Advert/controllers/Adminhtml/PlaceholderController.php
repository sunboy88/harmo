<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Adminhtml_PlaceholderController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/amadvert')
            ->_addBreadcrumb(Mage::helper('amadvert')->__('CMS'),       Mage::helper('amadvert')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('amadvert')->__('Placeholders'), Mage::helper('amadvert')->__('Placeholders'))
        ;
        return $this;
    }
    
    public function indexAction()
    {
        $this->_title($this->__('Placeholders'));
             
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('amadvert/adminhtml_placeholder'))
            ->renderLayout();
    }
    
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }
    
    protected function _initPlaceholder()
    {
        $id   = $this->getRequest()->getParam('placeholder_id');
        $placeholder = Mage::getModel('amadvert/placeholder');
        if ($id) 
        {
            $placeholder->load($id);
            if (!$placeholder->getId()) 
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amadvert')->__('This placeholder no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        Mage::register('amadvert_placeholder', $placeholder);
    }
    
    public function editAction()
    {
        $this->_title($this->__('Placeholders'))->_title($this->__('Edit Placeholder'));
        
        $this->_initPlaceholder();
        
        // Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) 
        {
            $placeholder->setData($data);
        }
             
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit'))
            ->_addLeft($this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tabs'))
            ->renderLayout();
    }
    
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) 
        {
            if (isset($data['selected_adverts']))
            {
                $data['should_save_selected_adverts'] = true;
            }
            $placeholderId = $this->getRequest()->getParam('placeholder_id');
            $model  = Mage::getModel('amadvert/placeholder');
            if ($placeholderId) 
            {
                $model->load($placeholderId);
            }
            
            if (isset($data['stores'])) {
                $data['store_ids'] = implode(',', $data['stores']);
            }
            
            if (isset($data['category_ids'])) {
                $data['category_ids'] = explode(',', $data['category_ids']);
                if (is_array($data['category_ids'])) {
                    foreach ($data['category_ids'] as $i => $categoryId) {
                        if (!$categoryId) {
                            unset($data['category_ids'][$i]);
                        }
                    }
                    $data['category_ids'] = array_unique($data['category_ids']);
                }
                $data['category_ids'] = implode(',', $data['category_ids']);
            }
            
            $model->setData($data);
            
            try {
                $model->save();
                $placeholderId = $model->getId();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amadvert')->__('The placeholder has been saved.'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                $this->_redirect('*/*/');
                return;
                
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('amadvert')->__('An error occurred while saving the placeholder: ') . $e->getMessage());
            }
            
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('placeholder_id' => $placeholderId));
            return;
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('placeholder_id')) 
        {
            try 
            {
                $model = Mage::getModel('amadvert/placeholder');
                $model->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amadvert')->__('The placeholder has been deleted.'));
                $this->_redirect('*/*/');
                return;
                
            } catch (Exception $e) 
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('placeholder_id' => $id));
                return;
            }
        }
    }
    
    public function categoriesJsonAction()
    {
        $this->_initPlaceholder();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_config_category')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }
    
    public function advertsGridAction()
    {
        $this->_initPlaceholder();
        
        $grid = $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_adverts')->setSelectedAdverts($this->getRequest()->getPost('selected_adverts', null));
        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($grid, 'getSelectedAdvertBlocks', 'selected_adverts', 'selected_adverts');
        
        $this->getResponse()->setBody(
            $grid->toHtml() . $serializer->toHtml()
        );
    }
}
