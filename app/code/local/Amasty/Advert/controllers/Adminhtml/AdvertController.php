<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Adminhtml_AdvertController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/amadvert')
            ->_addBreadcrumb(Mage::helper('amadvert')->__('CMS'),       Mage::helper('amadvert')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('amadvert')->__('Adverts'), Mage::helper('amadvert')->__('Adverts'))
        ;
        return $this;
    }
    
    public function indexAction()
    {
        $this->_title($this->__('Adverts'));
             
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('amadvert/adminhtml_advert'))
            ->renderLayout();
    }
    
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $this->_title($this->__('Adverts'))->_title($this->__('Edit Advert'));
        
        $id   = $this->getRequest()->getParam('advert_id');
        $advert = Mage::getModel('amadvert/advert');
        if ($id) 
        {
            $advert->load($id);
            if (!$advert->getId()) 
            {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amadvert')->__('This advert no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        
        // Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) 
        {
            $advert->setData($data);
        }
        
        Mage::register('amadvert_advert', $advert);
             
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('amadvert/adminhtml_advert_edit'))
            ->_addLeft($this->getLayout()->createBlock('amadvert/adminhtml_advert_edit_tabs'))
            ->renderLayout();
    }
    
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) 
        {
            $advertId = $this->getRequest()->getParam('advert_id');
            $model  = Mage::getModel('amadvert/advert');
            if ($advertId) 
            {
                $model->load($advertId);
            }
            $model->setData($data);
            try 
            {
                $model->save();
                $advertId = $model->getId();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amadvert')->__('The advert has been saved.'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                $this->_redirect('*/*/');
                return;
                
            } catch (Exception $e) 
            {
                $this->_getSession()->addException($e, Mage::helper('amadvert')->__('An error occurred while saving the advert: ') . $e->getMessage());
            }
            
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('advert_id' => $advertId));
            return;
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('advert_id')) 
        {
            try 
            {
                $model = Mage::getModel('amadvert/advert');
                $model->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('amadvert')->__('The advert has been deleted.'));
                $this->_redirect('*/*/');
                return;
                
            } catch (Exception $e) 
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('advert_id' => $id));
                return;
            }
        }
    }
}
