<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Advert_Component extends Mage_Core_Block_Template
{
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        
        // template depends on place type
        if (Mage::helper('amadvert')->checkConditionType($this->getPlaceholder()->getPlace(), 'fullscreen')
            || Mage::helper('amadvert')->checkConditionType($this->getPlaceholder()->getPlace(), 'fullscreen-precheckout'))
        {
            $this->setTemplate('amadvert/advert/fullscreen.phtml');
            return $this;
        }
        
        // this is default template
        $this->setTemplate('amadvert/advert/component.phtml');
        
        return $this;
    }
    
    protected function _toHtml()
    {
        if (Mage::helper('amadvert')->checkConditionType($this->getPlaceholder()->getPlace(), 'fullscreen'))
        {
            // checking if we should display fullscreen block only on the homepage
            if ($this->getPlaceholder()->getFullscreenOnlyhome())
            {
                $page = Mage::app()->getFrontController()->getRequest()->getRouteName();
                if (!($page == 'cms' && 'home' == Mage::getSingleton('cms/page')->getIdentifier()))
                {
                    return '';
                }
            }
        }
        
        if (Mage::helper('amadvert')->checkConditionType($this->getPlaceholder()->getPlace(), 'fullscreen')
            || Mage::helper('amadvert')->checkConditionType($this->getPlaceholder()->getPlace(), 'fullscreen-precheckout'))
        {
            // checking if we should display fullscreen block only once per visitor session
            if ($this->getPlaceholder()->getFullscreenOnlyonce())
            {
                $session   = Mage::getModel('core/session');
                $displayed = $session->getPlaceholdersDisplayed();
                if (is_array($displayed) && in_array($this->getPlaceholder()->getId(), $displayed))
                {
                    return '';
                }
                $displayed[] = $this->getPlaceholder()->getId();
                $session->setPlaceholdersDisplayed($displayed);
            }
        }
        
        // tracking block view
        if ($this->getPlaceholder()->getAdvertId())
        {
            $advert = Mage::getModel('amadvert/advert')->load($this->getPlaceholder()->getAdvertId());
            if ($advert->getId())
            {
                $advert->setViews($advert->getViews() + 1)->save();
            }
        }
        
        return parent::_toHtml();
    }
}