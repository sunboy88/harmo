<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Placeholder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'amadvert';
        $this->_controller = 'adminhtml_placeholder';
        $this->_headerText = Mage::helper('amadvert')->__('Placeholders');
        parent::__construct();
        $this->_updateButton('add', 'label', Mage::helper('amadvert')->__('Add New Placeholder'));
    }
}