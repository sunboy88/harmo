<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Placeholder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'placeholder_id';
        $this->_blockGroup = 'amadvert';
        $this->_controller = 'adminhtml_placeholder';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('amadvert')->__('Save Placeholder'));
        $this->_updateButton('delete', 'label', Mage::helper('amadvert')->__('Delete Placeholder'));
        
        $place = Mage::helper('amadvert')->getCurrentPlace();
        if (!$place || !in_array($place, array_keys(Mage::helper('amadvert')->getPlaceholderPlaces()))) {
            $this->_removeButton('save');
        }
        
        $this->_formScripts[] = '
            var templateSyntax = /(^|.|\r|\n)({{(\w+)}})/;
            function setSettings(urlTemplate, placeElement) {
                var template = new Template(urlTemplate, templateSyntax);
                setLocation(template.evaluate({place:$F(placeElement)}));
            }
            function saveAndContinueEdit() {
                editForm.submit($(\'edit_form\').action+\'back/edit/\');
            }
        ';
    }
    
    public function getHeaderText()
    {
        if (Mage::registry('amadvert_placeholder')->getId()) {
            return Mage::helper('amadvert')->__("Edit Placeholder '%s'", $this->htmlEscape(Mage::registry('amadvert_placeholder')->getTitle()));
        }
        else {
            return Mage::helper('amadvert')->__('New Placeholder');
        }
    }
}