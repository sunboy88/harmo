<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Placeholder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amadvertPlaceholderGrid');
        $this->setDefaultSort('alias');
        $this->setDefaultDir('ASC');
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amadvert/placeholder')->getCollection();
        /* @var $collection Amasty_Advert_Model_Mysql4_Placeholder_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('title', array(
            'header'    => Mage::helper('amadvert')->__('Placeholder Title'),
            'index'     => 'title',
        ));
        
        $this->addColumn('alias', array(
            'header'    => Mage::helper('amadvert')->__('Alias'),
            'index'     => 'alias',
        ));
        
        $this->addColumn('place', array(
            'header'    => Mage::helper('amadvert')->__('Position'),
            'sortable'  => true,
            'index'     => 'place',
            'type'      => 'options',
            'options'   => Mage::helper('amadvert')->getPlaceholderPlaces(true),
        ));
        
        $this->addColumn('is_active', array(
            'header'    => Mage::helper('amadvert')->__('Enabled'),
            'sortable'  => true,
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                '1' => Mage::helper('amadvert')->__('Yes'),
                '0' => Mage::helper('amadvert')->__('No'),
            ),
            'align' => 'center',
            'width' => '120px',
        ));

        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('placeholder_id' => $row->getId()));
    }
}