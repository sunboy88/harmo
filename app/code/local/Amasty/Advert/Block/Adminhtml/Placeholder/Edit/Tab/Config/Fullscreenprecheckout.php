<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Placeholder_Edit_Tab_Config_Fullscreenprecheckout extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Amasty_Adverts_Model_Advert */
        $model = Mage::registry('amadvert_placeholder');
        
        if (!Mage::app()->isSingleStoreMode()) {
            $model->setData('stores', explode(',', $model->getStoreIds()));
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('advert_');

        $fieldset = $form->addFieldset('fullscreen_fieldset', array('legend'=>Mage::helper('amadvert')->__('Fullscreen Pre-Checkout Configuration')));

        $yn = array(
            array(
                'value' => '1',
                'label' => $this->__('Yes'),
            ),
            array(
                'value' => '0',
                'label' => $this->__('No'),
            ),
        );
        
        $fieldset->addField('fullscreen_onlyonce', 'select', array(
            'name'      => 'fullscreen_onlyonce',
            'label'     => Mage::helper('amadvert')->__('Display Once Per Visitor'),
            'title'     => Mage::helper('amadvert')->__('Display Once Per Visitor'),
            'note'      => Mage::helper('amadvert')->__('If set to "Yes", advertisement will appear at the first checkout for current user session.'),
            'values'    => $yn,
        ));
        
        $fieldset->addField('fullscreen_effect', 'text', array(
            'name'      => 'fullscreen_effect',
            'label'     => Mage::helper('amadvert')->__('Effect Duration'),
            'title'     => Mage::helper('amadvert')->__('Effect Duration'),
            'note'      => Mage::helper('amadvert')->__('In seconds. Can be decimal. Set to 0 for instant display.'),
        ));
        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    public function getTabLabel()
    {
        return Mage::helper('amadvert')->__('Configuration: Fullscreen Pre-Checkout');
    }
    
    public function getTabTitle()
    {
        return Mage::helper('amadvert')->__('Configuration: Fullscreen Pre-Checkout');
    }
    
    public function canShowTab()
    {
        return true;
    }
    
    public function isHidden()
    {
        return false;
    }
}