<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Placeholder_Edit_Tab_Adverts extends Mage_Adminhtml_Block_Widget_Grid implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('placeholder_adverts_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        /*if ($this->_getPlaceholder() && $this->_getPlaceholder()->getId()) {
            $this->setDefaultFilter(array('in_adverts' => 1));
        }*/
    }
    
    protected function _getPlaceholder()
    {
        return Mage::registry('amadvert_placeholder');
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amadvert/advert')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('in_adverts', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'in_adverts',
            'values'            => $this->_getSelectedAdverts(),
            'align'             => 'center',
            'index'             => 'entity_id'
        ));
        
        $this->addColumn('advert_title', array(
            'header'    => Mage::helper('amadvert')->__('Advert Title'),
            'index'     => 'title',
        ));
        
        $this->addColumn('advert_is_active', array(
            'header'    =>Mage::helper('amadvert')->__('Enabled'),
            'sortable'  =>true,
            'index'     =>'is_active',
            'type'      => 'options',
            'options' => array(
                '1' => Mage::helper('amadvert')->__('Yes'),
                '0' => Mage::helper('amadvert')->__('No'),
            ),
            'align' => 'center',
            'width' => '120px',
        ));
        
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('amadvert')->__('Last Modified'),
            'index'     => 'updated_at',
        ));
        
        return parent::_prepareColumns();
    }
    
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in adverts flag
        if ($column->getId() == 'in_adverts') {
            $productIds = $this->_getSelectedAdverts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$productIds));
            } else {
                if($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
    
    public function getGridUrl()
    {
        return $this->getUrl('*/*/advertsGrid', array('_current'=>true));
    }
    
    /**
     * Retrieve selected adverts
     *
     * @return array
     */
    protected function _getSelectedAdverts()
    {
        $adverts = $this->getSelectedAdverts();
        if (!is_array($adverts)) {
            $adverts = $this->getSelectedAdvertBlocks();
        }
        return $adverts;
    }
    
    public function getSelectedAdvertBlocks()
    {
        return $this->_getPlaceholder()->getAdverts();
    }
    
    public function getTabLabel()
    {
        return Mage::helper('amadvert')->__('Active Adverts');
    }
    
    public function getTabTitle()
    {
        return Mage::helper('amadvert')->__('Active Adverts');
    }
    
    public function canShowTab()
    {
        return true;
    }
    
    public function isHidden()
    {
        return false;
    }
}