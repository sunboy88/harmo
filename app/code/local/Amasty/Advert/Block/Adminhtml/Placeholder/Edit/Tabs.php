<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Placeholder_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amadvert_placeholder_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('amadvert')->__('Placeholder Information'));
    }
    
    protected function _beforeToHtml()
    {
        $place = Mage::helper('amadvert')->getCurrentPlace();
        if (!$place || !in_array($place, array_keys(Mage::helper('amadvert')->getPlaceholderPlaces())))
        {
            $this->addTab('form_section', array(
                'label'     => Mage::helper('amadvert')->__('Settings'),
                'title'     => Mage::helper('amadvert')->__('Settings'),
                'content'   => $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_settings')->toHtml(),
            ));
        } else 
        {
            $this->addTab('form_section', array(
                'label'     => Mage::helper('amadvert')->__('General Information'),
                'title'     => Mage::helper('amadvert')->__('General Information'),
                'content'   => $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_main')->toHtml(),
            ));
            
            $this->addTab('adverts_section', array(
                'label'     => Mage::helper('amadvert')->__('Active Adverts'),
                'title'     => Mage::helper('amadvert')->__('Active Adverts'),
                'url'       => $this->getUrl('*/*/advertsGrid', array('_current' => true)),
                'class'     => 'ajax',
            ));
            
            
            
            /**
            * @see Amasty_Advert_Model_Placeholder::shouldDisplay for conditions validation
            */
            
            // tab for category-related positions
            if (Mage::helper('amadvert')->checkConditionType($place, 'category'))
            {
                $this->addTab('category_section', array(
                    'label'     => Mage::helper('amadvert')->__('Configuration: Categories'),
                    'title'     => Mage::helper('amadvert')->__('Configuration: Categories'),
                    'content'   => $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_config_category')->toHtml(),
                ));
            }
            
            // tab for fullscreen configuration
            if (Mage::helper('amadvert')->checkConditionType($place, 'fullscreen'))
            {
                $this->addTab('category_section', array(
                    'label'     => Mage::helper('amadvert')->__('Configuration: Fullscreen'),
                    'title'     => Mage::helper('amadvert')->__('Configuration: Fullscreen'),
                    'content'   => $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_config_fullscreen')->toHtml(),
                ));
            }
            
            // tab for fullscreen-precheckout configuration
            if (Mage::helper('amadvert')->checkConditionType($place, 'fullscreen-precheckout'))
            {
                $this->addTab('category_section', array(
                    'label'     => Mage::helper('amadvert')->__('Configuration: Fullscreen Pre-Checkout'),
                    'title'     => Mage::helper('amadvert')->__('Configuration: Fullscreen Pre-Checkout'),
                    'content'   => $this->getLayout()->createBlock('amadvert/adminhtml_placeholder_edit_tab_config_fullscreenprecheckout')->toHtml(),
                ));
            }
        }
        return parent::_beforeToHtml();
    }
}