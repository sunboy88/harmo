<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Advert_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'advert_id';
        $this->_blockGroup = 'amadvert';
        $this->_controller = 'adminhtml_advert';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('amadvert')->__('Save Advert'));
        $this->_updateButton('delete', 'label', Mage::helper('amadvert')->__('Delete Advert'));
        
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('advert_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'advert_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'advert_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    public function getHeaderText()
    {
        if (Mage::registry('amadvert_advert')->getId()) {
            return Mage::helper('cms')->__("Edit Advert '%s'", $this->htmlEscape(Mage::registry('amadvert_advert')->getAlias()));
        }
        else {
            return Mage::helper('cms')->__('New Advert');
        }
    }
}