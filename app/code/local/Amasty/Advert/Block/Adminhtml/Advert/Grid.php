<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Advert_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amadvertAdvertGrid');
        $this->setDefaultSort('alias');
        $this->setDefaultDir('ASC');
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amadvert/advert')->getCollection();
        /* @var $collection Amasty_Advert_Model_Mysql4_Advert_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('title', array(
            'header'    => Mage::helper('amadvert')->__('Advert Title'),
            'index'     => 'title',
        ));
        
        $this->addColumn('is_active', array(
            'header'    =>Mage::helper('amadvert')->__('Enabled'),
            'sortable'  =>true,
            'index'     =>'is_active',
            'type'      => 'options',
            'options' => array(
                '1' => Mage::helper('amadvert')->__('Yes'),
                '0' => Mage::helper('amadvert')->__('No'),
            ),
            'align' => 'center',
            'width' => '120px',
        ));
        
        $this->addColumn('updated_at', array(
            'header'    => Mage::helper('amadvert')->__('Last Modified'),
            'index'     => 'updated_at',
        ));
        
        $this->addColumn('views', array(
            'header'    => Mage::helper('amadvert')->__('Number Of Views'),
            'index'     => 'views',
        ));

        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('advert_id' => $row->getId()));
    }
}