<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Advert_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amadvert_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('amadvert')->__('Advert Information'));
    }
    
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('amadvert')->__('General Information'),
            'title'     => Mage::helper('amadvert')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('amadvert/adminhtml_advert_edit_tab_main')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}