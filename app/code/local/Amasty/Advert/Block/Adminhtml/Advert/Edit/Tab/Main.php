<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Adminhtml_Advert_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Amasty_Adverts_Model_Advert */
        $model = Mage::registry('amadvert_advert');

        $form = new Varien_Data_Form();
        
        $form->setHtmlIdPrefix('advert_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('amadvert')->__('Advert Details'), 'class' => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', array(
                'name' => 'entity_id',
            ));
        }
        
        $yn = array(
            array(
                'value' => '1',
                'label' => $this->__('Yes'),
            ),
            array(
                'value' => '0',
                'label' => $this->__('No'),
            ),
        );

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('amadvert')->__('Title'),
            'title'     => Mage::helper('amadvert')->__('Title'),
            'required'  => true,
        ));

        // we need all these replaces to direct wysiwyg to the cms url
        $mceConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $mceConfig->setData('files_browser_window_url', str_replace('amadvert', 'admin', $mceConfig->getData('files_browser_window_url')));
        $mceConfig->setData('directives_url', str_replace('amadvert', 'admin', $mceConfig->getData('directives_url')));
        $mceConfig->setData('directives_url_quoted', str_replace('amadvert', 'admin', $mceConfig->getData('directives_url_quoted')));
        $mceConfig->setData('widget_window_url', str_replace('amadvert', 'admin', $mceConfig->getData('widget_window_url')));
        $plugins = $mceConfig->getPlugins();
        if ($plugins)
        {
            foreach ($plugins as $i => $plugin)
            {
                if (isset($plugin['options']['url']))
                {
                    $plugins[$i]['options']['url'] = str_replace('amadvert', 'admin', $plugin['options']['url']);
                }
                if (isset($plugin['options']['onclick']['subject']))
                {
                    $plugins[$i]['options']['onclick']['subject'] = str_replace('amadvert', 'admin', $plugin['options']['onclick']['subject']);
                }
            }
        }
        $mceConfig->setPlugins($plugins);

        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => Mage::helper('amadvert')->__('Content'),
            'title'     => Mage::helper('amadvert')->__('Content'),
            'style'     => 'height:36em',
            'required'  => true,
            'config'    => $mceConfig,
        ));
        
        $fieldset->addField('is_active', 'select', array(
            'name'      => 'is_active',
            'label'     => Mage::helper('amadvert')->__('Enabled'),
            'title'     => Mage::helper('amadvert')->__('Enabled'),
            'values'    => $yn,
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    public function getTabLabel()
    {
        return Mage::helper('amadvert')->__('Advert Information');
    }
    
    public function getTabTitle()
    {
        return Mage::helper('amadvert')->__('Advert Information');
    }
    
    public function canShowTab()
    {
        return true;
    }
    
    public function isHidden()
    {
        return false;
    }
}