<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Block_Advert extends Mage_Core_Block_Template
{
    protected function _prepareLayout()
    {
        $this->setTemplate('amadvert/advert.phtml');
        return parent::_prepareLayout();
    }
    
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        
        // assign blocks
        $places = array();
        $places[] = $this->getPlace();
        
        if (Mage::registry('current_category') && !Mage::registry('current_product'))
        {
            $places[] = $this->getPlace() . '-category';
        }
        
        if  (     (Mage::getSingleton('cms/page')->getIdentifier() == 'home'  && Mage::app()->getFrontController()->getRequest()->getRouteName() == 'cms')
              || ($this->getUrl('') == $this->getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true)))     )
        {
            $places[] = $this->getPlace() . '-home';
        }
        
        if ('checkout' == Mage::app()->getRequest()->getModuleName() && 'index' == Mage::app()->getRequest()->getActionName()
                && ('multishipping' == Mage::app()->getRequest()->getControllerName() || 'onepage' == Mage::app()->getRequest()->getControllerName())
                && empty($_POST))
        {
            $places[] = $this->getPlace() . '-precheckout';
        }

        $placeholders      = Mage::helper('amadvert/display')->getPlaceholdersForPlaces($places);
        $placeholderBlocks = array();
        if ($placeholders)
        {
            foreach ($placeholders as $placeholder)
            {
                $block = $this->getLayout()->createBlock('amadvert/advert_component', 'advert_placeholder_' . $placeholder->getId())->setPlaceholder($placeholder);
                $placeholderBlocks[] = $block;
            }
        }
        
        $this->setPlaceholderBlocks($placeholderBlocks);
        
        return $this;
    }
    
    protected function _toHtml()
    {
        if (!$this->getPlaceholderBlocks())
        {
            return '';
        }
        return parent::_toHtml();
    }
}