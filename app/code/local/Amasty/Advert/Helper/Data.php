<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCurrentPlace()
    {
        $place = Mage::app()->getRequest()->getParam('place', 0);
        $model = Mage::registry('amadvert_placeholder');
        if ($model && $model->getPlace())
        {
            $place = $model->getPlace();
        }
        return $place;
    }
    
    public function checkConditionType($place, $type)
    {
        return (   
                      in_array($place, array(1, 2, 3, 4)) && 'category'               == $type
                   || in_array($place, array(5))          && 'fullscreen'             == $type
                   || in_array($place, array(10))         && 'fullscreen-precheckout' == $type
               );
    }

    /**
    * @see Amasty_Advert_Model_Placeholder::shouldDisplay for conditions validation
    */
    public function getPlaceholderPlaces($asAssociative = false)
    {
        $places = array(
            1   => array(
                'code'  => 'right-top-category',
                'title' => $this->__('Top of the right column on category page'),
            ),
            2   => array(
                'code'  => 'left-top-category',
                'title' => $this->__('Top of the left column on category page'),
            ),
            3   => array(
                'code'  => 'right-bottom-category',
                'title' => $this->__('Bottom of the right column on category page'),
            ),
            4   => array(
                'code'  => 'left-bottom-category',
                'title' => $this->__('Bottom of the left column on category page'),
            ),
            5   => array(
                'code'  => 'fullscreen',
                'title' => $this->__('Fullscreen popup on each page'),
            ),
            6   => array(
                'code'  => 'right-top',
                'title' => $this->__('Top of the right column'),
            ),
            7   => array(
                'code'  => 'left-top',
                'title' => $this->__('Top of the left column'),
            ),
            8   => array(
                'code'  => 'right-bottom',
                'title' => $this->__('Bottom of the right column'),
            ),
            9   => array(
                'code'  => 'left-bottom',
                'title' => $this->__('Bottom of the left column'),
            ),
            10   => array(
                'code'  => 'fullscreen-precheckout',
                'title' => $this->__('Fullscreen popup pre-checkout'),
            ),
            11   => array(
                'code'  => 'content',
                'title' => $this->__('Above the main content block'),
            ),
            12   => array(
                'code'  => 'menu-bottom',
                'title' => $this->__('Under the main menu'),
            ),
            13   => array(
                'code'  => 'menu-top',
                'title' => $this->__('Above the main menu'),
            ),
            14   => array(
                'code'  => 'footer-bottom',
                'title' => $this->__('Under the footer links'),
            ),
            15   => array(
                'code'  => 'footer-top',
                'title' => $this->__('Above the footer links'),
            ),
            16   => array(
                'code'  => 'menu-bottom-home',
                'title' => $this->__('Under the main menu on homepage'),
            ),
        );
        if ($asAssociative)
        {
            $placesAssoc = array();
            foreach ($places as $id => $data)
            {
                $placesAssoc[$id]   = $data['title'];
            }
            asort($placesAssoc, SORT_LOCALE_STRING);
            return $placesAssoc;
        }
        return $places;
    }
    
    public function getPlaceholderPlacesCodes()
    {
        $places = $this->getPlaceholderPlaces();
        $codes = array();
        foreach ($places as $id => $place)
        {
            $codes[$place['code']] = $id;
        }
        return $codes;
    }
}