<?php
/**
* @author Amasty Team
* @copyright Amasty
* @package Amasty_Advert
*/
class Amasty_Advert_Helper_Display extends Mage_Core_Helper_Abstract
{
    public function getPlaceholdersForPlaces($places)
    {
        $placeCodes = Mage::helper('amadvert')->getPlaceholderPlacesCodes();
        $placeIds = array();
        // detecting place ids for place codes (place codes comes from layout)
        foreach ($places as $place)
        {
            if (isset($placeCodes[$place]))
            {
                $placeIds[] = $placeCodes[$place];
            }
        }
        if (!$placeIds)
        {
            return array();
        }

        $placeholders = array();
        
        // getting placeholders collection for place ids detected
        $placeholderCollection = Mage::getModel('amadvert/placeholder')->getCollection();
        $placeholderCollection->addFieldToFilter('is_active', 1);
        $placeholderCollection->addFieldToFilter('place', array('in' => $placeIds));
        $placeholderCollection->load();
        if ($placeholderCollection->getSize())
        {
            foreach ($placeholderCollection as $placeholder)
            {
                if ($placeholder->shouldDisplay())
                {
                    $placeholder->populateContent();
                    if ($placeholder->getAdvertContent())
                    {
                        $placeholders[] = $placeholder;
                    }
                }
            }
        }
     
        return $placeholders;
    }
}