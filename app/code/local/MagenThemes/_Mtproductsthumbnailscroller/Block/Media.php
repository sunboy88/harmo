<?php
class MagenThemes_Mtproductsthumbnailscroller_Block_Media extends Mage_Catalog_Block_Product_View_Media
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getMtproductsthumbnailscroller()     
     { 
        if (!$this->hasData('mtproductsthumbnailscroller')) {
            $this->setData('mtproductsthumbnailscroller', Mage::registry('mtproductsthumbnailscroller'));
        }
        return $this->getData('mtproductsthumbnailscroller');
        
    }
	public function getConfig($att) 
	{
		$config = Mage::getStoreConfig('mtproductsthumbnailscroller');
		if (isset($config['mtproductsthumbnailscroller_config']) ) {
			$value = $config['mtproductsthumbnailscroller_config'][$att];
			return $value;
		} else {
			throw new Exception($att.' value not set');
		}
	}
}