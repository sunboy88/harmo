<?php
/*------------------------------------------------------------------------
# APL Solutions and Vision Co., LTD
# ------------------------------------------------------------------------
# Copyright (C) 2008-2010 Solutions and Vision Co., LTD. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: APL Solutions and Vision Co., LTD
# Websites: http://www.joomlavision.com/ - http://www.magentheme.com/
-------------------------------------------------------------------------*/ 
$installer = $this;
$installer->startSetup();
$installer->setConfigData('mtsmartmenu/mtsmartmenu_config/enabled',1);	
$installer->setConfigData('mtsmartmenu/mtsmartmenu_config/title','MT SmartMenu');	
$installer->setConfigData('mtsmartmenu/mtsmartmenu_config/menustyle','Accordion');	
$installer->endSetup(); 
