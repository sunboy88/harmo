<?php
/*------------------------------------------------------------------------
# APL Solutions and Vision Co., LTD
# ------------------------------------------------------------------------
# Copyright (C) 2008-2010 APL Solutions and Vision Co., LTD. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: APL Solutions and Vision Co., LTD
# Websites: http://www.joomlavision.com/ - http://www.magentheme.com/
-------------------------------------------------------------------------*/ 
class MagenThemes_MTProductsScroller_Model_Config_Categories
{
    public function toOptionArray()
    {
    	$_categories =$this->getStoreCategories();
    	$arr = array();
    	if($_categories)
    	{
			foreach($_categories as $_category){
				$_category=Mage::getModel('catalog/category')->load($_category->getId());
				if($_category->getIsActive())
				{
					$arr[] = array('value'=>$_category->getId(), 'label'=>$_category->getName());
					if($_category->getChildren())
					{
						$this->_getSubCategories($_category, $arr);
					}
				}
			} 
    	}else{
    		$_categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('entity_id' , array('gt' => '3'))
                    ->addAttributeToSelect('name');
			if (count($_categories) > 0){
				foreach($_categories as $_category){
					$arr[] = array('value'=>$_category->getId(), 'label'=>$_category->getName());								
				} 
			}	
    	}
    	return $arr;
	}
	protected function _getSubCategories($parent,&$arr)
	{
		$children = explode(',', $parent->getChildren());
		foreach ($children as $child)
		{
			$_category = Mage::getModel('catalog/category')->load($child);
			$arr[] = array('value'=>$_category->getId(), 'label'=>$this->getLabel($_category->getLevel(), $_category->getName()));
			if($_category->getChildren())
			{
				$this->_getSubCategories($_category, $arr);
			}
		}
	}
	protected function getLabel($level,$name)
	{
		$str = '';
		$level=($level-3)*3;
		for($i=0;$i<$level;$i++)
		{
			$str.='-';
		}
		$str.='|--- ';
		return $str.$name;
	}
	public function getStoreCategories()
	{
		$store = Mage::app()->getRequest()->getParam('store',false);
		$website = Mage::app()->getRequest()->getParam('website',false);
		if($store)
		{
			$parent     = Mage::app()->getStore($store)->getRootCategoryId();
			$rootCategory = Mage::getModel('catalog/category')->load($parent);
			
			$recursionLevel  = max(0, (int) Mage::app()->getStore()->getConfig('catalog/navigation/max_depth'));
			$_categories= $rootCategory->getCategories($parent, $recursionLevel, false, false, true);
			return $_categories;
		}else{
			/*$_categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('entity_id' , array('gt' => '3'))
                    ->addAttributeToSelect('name');
                    */
        
		}
		return false;//$_categories;
		$children = $rootCategory->getChildren();
		if(!empty($children))
		{
			$children = explode(',', $children);
		}
	}
	/*
    public function toOptionArray()
    {
    	$_cat =$this->getStoreCategories();
		$_categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('entity_id' , array('gt' => '3'))
                    ->addAttributeToSelect('name');
		$arr = array();
		if (count($_categories) > 0):
			foreach($_categories as $_category):
				$arr[] = array('value'=>$_category->getId(), 'label'=>$_category->getName());								
			endforeach; 
		endif;
		return $arr;
	}
	*/

	/*
	public function getStoreCategories($sorted=false, $asCollection=false, $toLoad=true)
    {
        $parent     = Mage::app()->getStore()->getRootCategoryId();
        $cacheKey   = sprintf('%d-%d-%d-%d', $parent, $sorted, $asCollection, $toLoad);
        if (isset($this->_storeCategories[$cacheKey])) {
            return $this->_storeCategories[$cacheKey];
        }
        $category = Mage::getModel('catalog/category');
        $recursionLevel  = max(0, (int) Mage::app()->getStore()->getConfig('catalog/navigation/max_depth'));
        $storeCategories = $category->getCategories($parent, $recursionLevel, $sorted, $asCollection, $toLoad);
        $this->_storeCategories[$cacheKey] = $storeCategories;
        return $storeCategories;
    }
    */
}
