<?php
/******************************************************
 * @package MtProductsScroller module for Magento 1.4.x.x and Magento 1.5.x.x
 * @version 1.4.1.1
 * @author http://www.9magentothemes.com
 * @copyright (C) 2011- 9MagentoThemes.Com
 * @license PHP files are GNU/GPL
*******************************************************/
?>
<?php
class MagenThemes_MtProductsScroller_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('mtproductsscroller')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('mtproductsscroller')->__('Disabled')
        );
    }
}