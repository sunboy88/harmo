<?php
/*------------------------------------------------------------------------
# APL Solutions and Vision Co., LTD
# ------------------------------------------------------------------------
# Copyright (C) 2008-2010 APL Solutions and Vision Co., LTD. All Rights 

Reserved.
# @license - Copyrighted Commercial Software
# Author: APL Solutions and Vision Co., LTD
# Websites: http://www.joomlavision.com/ - http://www.magentheme.com/
-------------------------------------------------------------------------*/ 
class MagenThemes_MtProducts_Model_Config_Categories
{
    public function toOptionArray()
    {
    	$_categories =$this->getStoreCategories();
    	$arr = array();
    	if($_categories)
    	{
			foreach($_categories as $_category){
				$_category=Mage::getModel('catalog/category')->load($_category->getId());
				if($_category->getIsActive())
				{
					$arr[] = array('value'=>$_category->getId(), 'label'=>$_category->getName());
					if($_category->getChildren())
					{
						$this->_getSubCategories($_category, $arr);
					}
				}
			} 
    	}else{
    		$_categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('entity_id' , array('gt' => '3'))
                    ->addAttributeToSelect('name');
			if (count($_categories) > 0){
				foreach($_categories as $_category){
					$arr[] = array('value'=>$_category->getId(), 'label'=>$_category->getName());								
				} 
			}	
    	}
    	return $arr;
	}
	protected function _getSubCategories($parent,&$arr)
	{
		$children = explode(',', $parent->getChildren());
		foreach ($children as $child)
		{
			$_category = Mage::getModel('catalog/category')->load($child);
			$arr[] = array('value'=>$_category->getId(), 'label'=>$this->getLabel($_category->getLevel(), $_category->getName()));
			if($_category->getChildren())
			{
				$this->_getSubCategories($_category, $arr);
			}
		}
	}
	protected function getLabel($level,$name)
	{
		$str = '|';
		$level=($level-2)*3;
		for($i=0;$i<$level;$i++)
		{
			$str.='-';
		}
		return $str.$name;
	}
	public function getStoreCategories()
	{
		$store = Mage::app()->getRequest()->getParam('store',false);
		if($store)
		{
		$parent     = Mage::app()->getStore($store)->getRootCategoryId();
		$rootCategory = Mage::getModel('catalog/category')->load($parent);
		
		$recursionLevel  = max(0, (int) Mage::app()->getStore()->getConfig('catalog/navigation/max_depth'));
		$_categories= $rootCategory->getCategories($parent, $recursionLevel, false, false, true);
		return $_categories;
		}else{
			/*$_categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addFieldToFilter('entity_id' , array('gt' => '3'))
                    ->addAttributeToSelect('name');
                    */
        
		}
		return false;//$_categories;
		$children = $rootCategory->getChildren();
		Mage::log($children,null,'dulv.log');
		if(!empty($children))
		{
			$children = explode(',', $children);
		}
	}
	/*
    public function toOptionArray()
    {
		$category = Mage::getModel('catalog/category'); 
		$tree = $category->getTreeModel(); 
		$tree->load();
		$ids = $tree->getCollection()->getAllIds(); 
		$arr = array();
		if ($ids)
		{ 
			$i=0;
			foreach ($ids as $id)
				{ 
					$cat = Mage::getModel('catalog/category'); 
					$cat->load($id);	
					if($cat->getIsActive()==1 && $cat->getId()!=1)
					{
						$arr[$i]=array('value'=>$cat->getId(), 'label'=>Mage::helper('adminhtml')->__($cat->getName()));
						$i++;
					}					
				} 
		}
		return $arr;
    }
    */
}
