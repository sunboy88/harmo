<?php
/*------------------------------------------------------------------------
# APL Solutions and Vision Co., LTD
# ------------------------------------------------------------------------
# Copyright (C) 2008-2010 APL Solutions and Vision Co., LTD. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: APL Solutions and Vision Co., LTD
# Websites: http://www.joomlavision.com/ - http://www.magentheme.com/
-------------------------------------------------------------------------*/ 
class MagenThemes_MtProducts_Block_MtProducts extends Mage_Catalog_Block_Product_Abstract
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getMtProducts()     
     { 
        if (!$this->hasData('mtproducts')) {
            $this->setData('mtproducts', Mage::registry('mtproducts'));
        }
        return $this->getData('mtproducts');
        
    }
    public function getListProducts()
    {
    	$products = null;
    	$mode=$this->getConfig('mode');
		switch ($mode) {
			case 'latest' :
				$products = $this->getListLatestProducts();
				break;
			case 'bestseller' : 
				$products = $this->getListBestSellerProducts();
				break;
			case 'mostviewed' :
				$products = $this->getListMostViewedProducts();
				break;
			case 'featured' :
				$products = $this->getListFeaturedProducts();
				break;
			case 'new' :
				$products = $this->getListNewProducts();
				break;
		}
		
		return $products;
    }
    public function getListLatestProducts($fieldorder = 'updated_at', $order = 'desc')
    {
    	$storeId    = Mage::app()->getStore()->getId();
    	$cateids = $this->getConfig('catsid');
    	if($cateids != null)
    	{
    		$arr_productids = $this->getProductByCategory();  
    		$products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->addIdFilter($arr_productids)// id product
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder ($fieldorder,$order) //latest product
            ;	
    	}
    	else 
    	{
        	$products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder ($fieldorder,$order) //latest product
            ;      
    	}		
        //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
        $this->setProductCollection($products);
    }
	public function getListBestSellerProducts($fieldorder = 'ordered_qty', $order = 'desc')
    {
    	$storeId    = Mage::app()->getStore()->getId();
    	$cateids = $this->getConfig('catsid');
    	if($cateids != null)
    	{
    		$arr_productids = $this->getProductByCategory();
    		$products = Mage::getResourceModel('reports/product_collection')
    		->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->addIdFilter($arr_productids)// id product
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder ($fieldorder,$order) //latest product
            ;
    	}
    	else 
    	{
    		$products = Mage::getResourceModel('reports/product_collection')
    		->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder ($fieldorder,$order) //latest product
            ;
    	}
    			
        //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
        $this->setProductCollection($products);
    }
	public function getListMostViewedProducts()
    {
    	$storeId    = Mage::app()->getStore()->getId();
    	$cateids = $this->getConfig('catsid');
    	if($cateids != null)
    	{
    		$arr_productids = $this->getProductByCategory();
    		$products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes            
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addViewsCount()
            ->addIdFilter($arr_productids)// id product
            ;
    	}
    	else 
    	{
    		$products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addViewsCount()
            ;
    	}
    			
        //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
        $this->setProductCollection($products);
    }
	public function getListFeaturedProducts()
    {
    	$storeId    = Mage::app()->getStore()->getId();
    	$cateids = $this->getConfig('catsid');
    	if($cateids != null)
    	{
			/* Comment by CEDCOSS */
			/* $arr_productids = $this->getProductByCategory(); */
			
			/* Added by CEDCOSS */
			$catsid = explode(',',$cateids);
			$all_cat = array();
			foreach($catsid as $cid){
				if($cid !=null){
					$all_cat[] = array('finset' => $cid);
				}
			}
			if(isset($_GET['ced_query'])){
				$all_cat = array('finset' => 1);
			}
   			$products = Mage::getModel('catalog/product')->getCollection()
			/*->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left') */
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            /* Comment by CEDCOSS */
			/* ->addIdFilter($arr_productids) */
			/* Added by CEDCOSS */
			->addFieldToFilter('category_id',$all_cat)
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addAttributeToFilter('featured1', array('eq' => 1))
			->groupByAttribute('entity_id');
			if(isset($_GET['ced_query'])){
				echo $products->getSelect();die('={{slide}}');
			}
    	}
    	else 
    	{
    		$products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addAttributeToFilter('featured1', array('eq' => 1));
    	}
    	
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
        $this->setProductCollection($products);
    }
	public function getListNewProducts()
    {
    	$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
    	$storeId    = Mage::app()->getStore()->getId();
    	$cateids = $this->getConfig('catsid');
    	if($cateids != null)
    	{
    		$arr_productids = $this->getProductByCategory();
    		$products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->addIdFilter($arr_productids)// id product
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addAttributeToFilter('news_from_date', array('date'=>true, 'to'=> $todayDate))
			->addAttributeToFilter(array(array('attribute'=>'news_to_date', 'date'=>true, 'from'=>$todayDate), array('attribute'=>'news_to_date', 'is' => new Zend_Db_Expr('null'))),'','left')
			->addAttributeToSort('news_from_date','desc');
    	}
    	else 
    	{
    		$products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addAttributeToFilter('news_from_date', array('date'=>true, 'to'=> $todayDate))
			->addAttributeToFilter(array(array('attribute'=>'news_to_date', 'date'=>true, 'from'=>$todayDate), array('attribute'=>'news_to_date', 'is' => new Zend_Db_Expr('null'))),'','left')
			->addAttributeToSort('news_from_date','desc');
    	}
    			
        //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);
        $this->setProductCollection($products);
    }
	public function getPro()
	{
        $storeId    = Mage::app()->getStore()->getId();
        // get array product_id
        $arr_productids = $this->getProductByCategory();   //id product

        $products = Mage::getResourceModel('reports/product_collection')
            //->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addIdFilter($arr_productids)// id product
            //->setOrder ('updated_at','desc') //latest product
            //->addAttributeToFilter("featured", 1); //feature product
           //->addViewsCount()// most viewed
			//->setOrder('ordered_qty', 'desc'); //best sellers on top
            ;

		
        //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);

        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);

        $this->setProductCollection($products);
    }
	function inArray($source, $target) {
		for($i = 0; $i < sizeof ( $source ); $i ++) {
			if (in_array ( $source [$i], $target )) {
				return true;
			}
		}
	}
	function getProductByCategory(){
        $return = array(); 
        $pids = array();
        $catsid=$this->getConfig('catsid');
        $products = Mage::getResourceModel ( 'catalog/product_collection' );
         
        foreach ($products->getItems() as $key => $_product){
            $arr_categoryids[$key] = $_product->getCategoryIds();
            
            if($catsid){    
                if(stristr($catsid, ',') === FALSE) {
                    $arr_catsid[$key] =  array(0 => $catsid);
                }else{
                    $arr_catsid[$key] = explode(",", $catsid);
                }
                
                $return[$key] = $this->inArray($arr_catsid[$key], $arr_categoryids[$key]);
            }
        }
        
        foreach ($return as $k => $v){ 
            if($v==1) $pids[] = $k;
        }    
        
        return $pids;   
    }
	public function getConfig($att) 
	{
		static $config=null;
		if(is_null($config))
		{
			$config = Mage::getStoreConfig('mtproducts');
		}
		
		if (isset($config['mtproducts_config']) && isset($config['mtproducts_config'][$att])) {
			$value = $config['mtproducts_config'][$att];
			return $value;
		} else {
			return false;
		}
	}
}
	