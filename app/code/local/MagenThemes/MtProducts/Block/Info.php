<?php
/*------------------------------------------------------------------------
# APL Solutions and Vision Co., LTD
# ------------------------------------------------------------------------
# Copyright (C) 2008-2010 APL Solutions and Vision Co., LTD. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: APL Solutions and Vision Co., LTD
# Websites: http://www.joomlavision.com/ - http://www.magentheme.com/
-------------------------------------------------------------------------*/ 
class MagenThemes_Mtproducts_Block_Info extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {		
		$html = $this->_getHeaderHtml($element);		
		$html.= $this->_getFieldHtml($element);        
        $html .= $this->_getFooterHtml($element);
        return $html;
    }
	protected function _getFieldHtml($fieldset)
    {
		$content = 'MT ProductList version : 2.0.4<br/>Author : <a href="http://www.magentheme.com" title="Magento Themes">MagenTheme.Com</a><br />Copyright &copy; 2011- MagenTheme.Com';
		return $content;
    }
}
