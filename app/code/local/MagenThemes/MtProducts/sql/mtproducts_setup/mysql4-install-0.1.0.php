<?php


/*------------------------------------------------------------------------
# APL Solutions and Vision Co., LTD
# ------------------------------------------------------------------------
# Copyright (C) 2008-2010 APL Solutions and Vision Co., LTD. All Rights 

Reserved.
# @license - Copyrighted Commercial Software
# Author: APL Solutions and Vision Co., LTD
# Websites: http://www.joomlavision.com/ - http://www.magentheme.com/
-------------------------------------------------------------------------*/ 
$installer = $this;
$installer->startSetup();

$installer->setConfigData('mtproducts/mtproducts_config/enabled',1);	
$installer->setConfigData('mtproducts/mtproducts_config/mode','latest');
$installer->setConfigData('mtproducts/mtproducts_config/title','MT Product List');
$installer->setConfigData('mtproducts/mtproducts_config/qty',9);
$installer->setConfigData('mtproducts/mtproducts_config/items',3);
$installer->setConfigData('mtproducts/mtproducts_config/width',135);
$installer->setConfigData('mtproducts/mtproducts_config/height',135);
$installer->setConfigData('mtproducts/mtproducts_config/addtocart',1);
$installer->setConfigData('mtproducts/mtproducts_config/addtowishlist',1);
$installer->setConfigData('mtproducts/mtproducts_config/addtocompare',1);
$installer->setConfigData('mtproducts/mtproducts_config/shortdiscription',0);
$installer->setConfigData('mtproducts/mtproducts_config/catsid','');
$installer->setConfigData('mtproducts/mtproducts_config/maxlength','');
$installer->endSetup(); 
