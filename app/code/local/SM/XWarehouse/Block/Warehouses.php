<?php

class SM_XWarehouse_Block_Warehouses extends Mage_Catalog_Block_Product_View
{
    public function getWarehouses()
    {
        $product = $this->getProduct();
        $productId = $product->getId();
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $result = array();

        if($customer->getId()){
            $warehouseDefault = Mage::getModel('xwarehouse/warehouse')->getCollection();
            $warehouseDefault->getSelect()->join(array('p_w'=>'product_warehouses'),'main_table.warehouse_id = p_w.warehouse_id',array('qty'))->where('p_w.qty > 0 and p_w.product_id = '.$productId.' and main_table.warehouse_id=?',$customer->getDefaultWarehouse());

            $result[] = $warehouseDefault->getFirstItem();

            $warehouses = Mage::getModel('xwarehouse/warehouse')->getCollection();
            $warehouses->getSelect()->join(array('p_w'=>'product_warehouses'),'main_table.warehouse_id = p_w.warehouse_id',array('qty'))
                                    ->where('p_w.qty > 0 and p_w.product_id = '.$productId.' and main_table.warehouse_id !=?', $customer->getDefaultWarehouse())
                                    ->order('main_table.short_name asc');

            foreach($warehouses as $warehouse){
                $result[] = $warehouse;
            }
            return $result;
        } else {
            $warehouses = Mage::getModel('xwarehouse/warehouse')->getCollection();
            $warehouses->getSelect()->join(array('p_w'=>'product_warehouses'),'main_table.warehouse_id = p_w.warehouse_id',array('qty'))->where('p_w.qty > 0 and p_w.product_id = '.$productId.'')->order('main_table.short_name asc');
            return $warehouses;
        }
    }

    public function getProduct()
    {
        return Mage::registry('product');
    }
}