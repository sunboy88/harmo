<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hiru
 * Date: 2/20/13
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */

class SM_XWarehouse_Block_Override_Adminhtml_Catalog_Product extends Mage_Adminhtml_Block_Catalog_Product {
    public function __construct() {
        parent::__construct();
        if (Mage::helper('xwarehouse/abstract')->isEnable()) {
            $this->setTemplate('sm/warehouse/adminhtml/catalog/product.phtml');
        }
    }
}