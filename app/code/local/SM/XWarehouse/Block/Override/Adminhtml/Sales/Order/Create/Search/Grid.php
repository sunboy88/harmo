<?php
/**
 * Author: Hieunt
 * Date: 4/5/13
 * Time: 5:00 PM
 */ 
class SM_XWarehouse_Block_Override_Adminhtml_Sales_Order_Create_Search_Grid extends Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid {
    protected function _prepareCollection()
    {
        $warehouseId = $this->getRequest()->getParam('warehouse_id');
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();

        if (!empty($warehouseId)) {

            $warehouse = new Zend_Db_Select($collection->getResource()->getReadConnection());
            $warehouse
                ->from($collection->getResource()->getTable('xwarehouse/warehouse_product'))
                ->where('1=1 AND enable = 1 AND qty > 0 AND warehouse_id = ?', $warehouseId);

            $collection->getSelect()->join(array('w' =>  $warehouse),'w.product_id = e.entity_id');
        }

        $collection
            ->setStore($this->getStore())
            ->addAttributeToSelect($attributes)
            ->addAttributeToSelect('sku')
            ->addStoreFilter()
            ->addAttributeToFilter('type_id', array_keys(
                Mage::getConfig()->getNode('adminhtml/sales/order/create/available_product_types')->asArray()
            ))
            ->addAttributeToSelect('gift_message_available');

        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
}