<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hiru
 * Date: 2/20/13
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */

class SM_XWarehouse_Block_Override_Adminhtml_Catalog_Product_Edit_Tab_Inventory extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Inventory {


    public function __construct()
    {
        parent::__construct();
        if (Mage::helper('xwarehouse/abstract')->isEnable())
        $this->setTemplate('sm/warehouse/adminhtml/catalog/product/tab/inventory.phtml');

    }

    public function _prepareLayout() {
        $this->enableJquery();
        parent::_prepareLayout();
    }

    public function isWarehouseItemEnable($wId) {
        if ($this->getProduct()->getId())
            return Mage::helper('xwarehouse')->isWarehouseItemEnable($this->getProduct()->getId(),$wId);
        return 0;
    }

    public function isVirtual()
    {
        return $this->getProduct()->getTypeInstance()->isVirtual();
    }

    /**
     * call this function in template to enable Jquery
    */
    public function enableJquery() {
        $layout = $this->getLayout();
        $helper = Mage::helper('xwarehouse');
        $helper->addJQuery($layout);
    }


}