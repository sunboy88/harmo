<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hiru
 * Date: 2/20/13
 * Time: 4:13 PM
 * To change this template use File | Settings | File Templates.
 */

class SM_XWarehouse_Block_Override_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item {
    /**
     * Default stock
    */
    protected $stockId = 1;

    public function setStock($id) {
        $this->stockId = $id;
    }

    public function getStockId()
    {
        return $this->stockId;
    }
}