<?php
/**
 * Author: Hieunt
 * Date: 4/12/13
 * Time: 10:48 AM
 */
class SM_XWarehouse_Block_Adminhtml_Permissions_User_Edit_Tabs extends Mage_Adminhtml_Block_Permissions_User_Edit_Tabs {

    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $isXposActive = Mage::getConfig()->getModuleConfig('xpos')->is('active', 'true');
        if( Mage::getStoreConfig('xwarehouse/general/enabled') ==1 &&
            ( !$isXposActive || (Mage::getStoreConfig('xpos/advanced/integrate_xmwh_enabled')==1 && Mage::getStoreConfig('xpos/general/enabled')==1 ))){
            $this->addTab('user_warehouse', array(
                'label'     => Mage::helper('adminhtml')->__('User Warehouse Role'),
                'title'     => Mage::helper('adminhtml')->__('User Warehouse Role'),
                'content'   => $this->getLayout()->createBlock('xwarehouse/adminhtml_permissions_warehouse_grid')->toHtml(),
            ));
        }
        $grandParent = get_parent_class(get_parent_class($this));
        return $grandParent::_beforeToHtml();
    }

}