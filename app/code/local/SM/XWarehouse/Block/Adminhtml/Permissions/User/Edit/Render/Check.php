<?php
/**
 * Author: Hieunt
 * Date: 4/12/13
 * Time: 10:48 AM
 */

class SM_XWarehouse_Block_Adminhtml_Permissions_User_Edit_Render_Check extends  Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
    public function render(Varien_Object $row)
    {
        $edit_user_id = Mage::app()->getRequest()->getParam('user_id');
        if(isset($edit_user_id)){
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = "SELECT * FROM `sm_user_warehouse` WHERE `user_id` =" . $edit_user_id . " AND warehouse_id = " . $row->getId() . " " ;
            $results = $readConnection->fetchAll($query);
            if(count($results) > 0){//assigned rights
                $html ='<input type="checkbox" name="warehouse['.$row->getId().']"  value="'.$row->getId().'" class="massaction-checkbox" checked="yes">';
            } else{
                $html ='<input type="checkbox" name="warehouse['.$row->getId().']"  value="'.$row->getId().'" class="massaction-checkbox">';
            }
        } else{ //new user
            $html ='<input type="checkbox" name="warehouse['.$row->getId().']"  value="'.$row->getId().'" class="massaction-checkbox">';
        }

        return $html;
    }
}