<?php
/**
 * Author: Hieunt
 * Date: 3/21/13
 * Time: 2:41 PM
 */ 
class SM_XWarehouse_Block_Adminhtml_Sales_Order_Create_Form extends Mage_Adminhtml_Block_Sales_Order_Create_Form {

    protected $_isModuleEnable = false;

    public function _prepareLayout() {
        if (!$this->isModuleEnable()) {

        }
    }

    public function getWarehouseSelectorDisplay()
    {
        if (!$this->isModuleEnable()) {
            return 'none';
        }

        $warehouseId = $this->getWarehouseId();
        $customerId = $this->getCustomerId();
        if (is_null($warehouseId) && !is_null($customerId)) {
            return 'block';
        }
        return 'none';
    }

    public function getStoreSelectorDisplay()
    {
        if (!$this->isModuleEnable()) {
            return parent::getStoreSelectorDisplay();
        }

        $storeId    = $this->getStoreId();
        $customerId = $this->getCustomerId();
        $warehouseId = $this->getWarehouseId();
        if (!is_null($customerId) && !$storeId && $warehouseId) {
            return 'block';
        }
        return 'none';
    }

    public function getDataSelectorDisplay()
    {
        if (!$this->isModuleEnable()) {
            return parent::getDataSelectorDisplay();
        }

        $storeId    = $this->getStoreId();
        $customerId = $this->getCustomerId();
        $warehouseId = $this->getWarehouseId();
        if (!is_null($customerId) && !is_null($storeId) && !is_null($warehouseId)) {
            return 'block';
        }
        return 'none';
    }

    public function getWarehouseId()
    {
        return $this->_getSession()->getWarehouseId();
    }

    public function getOrderDataJson()
    {
        if (!$this->isModuleEnable()) {
            return parent::getOrderDataJson();
        }

        $data = array();
        if (!is_null($this->getCustomerId())) {
            $data['customer_id'] = $this->getCustomerId();
            $data['addresses'] = array();

            /* @var $addressForm Mage_Customer_Model_Form */
            $addressForm = Mage::getModel('customer/form')
                ->setFormCode('adminhtml_customer_address')
                ->setStore($this->getStore());
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $data['addresses'][$address->getId()] = $addressForm->setEntity($address)
                    ->outputData(Mage_Customer_Model_Attribute_Data::OUTPUT_FORMAT_JSON);
            }
        }

        if (!is_null($this->getWarehouseId())) {
                $data['warehouse_id'] = $this->getWarehouseId();
        }

        if (!is_null($this->getStoreId())) {
            $data['store_id'] = $this->getStoreId();
            $currency = Mage::app()->getLocale()->currency($this->getStore()->getCurrentCurrencyCode());
            $symbol = $currency->getSymbol() ? $currency->getSymbol() : $currency->getShortName();
            $data['currency_symbol'] = $symbol;
            $data['shipping_method_reseted'] = !(bool)$this->getQuote()->getShippingAddress()->getShippingMethod();
            $data['payment_method'] = $this->getQuote()->getPayment()->getMethod();
        }
        return Mage::helper('core')->jsonEncode($data);
    }

    public function isModuleEnable() {
        return Mage::helper('xwarehouse/abstract')->isEnable();
    }
}