<?php
/**
 * Author: Hieunt
 * Date: 3/21/13
 * Time: 5:00 PM
 */

class SM_XWarehouse_Block_Adminhtml_Sales_Order_Create_Warehouse extends Mage_Core_Block_Template {
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_create_warehouse');
    }

    public function getHeaderText()
    {
        return Mage::helper('sales')->__('Please Select a Warehouse');
    }
}