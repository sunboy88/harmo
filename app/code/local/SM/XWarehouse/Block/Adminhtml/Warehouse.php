<?php

class SM_XWarehouse_Block_Adminhtml_Warehouse extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
      parent::__construct();
        $this->_controller = 'adminhtml_warehouse';
        $this->_blockGroup = 'xwarehouse';
        $this->_headerText = Mage::helper('xwarehouse')->__('Warehouses Manager');

      if ($this->_isAllowedAction('new')) {
          $this->_updateButton('add', 'label', Mage::helper('xwarehouse')->__('Add Warehouse'));
      } else {
          $this->_removeButton('add');
      }

  }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/warehouse/manage/' . $action);
    }
}