<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC
 */

class SM_XWarehouse_Block_Adminhtml_Stock extends Mage_Adminhtml_Block_Widget_Grid_Container {
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_stock';
        $this->_blockGroup = 'xwarehouse';
        $this->_headerText = Mage::helper('xwarehouse')->__('Warehouses Stock Manager');
        $this->_removeButton('add');

        $this->_addButton('saveAll', array(
            'label'     => $this->__('Save All'),
            'onclick'   => 'stockform.submit()',
            'class'     => 'save',
        ));

        $this->_addButton('reset', array(
            'label'     => $this->__('Reset All Change'),
            'onclick'   => "reset()",
            'class'     => 'delete inactive',
        ));
    }

    public function getSaveAllUrl()
    {
        return $this->getUrl('*/*/saveall');
    }
}