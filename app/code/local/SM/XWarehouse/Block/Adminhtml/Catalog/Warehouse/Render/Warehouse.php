<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC 
 */

class SM_XWarehouse_Block_Adminhtml_Catalog_Warehouse_Render_Warehouse extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
        $column = $this->getColumn();
        $helper = Mage::helper('xwarehouse');
        $warehouseId = $helper->getWarehouseByLabel($column->getId());

        $productId = $row->getId();

        $isEnable = $helper->isWarehouseItemEnable($productId,$warehouseId);

        if (empty($isEnable)) {
            $html = '<input type="text" name="warehouse['.$productId.']['.$warehouseId.']" class="editable" value="-" style="width:50%; display: none;"  />';
            $html .= '<span style="position: absolute; width: 50%;" class="itext" >-</span>';
            $html .= '<input type="checkbox" class="dancer" />';

            return $html;
        }
        // var_dump($row->getData());
        if (strpos($column->getIndex()," ")) {
            $qty = $row->getData(strtolower($column->getIndex()));
        } else {
            $qty = $row->getData(strtolower($column->getHeader()));
        }

        $html = '<input type="text" name="warehouse['.$productId.']['.$warehouseId.']" class="editable" value="'.intval($qty).'" style="width:50%; display: none;"  />';
        $html .= '<span style="position: absolute; width: 50%;" class="itext">'.intval($qty).'</span>';
        $html .= '<input type="checkbox" class="dancer" />';

        return $html;
    }
}