<?php
/**
 * Author: Hieunt
 * Date: 3/19/13
 * Time: 6:29 PM
 */

class SM_XWarehouse_Block_Adminhtml_Catalog_Product_Render_Qty extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
        $warehouseId = Mage::app()->getRequest()->getParam('warehouse');
        $productId = $row->getId();
        /** @var Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product')->load($productId);
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

        if ($warehouseId) {
            $qty = Mage::helper('xwarehouse')->getWarehouseQty($productId,$warehouseId);
            $collection = Mage::helper('xwarehouse')->getWarehouseItem($productId,$warehouseId);
            $item = $collection->getFirstItem();
            $enable = 0;
            if (!empty($item)) {
                $enable = $item->getEnable();
            }
            if(isset($product)) {
                if (!empty($enable)) {
                    return '<span>'.intval($qty).'</span>';
                } else {
                    return '<span>Not Available</span>';
                }
            }
        }
        else {
            return '<span>'.intval($stockItem->getQty()).'</span>';
        }


    }
}