<?php 
/*
 * 
 * @created     21 December'13
 * @author      Madhulika Gupta
 */
class SM_XWarehouse_Block_Adminhtml_Catalog_Warehouse_Render_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex()); 
		$products = Mage::getModel('catalog/product');
		$details = $products->loadByAttribute('sku', $value); 
		$html = '';
		foreach($details->getCategoryIds() as $id){
			$category = Mage::getModel('catalog/category')->load($id);
			$html .= $category->getName().", <br>";
		}
		return $html;
		
	}
	
}?>
   