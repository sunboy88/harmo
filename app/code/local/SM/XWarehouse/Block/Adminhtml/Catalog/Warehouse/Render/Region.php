<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Block_Adminhtml_Catalog_Warehouse_Render_Region extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
        $region = Mage::getModel('directory/region')->load($row->getRegion());
        if ($region->getId()) {
            return $region->getName();
        }
        return $row->getRegion();
    }
}
