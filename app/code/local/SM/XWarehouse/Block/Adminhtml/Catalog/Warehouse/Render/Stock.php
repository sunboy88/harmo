<?php
/**
 * Author: Hieunt
 * Date: 3/19/13
 * Time: 6:29 PM
 */

class SM_XWarehouse_Block_Adminhtml_Catalog_Warehouse_Render_Stock extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
        $html = '<input type="text" class="editable" value="'.intval($row->getQty()).'" style="width:50%; display: none;"  />';
        $html .= '<span style="position: absolute;" class="itext">'.intval($row->getQty()).'</span>';

        return $html;
    }
}