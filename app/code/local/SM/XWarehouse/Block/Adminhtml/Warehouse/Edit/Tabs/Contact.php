<?php
/**
 * Author: Hieunt
 * Date: 4/5/13
 * Time: 9:45 AM
 */

class SM_XWarehouse_Block_Adminhtml_Warehouse_Edit_Tabs_Contact extends Mage_Adminhtml_Block_Widget_Form {
    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('ocm_form', array('legend' => Mage::helper('xwarehouse')->__('Contact Person')));

        $fieldset->addField('contact_name', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('Contact Name'),
            'name' => 'contact_name',
        ));

        $fieldset->addField('contact_phone', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('Contact Phone'),
            'name' => 'contact_phone',
            'class' => 'validate-digits'
        ));

        $fieldset->addField('contact_email', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('Contact Email'),
            'name' => 'contact_email',
            'class' => 'validate-email'
        ));

        if (Mage::getSingleton('adminhtml/session')->getWarehouseData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getWarehouseData());
            Mage::getSingleton('adminhtml/session')->setWarehousetData(null);
        } elseif (Mage::registry('warehouse_data')) {
            $form->setValues(Mage::registry('warehouse_data')->getData());
        }
    }
}