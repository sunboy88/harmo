<?php

class SM_XWarehouse_Block_Adminhtml_Warehouse_Edit_Tabs_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        // initial form
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('ocm_form', array('legend' => Mage::helper('xwarehouse')->__('Warehouse information')));

        $fieldset->addField('label', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'label',
        ));

        if (Mage::getSingleton('adminhtml/session')->getWarehouseData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getWarehouseData());
            Mage::getSingleton('adminhtml/session')->setWarehousetData(null);
        } elseif (Mage::registry('warehouse_data')) {
            $form->setValues(Mage::registry('warehouse_data')->getData());
        }

        return parent::_prepareForm();
    }

}