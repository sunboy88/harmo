<?php

class SM_XWarehouse_Block_Adminhtml_Warehouse_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Add jQuery
    */
    public function _prepareLayout() {
        $layout = $this->getLayout();
        Mage::helper('xwarehouse')->addJQuery($layout);
        parent::_prepareLayout();
    }

    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'xwarehouse';
        $this->_controller = 'adminhtml_warehouse';
        
        $this->_updateButton('save', 'label', Mage::helper('xwarehouse')->__('Save'));

        if ($this->_isAllowedAction('delete')) {
            $this->_updateButton('delete', 'label', Mage::helper('xwarehouse')->__('Delete'));
        } else {
            $this->_removeButton('delete');
        }

		if ($this->_isAllowedAction('edit')) {
            $this->_addButton('saveandcontinue', array(
                'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick'   => 'saveAndContinueEdit()',
                'class'     => 'save',
            ), -100);
        } else {
            $this->_removeButton('saveandcontinue');
        }


        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('warehouse_data') && Mage::registry('warehouse_data')->getWarehouseId() ) {
            return Mage::helper('xwarehouse')->__("Edit warehouse '%s'", $this->htmlEscape(Mage::registry('warehouse_data')->getLabel()));
        } else {
            return Mage::helper('xwarehouse')->__('Add warehouse');
        }
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/warehouse/manage/' . $action);
    }
}