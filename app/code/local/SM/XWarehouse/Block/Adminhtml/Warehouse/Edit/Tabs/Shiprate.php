<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Block_Adminhtml_Warehouse_Edit_Tabs_Shiprate extends Mage_Adminhtml_Block_Widget_Form {
    protected function _prepareForm() {
        $form = new Varien_Data_Form(array('enctype' => 'multipart/form-data'));
        $this->setForm($form);

        $fieldset = $form->addFieldset('ocm_form', array('legend' => Mage::helper('xwarehouse')->__('Shipping Table Rate')));

        $conditions = Mage::getModel('adminhtml/system_config_source_shipping_tablerate')->toOptionArray();
        $fieldset->addField('condition', 'select', array(
            'label' => Mage::helper('xwarehouse')->__('Condition'),
            'values' => $conditions,
            'name' => 'contact_name',
        ));

        $fieldset->addField('fileinputname', 'file', array(
            'label'     => Mage::helper('xwarehouse')->__('Import'),
            'required'  => false,
            'name'      => 'fileinputname',
        ));
    }
}