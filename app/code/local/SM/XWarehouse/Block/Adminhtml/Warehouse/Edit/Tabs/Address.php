<?php
/**
 * Author: Hieunt
 * Date: 4/5/13
 * Time: 9:45 AM
 */

class SM_XWarehouse_Block_Adminhtml_Warehouse_Edit_Tabs_Address extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('ocm_form', array('legend' => Mage::helper('xwarehouse')->__('Address')));

        $eventElen = $fieldset->addField('address_type', 'radios', array(
            'label'     => Mage::helper('xwarehouse')->__('Address Type'),
            'name'      => 'address_type',
            'values' => array(
                array('value'=>'physical','label'=>'Physical'),
                array('value'=>'virtual','label'=>'Virtual'),
            ),
            'disabled' => false,
        ));

        $fieldset->addField('street', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('Street'),
            'name' => 'street',
        ));

        $fieldset->addField('postcode', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('PostCode'),
            'name' => 'postcode',
        ));



        $fieldset->addField('city', 'text', array(
            'label' => Mage::helper('xwarehouse')->__('City'),
            'class'     => 'required-entry',
            'required'  => true,
            'name' => 'city',
        ));


        $warehouse = Mage::registry('warehouse_data');
        $collection = Mage::getResourceModel('directory/region_collection');
        $isoCountry = array();
        $isoRegion = array();

        foreach ($collection->getData() as $row) {
            $isoCountry[] = $row['country_id'];
                $isoRegion[$row['country_id']][$row['region_id']] = $row['name'];
        }

        $fieldset->addField('region', 'select', array(
            'label' => Mage::helper('xwarehouse')->__('Region'),
            'values' => isset($isoRegion[$warehouse->getData('country')]) ? $isoRegion[$warehouse->getData('country')] : array() ,
            'name' => 'region',
        ));

        $_countries = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);

        $fieldset->addField('country', 'select', array(
            'label' => Mage::helper('xwarehouse')->__('Country'),
            'values' => $_countries,
            'class'     => 'required-entry',
            'required'  => true,
            'name' => 'country',
            'onchange' => 'getRegion(jQuery(this).val())'
        ));


        $eventElen->setAfterElementHtml('<script type="text/javascript">
            jQuery(document).ready(function(){
                $("address_typevirtual").observe("click", hidePhysicalAddress);
                $("address_typephysical").observe("click", showPhysicalAddress);
                // if warehouse address type was set to virtual before then hide physical address
                if ($("address_typevirtual").checked) {
                    $("warehouse_tabs_address_section").observe("click", hidePhysicalAddress);
                }
                // if creating new warehouse then virtual address was choose as default or editing current warehouse which have virtual as default address type
                if ((!$("address_typephysical").checked)) {
                    $("warehouse_tabs_address_section").observe("click", hidePhysicalAddress);
                    $("address_typevirtual").click();
                }

            });

            function showPhysicalAddress() {
                jQuery(\'input[name="city"]\').parents(\'tr\').show();
                jQuery(\'input[name="city"]\').addClass("required-entry");
                jQuery(\'input[name="street"]\').parents(\'tr\').show();
                jQuery(\'input[name="postcode"]\').parents(\'tr\').show();
                jQuery(\'input[name="region"]\').parents(\'tr\').show();
                jQuery(\'#country\').parents(\'tr\').show();
                jQuery(\'#country\').addClass("required-entry");
            }

            function showRegion(countryId) {

            }

            function textRegion(val) {
                jQuery(\'#region\').parent().empty().append(jQuery(\'<input type="text" name="region" id="region" value="\'+val+\'" class=" input-text" />\'));
            }

            function dropdownRegion(option) {
                jQuery(\'#region\').parent().empty().append(jQuery(\'<select id="region" name="region" class=" select"></select>\'));

                jQuery.each(option, function(key, value) {
                  jQuery(\'#region\').append(jQuery("<option></option>")
                     .attr("value", value).text(key));
                });
            }

            function hidePhysicalAddress() {

                jQuery(\'input[name="city"]\').parents(\'tr\').css({"display":"none"});
                jQuery(\'input[name="city"]\').removeClass("required-entry");
                jQuery(\'input[name="street"]\').parents(\'tr\').hide();
                jQuery(\'input[name="postcode"]\').parents(\'tr\').hide();
                jQuery(\'input[name="region"]\').parents(\'tr\').hide();
                jQuery(\'#country\').parents(\'tr\').hide();
                jQuery(\'#country\').removeClass("required-entry");
            }

        </script>');


        if (Mage::getSingleton('adminhtml/session')->getWarehouseData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getWarehouseData());
            Mage::getSingleton('adminhtml/session')->setWarehousetData(null);
        } elseif (Mage::registry('warehouse_data')) {
            $form->setValues(Mage::registry('warehouse_data')->getData());
        }
    }
}