<?php
/**
 * User: Hieunt
 * Date: 2/20/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */

class SM_XWarehouse_Block_Adminhtml_Warehouse_Switcher extends Mage_Adminhtml_Block_Template {
    protected $_hasDefaultOption = true;
    protected $_warehouseVarName = 'warehouse';

    public function __construct() {
        parent::__construct();
        $this->setUseAjax(true);
        $this->setUseConfirm(false);
        $this->setDefaultWarehouseName($this->__('All Warehouse'));
    }

    public function hasDefaultOption($hasDefaultOption = null)
    {
        if (null !== $hasDefaultOption) {
            $this->_hasDefaultOption = $hasDefaultOption;
        }
        return $this->_hasDefaultOption;
    }

    public function getSwitchUrl()
    {
        return $this->getUrl('*/*/*', array('_current' => true));
    }

    /**
     * get current store id on address
    */
    public function getWarehouseId()
    {
        return $this->getRequest()->getParam($this->_warehouseVarName);
    }

    public function getWarehouses() {
        $helper = Mage::helper('xwarehouse');
        $warehouses = $helper->getAllWarehouse();


        return $warehouses;
    }
}