<?php

class SM_XWarehouse_Block_Adminhtml_Warehouse_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('warehouse_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('xwarehouse')->__('Warehouse'));
    }

    protected function _beforeToHtml() {
        $this->addTab('general_section', array(
            'label' => $this->__('General Information'),
            'title' => $this->__('General Information'),
            'content' => $this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_edit_tabs_form')->toHtml(),
        ));

        $this->addTab('address_section', array(
            'label' => $this->__('Address'),
            'title' => $this->__('Address'),
            'content' => $this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_edit_tabs_address')->toHtml(),
        ));

        $this->addTab('contact_section', array(
            'label' => $this->__('Contact Person'),
            'title' => $this->__('Contact Person'),
            'content' => $this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_edit_tabs_contact')->toHtml(),
        ));

//        $this->addTab('shipping_section', array(
//            'label' => $this->__('Shipping Rate'),
//            'title' => $this->__('Shipping Rate'),
//            'content' => $this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_edit_tabs_shiprate')->toHtml(),
//        ));

        return parent::_beforeToHtml();
    }

}