<?php


class SM_XWarehouse_Block_Adminhtml_Warehouse_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('warehousestockGrid');
        $this->setDefaultSort('warehouse_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
    
    protected function _prepareCollection() {
        $collection = Mage::getModel('xwarehouse/warehouse')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('warehouse_id', array(
            'header' => Mage::helper('xwarehouse')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'warehouse_id',
        ));

        $this->addColumn('label', array(
            'header' => Mage::helper('xwarehouse')->__('Name'),
            'align' => 'left',
            'index' => 'label',
        ));

        $this->addColumn('address_type', array(
            'header' => Mage::helper('xwarehouse')->__('Address Type'),
            'align' => 'left',
            'index' => 'address_type',
        ));

        $this->addColumn('street', array(
            'header' => Mage::helper('xwarehouse')->__('Street'),
            'align' => 'left',
            'index' => 'street',
        ));

        $this->addColumn('postcode', array(
            'header' => Mage::helper('xwarehouse')->__('PostCode'),
            'align' => 'left',
            'index' => 'postcode',
        ));

        $this->addColumn('city', array(
            'header' => Mage::helper('xwarehouse')->__('City'),
            'align' => 'left',
            'index' => 'city',
        ));

        $this->addColumn('region', array(
            'header' => Mage::helper('xwarehouse')->__('Region'),
            'align' => 'left',
            'index' => 'region',
            'renderer' => 'xwarehouse/adminhtml_catalog_warehouse_render_region'
        ));

        $this->addColumn('country', array(
            'header' => Mage::helper('xwarehouse')->__('Country'),
            'align' => 'left',
            'index' => 'country',
        ));

        $this->addColumn('contact_name', array(
            'header' => Mage::helper('xwarehouse')->__('Contact Name'),
            'align' => 'left',
            'index' => 'contact_name',
        ));

        $this->addColumn('contact_phone', array(
            'header' => Mage::helper('xwarehouse')->__('Contact Phone'),
            'align' => 'left',
            'index' => 'contact_phone',
        ));

        $this->addColumn('contact_email', array(
            'header' => Mage::helper('xwarehouse')->__('Contact Email'),
            'align' => 'left',
            'index' => 'contact_email',
        ));



        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        if ($this->_isAllowedAction('edit')) {
            return $this->getUrl('*/*/edit', array('id' => $row->getWarehouseId()));
        }
        return '#';
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('warehouse_id');
        $this->getMassactionBlock()->setFormFieldName('warehouse');
        if ($this->_isAllowedAction('delete')) {
            $this->getMassactionBlock()->addItem('delete', array(
                'label' => Mage::helper('xwarehouse')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('xwarehouse')->__('Are you sure?')
            ));
        }

        $this->getMassactionBlock()->addItem('enableitem', array(
            'label' => Mage::helper('xwarehouse')->__('Enable All Items'),
            'url' => $this->getUrl('*/*/play'),
            'confirm' => Mage::helper('xwarehouse')->__('Are you sure?')
        ));
        return $this;
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/warehouse/manage/' . $action);
    }


}