<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC
 */

class SM_XWarehouse_Block_Adminhtml_Stock_Grid extends Mage_Adminhtml_Block_Widget_Grid { 

    public function __construct() {
        parent::__construct();

        $this->setId('warehouseGrid');
        $this->setDefaultSort('warehouse_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
	
	  protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
        // Only those kind of product could have qty
        $collection->addFieldToFilter('type_id' ,array('in' => array('simple','virtual','downloadable')));

        $collection->getSelect()
            ->join(array('st' => $collection->getTable('xwarehouse/stock')),'e.entity_id = st.product_id');

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');
        }
		
	//	$collection->joinField('store_id', 'catalog_category_product_index', 'store_id', 'product_id=entity_id', '{{table}}.store_id = 6', 'left');
//


		
		
		
		
	
        $this->setCollection($collection);
		 parent::_prepareCollection();
		 
		 $this->getCollection()->addWebsiteNamesToResult();
		 return $this;
		
    }
	
protected function _filterStoreCondition($collection, $column)
{
    if ( !$value = $column->getFilter()->getValue() ) {
        return;
    }

    $this->getCollection()->addStoreFilter($value);
					 $this->getCollection()->groupByAttribute('entity_id');

}
//deepak  add category filter
public function filterCallback($collection, $column)
	{
		$value = $column->getFilter()->getValue();
		$_category = Mage::getModel('catalog/category')->load($value);
		$collection->addCategoryFilter($_category);
		return $collection;
	}


    protected function _prepareColumns() {
        $this->addColumn('id',
            array(
            'header' => Mage::helper('catalog')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
            ));
			
	 $this->addColumn('entity_id',
            array(
            'header' => Mage::helper('catalog')->__('Product Id'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'entity_id',
            ));
        $this->addColumn('name',
            array(
                'header'=> Mage::helper('catalog')->__('Name'),
                'index' => 'name',
            ));
			

/*
if ( !Mage::app()->isSingleStoreMode() ) {
    $this->addColumn('store_id', array(
        'header' => Mage::helper('catalog')->__('Store View'),
        'index' => 'store_id',
        'type' => 'store',
        'store_all' => true,
        'store_view' => true,
        'sortable' => true,
        'filter_condition_callback' => array($this, '_filterStoreCondition'),
    ));
}*/



	$this->addColumn('category', array(
            'header' => Mage::helper('catalog')->__('Category'),
            'align' => 'left',
            'index' => 'sku',
            'width'     => '120',
			'type'  => 'options',
			'options'	=> Mage::getSingleton('xwarehouse/system_config_source_category')->toOptionArray(),
			'renderer' => 'xwarehouse/adminhtml_catalog_warehouse_render_category',
			'filter_condition_callback' => array($this, 'filterCallback'),
        ));
		
		
		//deepak	
		 if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('websites',
                array(
                    'header'=> Mage::helper('catalog')->__('Websites'),
                    'width' => '100px',
                    'sortable'  => false,
                    'index'     => 'websites',
                    'type'      => 'options',
                    'options'   => Mage::getModel('core/website')->getCollection()->toOptionHash(),
            ));
        }

        $this->addColumn('type',
            array(
                'header'=> Mage::helper('catalog')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type'  => 'options',
                'options' => array('simple' => 'Simple Product','virtual' => 'Virtual Product' , 'downloadable' => 'Downloadable Product'),
            ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();

        $this->addColumn('set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
            ));

        $this->addColumn('sku',
            array(
                'header'=> Mage::helper('catalog')->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
            ));

        $validateBarcodeEnabled = Mage::getStoreConfig('barcode/general/enabled');
        if ($validateBarcodeEnabled == 1){
            $this->addColumn('sm_barcode',
                array(
                    'header'=> Mage::helper('catalog')->__('Barcode'),
                    'width' => '100px',
                    'index' => 'sm_barcode',
                    ));
        }

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $this->addColumn('qty',
                array(
                    'header'=> Mage::helper('catalog')->__('Global Stock'),
                    'width' => '100px',
                    'type'  => 'number',
                    'index' => 'qty',
//                    'renderer' => 'xwarehouse/adminhtml_catalog_warehouse_render_stock'
                ));
        }

        $warehouseCollection = Mage::getModel('xwarehouse/warehouse')->getCollection();
        foreach($warehouseCollection as $warehouse) {
            $this->addColumn(strtolower($warehouse->getLabel()),
                array(
                    'header'=> Mage::helper('catalog')->__($warehouse->getLabel()),
                    'width' => '100px',
                    'type'  => 'number',
                    'index' => strtolower($warehouse->getLabel()),
                    'renderer' => 'xwarehouse/adminhtml_catalog_warehouse_render_warehouse'
                ));
        }


        return parent::_prepareColumns();
    }

    /**
     *
    */
    protected function _addColumnFilterToCollection($column) {
        if ($this->getCollection()) {
            $field = ( $column->getFilterIndex() ) ? $column->getFilterIndex() : $column->getIndex();
            
			
			if ($column->getId() == 'websites') {
			
                $this->getCollection()->joinField('websites',
                    'catalog/product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left');;
					
				 $this->getCollection()->groupByAttribute('entity_id');
					
            }
			
			else if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            }
			
			
			 else {
                $isWarehouse = get_class($column->getRenderer()) == 'SM_XWarehouse_Block_Adminhtml_Catalog_Warehouse_Render_Warehouse';

                $cond = $column->getFilter()->getCondition();
                if ($isWarehouse) { // Warehouse qty filter
                    $warehouseLabel = $column->getId();
                    $this->_warehouseQtyFilter($warehouseLabel,$cond);
                } else if($column->getId() == 'id') { // Id Filter
                    $this->_idFilter($cond);
                }
                else {
                    if ($field && isset($cond)) {
                        $this->getCollection()->addFieldToFilter($field , $cond);
                    }
                }
            }
        }
        return $this;

    }

    protected function _warehouseQtyFilter($warehouseLabel,$condition) {
        if (!empty($condition['from']) || !empty($condition['to'])) {
            if (empty($condition['from'])){
                $this->getCollection()->getSelect()
                    ->where('`st`.`'.$warehouseLabel.'` <= '.$condition['to']);
            } elseif (empty($condition['to'])) {
                $this->getCollection()->getSelect()
                    ->where('`st`.`'.$warehouseLabel.'` >= '.$condition['from']);
            } else {
                $this->getCollection()->getSelect()
                ->where('`st`.`'.$warehouseLabel.'` BETWEEN '.$condition['from'].' AND '.$condition['to']);
            }

        }
    }

    protected function _idFilter($condition) {
        if (is_array($condition)) {
            foreach($condition as $con => $value) {
                $this->getCollection()->getSelect()
                    ->where('`st`.`id` '.$con.' '. $value->__toString());
            }
        }
    }

    public function getRowUrl($row) {
        return '';
    }
}