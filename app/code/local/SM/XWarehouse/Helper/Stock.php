<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Helper_Stock {
    public function existInStockTable($productId) {
        $resource = Mage::getSingleton('core/resource');
        $readAdapter = $resource->getConnection('core_read');
        $resource = Mage::getModel('xwarehouse/warehouse')->getResource();

        $select = $readAdapter->select()
            ->from($resource->getTable('xwarehouse/stock'),array('id'))
            ->where('product_id = ?', $productId);
        $id = $readAdapter->fetchOne($select);
        if (!empty($id)) {
            return true;
        }
        return false;
    }
}