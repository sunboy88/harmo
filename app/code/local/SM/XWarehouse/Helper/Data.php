<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hiru
 * Date: 2/19/13
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */ 
class SM_XWarehouse_Helper_Data extends Mage_Core_Helper_Abstract {
	

    public function getAllStock() {
        $result = array();

        /**
         * Since stock model wasn't defined its collection, then we must read directly from db to get all stock
        */
        $resource = new Mage_Core_Model_Resource();
        $read = $resource->getConnection('core_read');
        $select = $read->select()->from('cataloginventory_stock');
        $stocks = $read->fetchAll($select);

        foreach($stocks as $stock) {
            $result[$stock['stock_id']] = $stock['stock_name'];
        }

        return $result;

    }

    public function getAllWarehouse()
    {
        $warehouses = Mage::getModel('xwarehouse/warehouse')->getCollection();
        $result = array();
        foreach($warehouses as $warehouse){
            $result[] = array(
                'value' => $warehouse->getId(),
                'label' => $warehouse->getLabel(),
            );
        }

        return $result;
    }

    /**
     * This function use to return stock information of product in warehouse
    */

    public function getWarehouseItem($pId,$wId) {
        /** @var SM_XWarehouse_Model_Resource_Warehouse_Product_Collection*/
        $collection = Mage::getModel('xwarehouse/warehouse_product')
            ->getCollection()
            ->addProductFilter($pId)
            ->addWarehouseFilter($wId);
        return $collection;
    }
    /**
     *
    */

    public function getWarehouseQty($pId,$wId) {
        /** @var SM_XWarehouse_Model_Resource_Warehouse_Product_Collection*/
        $resource = Mage::getSingleton('core/resource');
        $readAdapter = $resource->getConnection('core_read');
        $resource = Mage::getModel('xwarehouse/warehouse_product')->getResource();
        $select = $readAdapter->select()
            ->from($resource->getTable('xwarehouse/warehouse_product'),array('qty'))
            ->where('warehouse_id = ?', $wId)
            ->where('product_id = ?',$pId);
        $qty = $readAdapter->fetchOne($select);
//		echo 'Default'.$qty;
        if (!empty($select)) {
            return $qty;
        }
        return 0;
    }

    /**
     *
    */

    public function setWarehouseQty($pId,$wId,$qty) {
        /** @var SM_XWarehouse_Model_Resource_Warehouse_Product_Collection*/

        $collection = $this->getWarehouseItem($pId,$wId);
        $item = $collection->getFirstItem();
        if($item->getId()) {
            $item->setEnable(1);
            $item->setQty($qty);
            $item->save();
        }
    }

    public function disableItem($pId,$wId) {
        $collection = $this->getWarehouseItem($pId,$wId);
        $item = $collection->getFirstItem();
        if($item->getId()) {
            $item->setEnable(0);
            $item->save();
        }
    }

    public function enableItem($pId,$wId) {
        $collection = $this->getWarehouseItem($pId,$wId);
        $item = $collection->getFirstItem();
        if($item->getId()) {
            $item->setEnable(1);
            $item->save();
        }
    }


    public function isWarehouseItemEnable($pId,$wId) {
        /** @var SM_XWarehouse_Model_Resource_Warehouse_Product_Collection*/
        $resource = Mage::getSingleton('core/resource');
        $readAdapter = $resource->getConnection('core_read');
        $resource = Mage::getModel('xwarehouse/warehouse_product')->getResource();
        $select = $readAdapter->select()
            ->from($resource->getTable('xwarehouse/warehouse_product'),array('enable'))
            ->where('warehouse_id = ?', $wId)
            ->where('product_id = ?',$pId);
        $enable = $readAdapter->fetchOne($select);
        if (!empty($select)) {
            return $enable;
        }
        return 0;
    }
    /**
     * Filter warehouse for product collection
    */

    public function warehouseFilter($collection,$warehouseId) {
        if (!($collection instanceof Mage_Catalog_Model_Resource_Product_Collection)) {
            return false;
        }
        $warehouse = Mage::getModel('xwarehouse/warehouse')->load($warehouseId);
        if ($warehouse->getId()) {
            $select = $collection->getSelect();
            $warehouse = new Zend_Db_Select($collection->getResource()->getReadConnection());
            $warehouse
                ->from($collection->getResource()->getTable('xwarehouse/warehouse_product'))
                ->where('enable = 1 AND warehouse_id = ?', $warehouseId);

            $select->join(array('w' =>  $warehouse),'w.product_id = e.entity_id');
        }
    }

    public function getWarehouseByLabel($label) {
        $resource = Mage::getSingleton('core/resource');
        $readAdapter = $resource->getConnection('core_read');
        $resource = Mage::getModel('xwarehouse/warehouse')->getResource();
        $select = $readAdapter->select()
            ->from($resource->getTable('xwarehouse/warehouse'),array('warehouse_id'))
            ->where('label = ?', $label);
        $id = $readAdapter->fetchOne($select);
		print_r($array);
        if (!empty($select)) {
            return $id;
        }
        return 0;
    }
    /**
     * sync qty from all warehouse to global stock for specific product
    */
    public function syncQty($pId) {
        $totalQty = 0;
        $resource = Mage::getSingleton('core/resource');
        $zennDb = $this->_getZendDb();

        $select = $zennDb->select()->from($resource->getTableName('xwarehouse/warehouse_product'))
            ->where('enable = 1')
            ->where('product_id = ?',$pId);
        $stm = $zennDb->query($select);
        $collection = $stm->fetchAll();

        foreach ($collection as $item) {
            $totalQty += $item['qty'];
        }

        // Update global stock
        $this->_updateStocks($pId,$totalQty);
    }

    protected function _updateStocks($pId,$qty){
        $resource = Mage::getSingleton('core/resource');
        $writeAdapter = $resource->getConnection('core_write');
        $resource = Mage::getModel('xwarehouse/warehouse')->getResource();

        $sql            = "UPDATE " . $resource->getTable('cataloginventory/stock_item') . " csi,
                       " . $resource->getTable('cataloginventory/stock_status') . " css
                       SET
                       csi.qty = ?,
                       csi.is_in_stock = ?,
                       css.qty = ?,
                       css.stock_status = ?
                       WHERE
                       csi.product_id = ?
                       AND csi.product_id = css.product_id";
        $isInStock      = $qty > 0 ? 1 : 0;
        $stockStatus    = $qty > 0 ? 1 : 0;

        try{
            $result = $writeAdapter->query($sql, array($qty, $isInStock, $qty, $stockStatus, $pId));
        } catch(Exception $e) {
            Mage::log($e->getMessage());
        }
    }

    /**
     * Add clumn name $stockName into table st
    */
    public function addStock($stockName) {

    }

    /**
     * Call this function to add jquery on template. Put in controller to use
    */
    public function addJQuery($layout) {
        $head = $layout->getBlock('head');
        $jqueryPath = 'sm/jquery-1.9.1.js';
        $head->addJs($jqueryPath);
    }

    /**
     * Enable All Item inside a warehouse
     * Return number of record was updated
     */

    public function enableAllItem($warehouseId) {
        $resource = Mage::getSingleton('core/resource');
        $zennDb = $this->_getZendDb();
        // Get All disabled item
        $items = $zennDb->select()
            ->from(array('l' => $resource->getTableName('xwarehouse/warehouse_product')),array('product_id','qty'))
            ->where('warehouse_id = ?', $warehouseId)
            ->where('enable = ?' , 0);
        $stmt = $zennDb->query($items);
        $itemCollection = $stmt->fetchAll();

        $zennDb->update($resource->getTableName('xwarehouse/warehouse_product'),array('enable' => 1),'warehouse_id = '.$warehouseId);
        //Update global

        foreach($itemCollection as $item) {
            $this->syncQty($item['product_id']);
        }
        return count($itemCollection);
    }

    /**
     * Init item for warehouse
     */
    public function initWarehouse($wid) {
        $resource = Mage::getSingleton('core/resource');
        $zennDb = $this->_getZendDb();

        // Get table product warehouse content
        $items = $zennDb->select()
            ->from(array('l' => $resource->getTableName('xwarehouse/warehouse_product')),array('product_id'))
            ->where('warehouse_id = ?', $wid);
        $stmt = $zennDb->query($items);
        $itemCollection = $stmt->fetchAll();

        // get table product content
        $items = $zennDb->select()
            ->from(array('l' => $resource->getTableName('catalog/product')),array('entity_id','type_id'))
            ->where('type_id in (?)', array('in' => array('simple','virtual','downloadable')));
        $stmt = $zennDb->query($items);
        $catalogCollection = $stmt->fetchAll();

        $existItem = array();


        foreach($itemCollection as $item) {
            $existItem[$item['product_id']] = $item['product_id'];
        }
        $sql = '';

        foreach($catalogCollection as $prod) {
            $id = $prod['entity_id'];
            if (!isset($existItem[$id])) {
                $data = array(
                    'product_id'      => $id,
                    'warehouse_id' => $wid,
                    'qty'      => 0,
                );
                $zennDb->insert($resource->getTableName('xwarehouse/warehouse_product'),$data);
            }
        }
    }

    public function _getZendDb() {
        $resource = Mage::getSingleton('core/resource');
        $writeAdapter = $resource->getConnection('core_write');
        $zendDb = Zend_Db::factory('Pdo_Mysql',$writeAdapter->getConfig());
        return $zendDb;
    }
}