<?php
/**
 * Author: Hieunt
 * Date: 3/21/13
 * Time: 4:24 PM
 */
require_once 'Mage/Adminhtml/controllers/Sales/Order/CreateController.php';


class SM_XWarehouse_Adminhtml_Sales_Order_CreateController extends Mage_Adminhtml_Sales_Order_CreateController {
    
    protected function _initSession()
    {
        /**
         * Identify warehouse
         */

        if ($warehouseId = $this->getRequest()->getParam('warehouse_id')) {
            $this->_getSession()->setWarehouseId((int) $warehouseId);
        }
        return parent::_initSession();
    }

    public function saveAction()
    {
        try {
            $this->_processActionData('save');
            if ($paymentData = $this->getRequest()->getPost('payment')) {
                $this->_getOrderCreateModel()->setPaymentData($paymentData);
                $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
            }

            $order = $this->_getOrderCreateModel()
                ->setIsValidate(true)
                ->importPostData($this->getRequest()->getPost('order'))
                ->createOrder();

            $this->_getSession()->clear();
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The order has been created.'));
            $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $this->_getOrderCreateModel()->saveQuote();
            $message = $e->getMessage();
            if( !empty($message) ) {
                Mage::getSingleton('adminhtml/session')->addError($message);
            }
            $this->_redirect('*/*/');
        } catch (Mage_Core_Exception $e){
            $message = $e->getMessage();
            if( !empty($message) ) {
                Mage::getSingleton('adminhtml/session')->addError($message);
            }
            $this->_redirect('*/*/');
        }
        catch (Exception $e){
            $this->_getSession()->addException($e, $this->__('Order saving error: %s', $e->getMessage()));
            $this->_redirect('*/*/');
        }
    }
}