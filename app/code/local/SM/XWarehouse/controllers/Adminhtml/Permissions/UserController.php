<?php
/**
 * Author: Hieunt
 * Date: 4/12/13
 * Time: 10:48 AM
 */
require_once 'Mage/Adminhtml/controllers/Permissions/UserController.php';


class SM_XWarehouse_Adminhtml_Permissions_UserController extends Mage_Adminhtml_Permissions_UserController {


    public function saveAction(){

        $data = $this->getRequest()->getPost();
        $warehouses = $this->getRequest()->getPost('warehouse');
        if (empty($warehouses))  $warehouses = array();

        $edit_user_id = Mage::app()->getRequest()->getParam('user_id');
        if(!empty($edit_user_id) && isset($warehouses)){
            //remove all warehouse involved with this user
            $collection = Mage::getModel('xwarehouse/user')->getCollection();
            $collection->addFieldToFilter('user_id',array('eq' => $edit_user_id));
            foreach($collection as $item) {
                $item->delete();
            }
            // Add new allow warehouses for user
            foreach($warehouses as $wId) {
                $model = Mage::getModel('xwarehouse/user');
                $model->setUserId($edit_user_id);
                $model->setWarehouseId($wId);
                $model->save();
            }
        }

//         Mage::getSingleton('adminhtml/session')->addSuccess("Assigned the warehous");

        unset($data['warehouse']);
        if ($data) {

            $id = $this->getRequest()->getParam('user_id');
            $model = Mage::getModel('admin/user')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This user no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
            $model->setData($data);

            /*
             * Unsetting new password and password confirmation if they are blank
             */
            if ($model->hasNewPassword() && $model->getNewPassword() === '') {
                $model->unsNewPassword();
            }
            if ($model->hasPasswordConfirmation() && $model->getPasswordConfirmation() === '') {
                $model->unsPasswordConfirmation();
            }

            $result = $model->validate();
            if (is_array($result)) {
                Mage::getSingleton('adminhtml/session')->setUserData($data);
                foreach ($result as $message) {
                    Mage::getSingleton('adminhtml/session')->addError($message);
                }
                $this->_redirect('*/*/edit', array('_current' => true));
                return $this;
            }

            try {
                $model->save();
                if ( $uRoles = $this->getRequest()->getParam('roles', false) ) {
                    /*parse_str($uRoles, $uRoles);
                    $uRoles = array_keys($uRoles);*/
                    if ( 1 == sizeof($uRoles) ) {
                        $model->setRoleIds($uRoles)
                            ->setRoleUserId($model->getUserId())
                            ->saveRelations();
                    } else if ( sizeof($uRoles) > 1 ) {
                        //@FIXME: stupid fix of previous multi-roles logic.
                        //@TODO:  make proper DB upgrade in the future revisions.
                        $rs = array();
                        $rs[0] = $uRoles[0];
                        $model->setRoleIds( $rs )->setRoleUserId( $model->getUserId() )->saveRelations();
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The user has been saved.'));

                //Assign warehouse right to user.
                $user_id = $model->getId();
                if( !isset($edit_user_id) && isset($user_id)) {//new user
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection('core_write');
                    if(!empty($warehouses)){
                        foreach($warehouses as $warehouse_id){
                            //Insert new row
                            $query = "INSERT INTO `sm_user_warehouse` (user_id,warehouse_id) VALUES (" . $user_id . ',' . $warehouse_id . ')';
                            $writeConnection->query($query);
                        }
                    }
                }


                Mage::getSingleton('adminhtml/session')->setUserData(false);
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setUserData($data);
                $this->_redirect('*/*/edit', array('user_id' => $model->getUserId()));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function assignwarehouseAction(){
        $data = $this->getRequest()->getPost();
        $warehouses = $data['warehouse'];
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        if(!empty($warehouses)){
            //Remove exist rows: Quick solution.
            $remove_query = "DELETE FROM `sm_user_warehouse` WHERE `user_id` = 1";
            $writeConnection->query($remove_query);
            foreach($warehouses as $warehouse_id){
                //Insert new row
                $query = "INSERT INTO `sm_user_warehouse` VALUE (NULL, " . 1 . ',' . $warehouse_id . ')';
                $results = $writeConnection->query($query);
            }
        }
        //Add msg
        $this->_redirect("*/*/index");
    }


}