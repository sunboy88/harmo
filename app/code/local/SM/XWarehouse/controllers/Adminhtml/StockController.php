<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC
 */

class SM_XWarehouse_Adminhtml_StockController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('catalog/xwarehouse')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Warehouses Stock Manager'), Mage::helper('adminhtml')->__('Warehouses Stock Manager'));

        return $this;
    }

    public function indexAction() {
        $this->checkIsAllow();
        $this->loadLayout();
        $this->_setActiveMenu('catalog/warehouse');
        $this->renderLayout();
    }

    public function checkIsAllow() {
        $active = Mage::helper('smcore')->checkLicense(SM_XWarehouse_Helper_Abstract::PRODUCT_NAME, Mage::getStoreConfig('xwarehouse/general/key'), false);
        if (!Mage::helper('xwarehouse/abstract')->isEnable() || !$active) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('The X-Warehouse module has been disabled or license was wrong. Please enable to use.'));
            $this->_redirect('adminhtml/system_config/edit/section/xwarehouse');
            return;
        }
    }

    public function saveallAction() {
        $stock = $this->getRequest()->getParam('warehouse');
        $helper = Mage::helper('xwarehouse');
        if (!empty($stock))
        foreach($stock as $productId => $value) {
            if (!empty($value))
            foreach($value as $warehouseId => $qty) {
                $collection = $helper->getWarehouseItem($productId,$warehouseId);
                $item = $collection->getFirstItem();
                // That item exits in warehouse item table
                if ($item->getId()) {
                    if ($qty == '-') {
                        $helper->disableItem($productId,$warehouseId);
                    } else {
                        $helper->setWarehouseQty($productId,$warehouseId,$qty);
                    }
                } else {
                    $item = Mage::getModel('xwarehouse/warehouse_product')->load(null);
                    if ($qty == '-') {
                        $item->setEnable(0);
                    } else {
                        $item->setEnable(1);
                    }
                    $item->setProductId($productId);
                    $item->setWarehouseId($warehouseId);
                    $item->setQty($qty);
                    $item->save();
                }

            }
            // Sync warehouse qty to global qty
            $helper->syncQty($productId);
        }

        $this->_redirect('*/*/index');
    }

}