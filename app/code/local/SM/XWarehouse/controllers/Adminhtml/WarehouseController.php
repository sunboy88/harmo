<?php

class SM_XWarehouse_Adminhtml_WarehouseController extends Mage_Adminhtml_Controller_Action {

    // init action
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('catalog/xwarehouse')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Warehouses Manager'), Mage::helper('adminhtml')->__('Warehouses Manager'));

        return $this;
    }

    // edit action
    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('xwarehouse/warehouse')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('warehouse_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('catalog/xwarehouse');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Warehouses Manager'), Mage::helper('adminhtml')->__('Warehouses Manager'));

            $this->_addContent($this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_edit'))
                    ->_addLeft($this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('xwarehouse')->__('Warehouse does not exist'));
            $this->_redirect('*/*/');
        }
    }

    // new action: foward to edit
    public function newAction() {
        $this->_forward('edit');
    }

    // save action
    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            /**
             * Unset address information if user input address information then choose address type virtual
            */
            if ($data['address_type'] == 'virtual') {
                $data['street'] = '';
                $data['region'] = '' ;
                $data['postcode'] = '';
                $data['country'] = '';
                $data['city'] = '';
            }
            $label = $data['label'];
            if (isset($label)) {
                $this->_saveShippingRate();
                $id = $this->getRequest()->getParam('id');
                $warehouses = Mage::getModel('xwarehouse/warehouse')->getCollection();
                foreach($warehouses as $warehouse) {
                    if ($warehouse->getId() == $id) continue;
                    if ($label == $warehouse->getLabel()) {
                        Mage::getSingleton('adminhtml/session')->addError($this->__('Duplicate warehouse name, please use another name for warehouse'));
                        Mage::getSingleton('adminhtml/session')->setFormData($data);
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                        return;
                    }
                }
            }

            $model = Mage::getModel('xwarehouse/warehouse');
            $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));
            try {
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('xwarehouse')->__('Warehouse was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('xwarehouse')->__('Unable to find warehouse to save'));
        $this->_redirect('*/*/');
    }

    /**
     * Delete warehouse then delete product item warehouse also
    */
    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('xwarehouse/warehouse');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Warehouse was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete warehouse then delete product item warehouse also
     */
    public function massDeleteAction() {
        $warehouseIds = $this->getRequest()->getParam('warehouse');
        if (!is_array($warehouseIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select warehouse(s)'));
        } else {
            try {
                foreach ($warehouseIds as $warehouseId) {
                    $warehouse = Mage::getModel('xwarehouse/warehouse')->load($warehouseId);
                    $warehouse->delete();


                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($warehouseIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    // change status for multiple records
    public function massStatusAction() {
        $warehouseIds = $this->getRequest()->getParam('warehouse');
        if (!is_array($warehouseIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select warehouse(s)'));
        } else {
            try {
                foreach ($warehouseIds as $warehouseId) {
                    $warehouseId = Mage::getSingleton('xwarehouse/warehouse')
                            ->load($warehouseId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($warehouseIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function playAction() {
        $warehouseIds = $this->getRequest()->getParam('warehouse');
        $helper = Mage::helper('xwarehouse');

        if (!is_array($warehouseIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select warehouse(s)'));
        } else {
            $record = 0;
            foreach($warehouseIds as $warehouseId) {
                $record += $helper->enableAllItem($warehouseId);
            }
            $this->_getSession()->addSuccess(
                $this->__('Total of %d record(s) were successfully updated', $record)
            );
        }
        $this->_redirect('*/*/index');
    }

    // export to csv-type action
    public function exportCsvAction() {
        $fileName = 'warehouse.csv';
        $content = $this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_grid')
                ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    // export to xml-type action
    public function exportXmlAction() {
        $fileName = 'warehouse.xml';
        $content = $this->getLayout()->createBlock('xwarehouse/adminhtml_warehouse_grid')
                ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    // index action
    public function indexAction() {

        $active = Mage::helper('smcore')->checkLicense(SM_XWarehouse_Helper_Abstract::PRODUCT_NAME, Mage::getStoreConfig('xwarehouse/general/key'), false);
        if (!Mage::helper('xwarehouse/abstract')->isEnable() || !$active) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('The X-Warehouse module has been disabled or license was wrong. Please enable to use.'));
            $this->_redirect('adminhtml/system_config/edit/section/xwarehouse');
            return;
        }

        $this->loadLayout();
        $this->_setActiveMenu('catalog/warehouse');
        $this->renderLayout();
    }

    protected function _saveShippingRate() {
        $data = $this->getRequest()->getPost();
        if(isset($data['fileinputname'])) {
            try {
                $uploader = new Varien_File_Uploader('fileinputname');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything


                $uploader->setAllowRenameFiles(false);

                // setAllowRenameFiles(true) -> move your file in a folder the magento way
                // setAllowRenameFiles(true) -> move your file directly in the $path folder
                $uploader->setFilesDispersion(false);

                $path = Mage::getBaseDir('media') . DS ;

                $uploader->save($path, $data['fileinputname']);

            }catch(Exception $e) {

            }
        }
    }

}