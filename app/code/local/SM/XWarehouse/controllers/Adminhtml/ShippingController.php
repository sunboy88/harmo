<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Adminhtml_ShippingController extends Mage_Adminhtml_Controller_Action {
    public function exportTableratesAction() {
        $fileName   = 'Shiprate.csv';
        /** @var $gridBlock Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid */
        $gridBlock  = $this->getLayout()->createBlock('xwarehouse/adminhtml_shipping_tablerate_grid');
        $website    = Mage::app()->getWebsite($this->getRequest()->getParam('website'));
        if ($this->getRequest()->getParam('conditionName')) {
            $conditionName = $this->getRequest()->getParam('conditionName');
        } else {
            $conditionName = $website->getConfig('carriers/shiprates/condition_name');
        }
        $gridBlock->setWebsiteId($website->getId())->setConditionName($conditionName);
        $content    = $gridBlock->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function getRegionAction() {
        $collection = Mage::getResourceModel('directory/region_collection');
        $countryId = $this->getRequest()->getParam('country');
        $isoRegion = array();

        foreach ($collection->getData() as $row) {
            $isoCountry[] = $row['country_id'];
            if ($countryId == $row['country_id'])
            $isoRegion[$row['name']] = $row['region_id'];
        }

        $this->getResponse()->setBody(json_encode($isoRegion));
    }
}