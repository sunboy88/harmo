<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('xwarehouse/shiprates')};
");

/**
 * Create table 'xwarehouse/shiprates'
 */

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('xwarehouse/shiprates')};
CREATE TABLE {$this->getTable('xwarehouse/shiprates')} (
  `id` int(5) unsigned NOT NULL auto_increment,
  `website_id` int(10) NOT NULL ,
  `warehouse_country_id` varchar(10) NOT NULL DEFAULT '0',
  `warehouse_region_id` int(10) NOT NULL DEFAULT 0,
  `warehouse_zip` varchar(10) NOT NULL DEFAULT '*',
  `dest_country_id` varchar(10) NOT NULL DEFAULT '0',
  `dest_region_id` int(10) NOT NULL DEFAULT 0,
  `dest_zip` varchar(10) NOT NULL DEFAULT '*',
  `condition_name` varchar(99) NOT NULL ,
  `condition_value` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `cost` decimal(12,4) NOT NULL DEFAULT 0.0000,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");