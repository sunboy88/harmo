<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC
 */



$installer = $this;
$installer->startSetup();

$installer->run("DROP TABLE IF EXISTS {$this->getTable('xwarehouse/stock')};
CREATE TABLE {$this->getTable('xwarehouse/stock')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(11) NOT NULL unique,
  `Default` varchar(10) NOT NULL default '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$defaultWarehouse = Mage::getModel('xwarehouse/warehouse')
    ->setLabel('Default')
    ->setAddressType('virtual');
$defaultWarehouse->save();

$collection = Mage::getModel('catalog/product')->getCollection();
$warehouseId = Mage::helper('xwarehouse')->getWarehouseByLabel('Default');

$resource = Mage::getSingleton('core/resource');
$readAdapter = $resource->getConnection('core_read');
$writeAdapter = $resource->getConnection('core_write');
$resource = Mage::getModel('xwarehouse/warehouse')->getResource();
$query = 'INSERT INTO `'.$resource->getTable('xwarehouse/warehouse_product').'` (`product_id`,`warehouse_id`,`qty`,`enable`) VALUES ';
$insert = 'INSERT INTO `'.$resource->getTable('xwarehouse/stock').'` (`product_id` , `Default`) VALUES ';
if (!empty($warehouseId))
{
    $round = 1;

    foreach($collection as $product) {
        // Add product for default warehouse
        if ($round % 500 == 0) {
            $writeAdapter->query(substr($query,0,strlen($query) - 1));
            $writeAdapter->query(substr($insert,0,strlen($insert) - 1));
            $query = 'INSERT INTO `'.$resource->getTable('xwarehouse/warehouse_product').'` (`product_id`,`warehouse_id`,`qty`,`enable`) VALUES ';
            $insert = 'INSERT INTO `'.$resource->getTable('xwarehouse/stock').'` (`product_id` , `Default`) VALUES ';
        }

        $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();

        $query .= '(' . $product->getId() .','. $warehouseId . ','. $qty.', 1 ) ,';
        $insert .= '(' . $product->getId() . ','.$qty.' ) ,';

        // Reach the last one
        if ($round == count($collection)) {
            $writeAdapter->query(substr($query,0,strlen($query) - 1));
            $writeAdapter->query(substr($insert,0,strlen($insert) - 1));
        }
        //Add product for stock table
        $round++;
    }
}

