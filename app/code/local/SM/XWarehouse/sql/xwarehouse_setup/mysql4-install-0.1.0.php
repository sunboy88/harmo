<?php
/**
 * User: Hieunt
 * Date: 2/19/13
 * Time: 2:09 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('xwarehouse/warehouse')};
CREATE TABLE {$this->getTable('xwarehouse/warehouse')} (
  `warehouse_id` int(11) unsigned NOT NULL auto_increment,
  `label` varchar(255) NOT NULL unique,
  `address_type`  varchar(20) NULL,
  `street` varchar(255) NULL,
  `postcode` varchar(255) NULL,
  `city` varchar(255)NULL,
  `region` varchar(255) NULL,
  `country` varchar(255) NULL,
  `contact_name` varchar(255) NOT NULL default '',
  `contact_phone` varchar(255) NOT NULL default '',
  `contact_email` varchar(255) NOT NULL default '',
  `enable` int(1) unsigned NOT NULL default 1,
  PRIMARY KEY (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->run("
DROP TABLE IF EXISTS {$this->getTable('xwarehouse/warehouse_product')};
CREATE TABLE {$this->getTable('xwarehouse/warehouse_product')} (
  `warehouse_product_id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(255) NOT NULL DEFAULT 0,
  `warehouse_id`  int(20) NOT NULL DEFAULT 0,
  `qty` decimal(12,4) NOT NULL DEFAULT 0,
  `enable` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`warehouse_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
ALTER TABLE `{$this->getTable('sales/order_item')}`
ADD `warehouse_id` smallint(5);

ALTER TABLE `{$this->getTable('sales/order')}`
ADD `warehouse_id` smallint(5);
");

$installer->run("
ALTER TABLE `{$this->getTable('core/website')}`
ADD `warehouse` smallint(5);
");


$installer->endSetup();