<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('xwarehouse/user')};
");

$table = $installer->getConnection()
    ->newTable($installer->getTable('xwarehouse/user'))
    ->addColumn('user_warehouse_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Item Id')
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'User Id')
    ->addColumn('warehouse_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Warehouse Id');

$installer->getConnection()->createTable($table);

$installer->run("
ALTER TABLE `{$this->getTable('core/website')}`
ADD `warehouse` smallint(5);
");

$installer->endSetup();
