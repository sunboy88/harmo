<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Model_Adminhtml_System_Config_Backend_Shipping_Shiprates extends Mage_Core_Model_Config_Data {
    public function _afterSave()
    {
        Mage::getResourceModel('xwarehouse/shiprates')->uploadAndImport($this);
    }
}