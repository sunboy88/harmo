<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Model_Shiprates extends  Mage_Core_Model_Abstract {
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/shiprates');
    }
}