<?php
/**
 * Author: Hieunt
 * Date: 4/12/13
 * Time: 10:46 AM
 */

class SM_XWarehouse_Model_User extends Mage_Core_Model_Abstract {
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/user');
    }
}