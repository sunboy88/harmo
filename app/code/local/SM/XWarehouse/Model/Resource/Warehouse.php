<?php

class SM_XWarehouse_Model_Resource_Warehouse extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('xwarehouse/warehouse', 'warehouse_id');
    }
}