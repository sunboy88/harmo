<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC
 */

class SM_XWarehouse_Model_Resource_Stock extends Mage_Core_Model_Resource_Db_Abstract {

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('xwarehouse/stock', 'id');
    }
}