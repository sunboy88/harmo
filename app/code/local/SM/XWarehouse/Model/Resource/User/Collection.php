<?php
/**
 * Author: Hieunt
 * Date: 4/12/13
 * Time: 10:48 AM
 */

class SM_XWarehouse_Model_Resource_User_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/user');
    }
}