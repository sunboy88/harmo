<?php

class SM_XWarehouse_Model_Resource_Warehouse_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/warehouse');
    }

}