<?php

class SM_XWarehouse_Model_Resource_Warehouse_Product_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/warehouse_product');
    }
    
    public function addProductFilter($productId)
    {
        $this->addFilter('product_id', $productId);
        return $this;
    }
    public function addWarehouseFilter($warehouseId)
    {
        $this->addFilter('warehouse_id', $warehouseId);
        return $this;
    }        
}