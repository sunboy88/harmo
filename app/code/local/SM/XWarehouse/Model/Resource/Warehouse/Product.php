<?php


class SM_XWarehouse_Model_Resource_Warehouse_Product extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('xwarehouse/warehouse_product', 'warehouse_product_id');
    }
}