<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Model_Resource_Stock_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/stock');
    }
}