<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Model_Resource_Shiprates extends  Mage_Core_Model_Resource_Db_Abstract {

    /**
     * Import table rates website ID
     *
     * @var int
     */
    protected $_importWebsiteId     = 0;

    /**
     * Errors in import process
     *
     * @var array
     */
    protected $_importErrors        = array();

    /**
     * Count of imported table rates
     *
     * @var int
     */
    protected $_importedRows        = 0;

    /**
     * Array of unique table rate keys to protect from duplicates
     *
     * @var array
     */
    protected $_importUniqueHash    = array();

    /**
     * Array of countries keyed by iso2 code
     *
     * @var array
     */
    protected $_importIso2Countries;

    /**
     * Array of countries keyed by iso3 code
     *
     * @var array
     */
    protected $_importIso3Countries;

    /**
     * Associative array of countries and regions
     * [country_id][region_code] = region_id
     *
     * @var array
     */
    protected $_importRegions;

    /**
     * Import Table Rate condition name
     *
     * @var string
     */
    protected $_importConditionName;

    /**
     * Array of condition full names
     *
     * @var array
     */
    protected $_conditionFullNames  = array();

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('xwarehouse/shiprates', 'id');
    }

    public function uploadAndImport(Varien_Object $object) {
        if (empty($_FILES['groups']['tmp_name']['shiprates']['fields']['import']['value'])) {
            return $this;
        }

        $csvFile = $_FILES['groups']['tmp_name']['shiprates']['fields']['import']['value'];
        $website = Mage::app()->getWebsite($object->getScopeId());

        $this->_importWebsiteId     = (int)$website->getId();
        $this->_importUniqueHash    = array();
        $this->_importErrors        = array();
        $this->_importedRows        = 0;

        $io     = new Varien_Io_File();
        $info   = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        // check and skip headers
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 5) {
            $io->streamClose();
            Mage::throwException(Mage::helper('shipping')->__('Invalid Table Rates File Format'));
        }

        if ($object->getData('groups/warehouse/fields/condition_name/inherit') == '1') {
            $conditionName = (string)Mage::getConfig()->getNode('default/carriers/shiprates/condition_name');
        } else {
            $conditionName = $object->getData('groups/shiprates/fields/condition_name/value');
        }
        $this->_importConditionName = $conditionName;

        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();

        try {
            $rowNumber  = 1;
            $importData = array();

            $this->_loadDirectoryCountries();
            $this->_loadDirectoryRegions();

            // delete old data by website and condition name
            $condition = array(
                'website_id = ?'     => $this->_importWebsiteId,
                'condition_name = ?' => $this->_importConditionName
            );
            $adapter->delete($this->getMainTable(), $condition);

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;

                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);
                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            $this->_saveImportData($importData);
            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('shipping')->__('An error occurred while import table rates.'));
        }

        $adapter->commit();

        if ($this->_importErrors) {
            $error = Mage::helper('shipping')->__('File has not been imported. See the following list of errors: %s', implode(" \n", $this->_importErrors));
            Mage::throwException($error);
        }

        return $this;
    }

    /**
     * Load directory countries
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryCountries()
    {
        if (!is_null($this->_importIso2Countries) && !is_null($this->_importIso3Countries)) {
            return $this;
        }

        $this->_importIso2Countries = array();
        $this->_importIso3Countries = array();

        /** @var $collection Mage_Directory_Model_Resource_Country_Collection */
        $collection = Mage::getResourceModel('directory/country_collection');
        foreach ($collection->getData() as $row) {
            $this->_importIso2Countries[$row['iso2_code']] = $row['country_id'];
            $this->_importIso3Countries[$row['iso3_code']] = $row['country_id'];
        }

        return $this;
    }

    /**
     * Load directory regions
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryRegions()
    {
        if (!is_null($this->_importRegions)) {
            return $this;
        }

        $this->_importRegions = array();

        /** @var $collection Mage_Directory_Model_Resource_Region_Collection */
        $collection = Mage::getResourceModel('directory/region_collection');
        foreach ($collection->getData() as $row) {
            $this->_importRegions[$row['country_id']][$row['code']] = (int)$row['region_id'];
        }

        return $this;
    }

    /**
     * Return import condition full name by condition name code
     *
     * @param string $conditionName
     * @return string
     */
    protected function _getConditionFullName($conditionName)
    {
        if (!isset($this->_conditionFullNames[$conditionName])) {
            $name = Mage::getSingleton('shipping/carrier_tablerate')->getCode('condition_name_short', $conditionName);
            $this->_conditionFullNames[$conditionName] = $name;
        }

        return $this->_conditionFullNames[$conditionName];
    }

    /**
     * Validate row for import and return table rate array or false
     * Error will be add to _importErrors array
     *
     * @param array $row
     * @param int $rowNumber
     * @return array|false
     */
    protected function _getImportRow($row, $rowNumber = 0)
    {
        // validate row
        if (count($row) < 8) {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Table Rates format in the Row #%s', $rowNumber);
            return false;
        }

        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }

        // validate warehouse country
        if (isset($this->_importIso2Countries[$row[0]])) {
            $warehouseCountryId = $this->_importIso2Countries[$row[0]];
        } elseif (isset($this->_importIso3Countries[$row[0]])) {
            $warehouseCountryId = $this->_importIso3Countries[$row[0]];
        } elseif ($row[0] == '*' || $row[0] == '') {
            $warehouseCountryId = '0';
        } else {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Warehouse Country "%s" in the Row #%s.', $row[0], $rowNumber);
            return false;
        }

        // validate warehouse region
        if ($warehouseCountryId != '0' && isset($this->_importRegions[$warehouseCountryId][$row[1]])) {
            $warehouseRegionId = $this->_importRegions[$warehouseCountryId][$row[1]];
        } elseif ($row[1] == '*' || $row[1] == '') {
            $warehouseRegionId = 0;
        } else {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Warehouse Region/State "%s" in the Row #%s.', $row[1], $rowNumber);
            return false;
        }

        // detect zip code
        if ($row[2] == '*' || $row[2] == '') {
            $warehouseZipCode = '*';
        } else {
            $warehouseZipCode = $row[2];
        }

        // validate destination country
        if (isset($this->_importIso2Countries[$row[3]])) {
            $countryId = $this->_importIso2Countries[$row[3]];
        } elseif (isset($this->_importIso3Countries[$row[3]])) {
            $countryId = $this->_importIso3Countries[$row[3]];
        } elseif ($row[3] == '*' || $row[3] == '') {
            $countryId = '0';
        } else {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Destination Country "%s" in the Row #%s.', $row[3], $rowNumber);
            return false;
        }

        // validate destination region
        if ($countryId != '0' && isset($this->_importRegions[$countryId][$row[4]])) {
            $regionId = $this->_importRegions[$countryId][$row[4]];
        } elseif ($row[4] == '*' || $row[1] == '') {
            $regionId = 0;
        } else {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Destination Region/State "%s" in the Row #%s.', $row[1], $rowNumber);
            return false;
        }

        // detect zip code
        if ($row[5] == '*' || $row[5] == '') {
            $zipCode = '*';
        } else {
            $zipCode = $row[5];
        }

        // validate condition value
        $value = $this->_parseDecimalValue($row[6]);
        if ($value === false) {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid %s "%s" in the Row #%s.', $this->_getConditionFullName($this->_importConditionName), $row[3], $rowNumber);
            return false;
        }

        // validate price
        $price = $this->_parseDecimalValue($row[7]);
        if ($price === false) {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Shipping Price "%s" in the Row #%s.', $row[4], $rowNumber);
            return false;
        }

        // protect from duplicate
        $hash = sprintf("%s-%d-%s-%s-%d-%s-%F", $warehouseCountryId , $warehouseRegionId , $warehouseZipCode , $countryId, $regionId, $zipCode, $value);
        if (isset($this->_importUniqueHash[$hash])) {
            $this->_importErrors[] = Mage::helper('shipping')->__('Duplicate Row #%s (Warehouse Country "%s", Warehouse Region/State "%s", Warehouse Zip "%s" ,Destination Country "%s", Destination Region/State "%s", Destination Zip "%s" and Value "%s").', $rowNumber, $row[0], $row[1], $warehouseZipCode, $row[3], $row[4], $zipCode, $value);
            return false;
        }
        $this->_importUniqueHash[$hash] = true;

        return array(
            $this->_importWebsiteId,    // website_id
            $warehouseCountryId,        // warehouse country id
            $warehouseRegionId,         // warehouse region id
            $warehouseZipCode,          // warehouse zipcode
            $countryId,                 // dest_country_id
            $regionId,                  // dest_region_id,
            $zipCode,                   // dest_zip
            $this->_importConditionName,// condition_name,
            $value,                     // condition_value
            $price                      // price
        );
    }

    /**
     * Save import data batch
     *
     * @param array $data
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _saveImportData(array $data)
    {
        if (!empty($data)) {
            $columns = array('website_id', 'warehouse_country_id', 'warehouse_region_id', 'warehouse_zip', 'dest_country_id', 'dest_region_id', 'dest_zip',
                'condition_name', 'condition_value', 'price');
            $this->_getWriteAdapter()->insertArray($this->getMainTable(), $columns, $data);
            $this->_importedRows += count($data);
        }

        return $this;
    }

    protected function _parseDecimalValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        $value = (float)sprintf('%.4F', $value);
        if ($value < 0.0000) {
            return false;
        }
        return $value;
    }

    /**
     * Return table rate array or false by rate request
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return array|boolean
     */
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        $adapter = $this->_getReadAdapter();
        $currentWebsite = Mage::app()->getWebsite()->getId();
        //For backend
        if ($currentWebsite == 0) {
            $warehouseId = Mage::app()->getRequest()->getParam('warehouse_id');
        } else {
            $warehouseId = Mage::getModel('core/website')->load($request->getWebsiteId())->getWarehouse();
        }
        $warehouse = Mage::getModel('xwarehouse/warehouse')->load($warehouseId);

        if (!$warehouse->getId()) {
            return;
        }

        $bind = array(
            ':website_id' => 0,
            ':w_country_id' => $warehouse->getCountry(),
            ':w_region_id' => (int) $warehouse->getRegion(),
            ':w_postcode' => $warehouse->getPostcode(),
            ':country_id' => $request->getDestCountryId(),
            ':region_id' => (int) $request->getDestRegionId(),
            ':postcode' => $request->getDestPostcode()
        );
        $select = $adapter->select()
            ->from($this->getMainTable())
//            ->where('website_id = :website_id')
            ->order(array('condition_value  DESC'))
            ->limit(1);

        // Render destination condition
        $orWhere = '(' . implode(') OR (', array(
                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = :postcode",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = '*'",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = :postcode",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = '*'",

                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = '*' AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = '*'",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = '0' AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = '*'",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = '0' AND warehouse_zip = '*' AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = '*'",

                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = '*' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = :postcode",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = '0' AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = :postcode",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = '0' AND warehouse_zip = '*' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = :postcode",

                "warehouse_country_id = :w_country_id AND warehouse_region_id = :w_region_id AND warehouse_zip = '*' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = '*'",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = '0' AND warehouse_zip = :w_postcode AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = '*'",
                "warehouse_country_id = :w_country_id AND warehouse_region_id = '0' AND warehouse_zip = '*' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_zip = '*'",

                "warehouse_country_id = '0' AND warehouse_region_id = '0' AND warehouse_zip = '*' AND dest_country_id = '0' AND dest_region_id = :region_id  AND dest_zip = '*'",
                "warehouse_country_id = '0' AND warehouse_region_id = :w_region_id AND warehouse_zip = '*' AND dest_country_id = '0' AND dest_region_id = :region_id  AND dest_zip = '*'",

                "warehouse_country_id = '0' AND warehouse_region_id = '0' AND warehouse_zip = '*' AND dest_country_id = '0' AND dest_region_id = '0' AND dest_zip = '*'",
                "warehouse_country_id = '0' AND warehouse_region_id = :w_region_id AND warehouse_zip = '*' AND dest_country_id = '0' AND dest_region_id = '0' AND dest_zip = '*'",

            )) . ')';
        $select->where($orWhere);

        // Render condition by condition name
        if (is_array($request->getConditionName())) {
            $orWhere = array();
            $i = 0;
            foreach ($request->getConditionName() as $conditionName) {
                $bindNameKey  = sprintf(':condition_name_%d', $i);
                $bindValueKey = sprintf(':condition_value_%d', $i);
                $orWhere[] = "(condition_name = {$bindNameKey} AND condition_value <= {$bindValueKey})";
                $bind[$bindNameKey] = $conditionName;
                $bind[$bindValueKey] = $request->getData($conditionName);
                $i++;
            }

            if ($orWhere) {
                $select->where(implode(' OR ', $orWhere));
            }
        } else {
            $bind[':condition_name']  = $request->getConditionName();
            $bind[':condition_value'] = $request->getData($request->getConditionName());

            $select->where('condition_name = :condition_name');
            $select->where('condition_value <= :condition_value');
        }

        $result = $adapter->fetchRow($select, $bind);
        // Normalize destination zip code
        if ($result && $result['dest_zip'] == '*') {
            $result['dest_zip'] = '';
        }
        return $result;
    }
}