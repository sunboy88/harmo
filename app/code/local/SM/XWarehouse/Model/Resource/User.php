<?php
/**
 * Author: Hieunt
 * Date: 4/12/13
 * Time: 10:47 AM
 */

class SM_XWarehouse_Model_Resource_User extends Mage_Core_Model_Resource_Db_Abstract {
    public function _construct()
    {
        $this->_init('xwarehouse/user', 'user_warehouse_id');
    }
}