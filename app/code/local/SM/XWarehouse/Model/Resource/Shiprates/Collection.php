<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Model_Resource_Shiprates_Collection extends  Mage_Core_Model_Resource_Db_Collection_Abstract {
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/shiprates');
    }

    public function setWebsiteFilter($websiteId)
    {
        return $this->addFieldToFilter('website_id', $websiteId);
    }

    /**
     * Add condition name (code) filter to collection
     *
     * @param string $conditionName
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate_Collection
     */
    public function setConditionFilter($conditionName)
    {
        return $this->addFieldToFilter('condition_name', $conditionName);
    }

    /**
     * Add country filter to collection
     *
     * @param string $countryId
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate_Collection
     */
    public function setCountryFilter($countryId)
    {
        return $this->addFieldToFilter('dest_country_id', $countryId);
    }
}