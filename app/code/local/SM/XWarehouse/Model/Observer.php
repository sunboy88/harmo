<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hiru
 * Date: 2/19/13
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */

class SM_XWarehouse_Model_Observer {

    /**
     * Flag to stop observer executing more than once
     *
     * @var static bool
     */
    static protected $_singletonFlag = false;


    /**
     * Save warehouse data when saving product
    */
    public function saveProductTabData(Varien_Event_Observer $observer) {
        if (!self::$_singletonFlag) {
            self::$_singletonFlag = true;
            
            $product = $observer->getEvent()->getProduct();
            $complexType = array('configurable','bundle','group');

            // if product is complex type then dont care about qty
            if (in_array($product->getData('type_id'),$complexType)) {
//                $this->_createItemForComplexProduct($product->getId());
            } else {
                $helperStock = Mage::helper('xwarehouse/stock');
                if (!$helperStock->existInStockTable($product->getId())) {
                    $stockItem = Mage::getModel('xwarehouse/stock')->load(null);
                    $stockItem->setData('product_id',$product->getId());
                    $stockItem->save();
                }
                try{
                    $values = $this->_getRequest()->getPost('warehouse');
                    if ($values && is_array($values)) {
                        $totalQty = 0;
                        foreach ($values as $warehouseId => $item) {
                            if (!$warehouseId) continue;
                            if (empty($item['disable'])) $item['disable'] = 0;


                            $collection = Mage::helper('xwarehouse')->getWarehouseItem($product->getId(),$warehouseId);
                            if (count($collection) > 0) {
                                // update existing row

                                $wItem = $collection->getFirstItem();
                                if (isset($item['qty'])) {
                                    $wItem->setQty($item['qty']);
                                }
                                $wItem->setEnable($item['disable'] == 1 ? 0 : 1);
                                $wItem->save();
                            } else {
                                // create new row
                                $warehouse = Mage::getModel('xwarehouse/warehouse')->load($warehouseId);
                                if ($warehouse->getId()) {
                                    Mage::getModel('xwarehouse/warehouse_product')
                                        ->setData(array(
                                            'product_id' => $product->getId(),
                                            'qty' => isset($item['qty']) ? $item['qty'] : 0 ,
                                            'warehouse_id' => $warehouseId,
                                            'warehouse_short_name' => $warehouse->getShortName(),
                                            'enable' => $item['disable'] == 1 ? 0 : 1
                                        ))->save();

                                }
                            }

                            /**
                             * Enable warehouse item will increase root qty
                             */
                            if ($item['disable'] == 0 && $item['qty'] > 0) {
                                $totalQty += isset($item['qty']) ? $item['qty'] : 0;
                            }

                        }

                        // reset default inventory stock
                        $helper = Mage::helper('xwarehouse');
                        $helper->syncQty($product->getId());

                    }
                }
                catch(Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
    }

    /**
     * Delete product also:
     * Delete warehouse product item
     * Delete Stock information - in table sm_stock
    */
    public function deleteProductAfter($observer) {
        $helper = Mage::helper('xwarehouse');
        $product = $observer->getProduct();
        //Delete warehouse product item
        $collection = Mage::getModel('xwarehouse/warehouse_product')->getCollection();
        $collection->addFieldToFilter('product_id',array('eq' => $product->getId()));

        foreach($collection as $item) {
            $item->delete();
        }
        // Delete stock information

        $stockCollection = Mage::getModel('xwarehouse/stock')->getCollection();
        $stockCollection->addFieldToFilter('product_id',array('eq' => $product->getId()));

        foreach($stockCollection as $item) {
            $item->delete();
        }
    }
    /**
     * @todo: order on multi-warehouse at same time
     * Saving warehouse id to order information
    */
    public function orderSaveBefore($observer) {
        $order = $observer->getOrder();
        $warehouseId = $this->getWarehouseId();
        if (!empty($warehouseId))
            $order->setWarehouseId($warehouseId);
    }

    /**
     * Update warehouse item 
    */
    public function orderSaveAfter($observer) {
        if (!is_null(Mage::registry('wh_result'))) {
            $result =  Mage::registry('wh_result');
            
            //Add by Phu, Nguyen Manh
            Mage::log('SM_XWarehouse_Model_Observer orderSaveAfter');
            //Mage::log($result);  
	        
	        //$order = $observer->getOrder();
	        //$warehouseId = $order->getWarehouseId();
            //Mage::log('SM_XWarehouse_Model_Observer orderSaveAfter warehouseId 1=' . $warehouseId);
            
            $warehouseId = 1;//Default is FR warehouse
	        
            foreach($result as $item) {
            	if($item['wid']!='') {
            		$warehouseId = $item['wid']; 
            	}else{
			        $usStores = array(4,14,15,16,17,18,19,20);
			        if (in_array(Mage::app()->getStore()->getStoreId(),$usStores)) $warehouseId = 2;
            	}
	            Mage::log('SM_XWarehouse_Model_Observer orderSaveAfter $item-wid=[' .$item['wid']. '] warehouseId=' . $warehouseId);
	            
                $collection = Mage::helper('xwarehouse')->getWarehouseItem($item['pid'],$warehouseId);
                $wProduct = $collection->getFirstItem();
                
                Mage::log('SM_XWarehouse_Model_Observer orderSaveAfter wProduct [' .$wProduct->getId(). '] QTY before='.$wProduct->getQty());
                
                if ($wProduct->getId()) {
                    $wProduct->setQty($wProduct->getQty() - $item['qty']); 
                    $wProduct->save();
                }

                Mage::log('SM_XWarehouse_Model_Observer orderSaveAfter wProduct QTY after='.$wProduct->getQty()); 
            }
            Mage::unregister('wh_result');
        }

    }

    /**
     * Saving warehouse_id to order item information
     */

    public function orderItemSaveBefore($observer) {
        $item = $observer->getItem();
        $warehouseId = $this->getWarehouseId();
        if (!empty($warehouseId))
        $item->setWarehouseId($warehouseId);
    }

    /**
     * Return qty to warehouse when canceling order
    */
    public function cancelOrderItem($observer) {
        $item = $observer->getEvent()->getItem();

        $children = $item->getChildrenItems();
        $qty = $item->getQtyOrdered() - max($item->getQtyShipped(), $item->getQtyInvoiced()) - $item->getQtyCanceled();
        $warehouseId = $item->getWarehouseId();

        if ($item->getId() && ($productId = $item->getProductId()) && empty($children) && $qty && !is_null($warehouseId)) {
            $collection = Mage::helper('xwarehouse')->getWarehouseItem($productId,$warehouseId);
            $pWarehouse = $collection->getFirstItem();
            if ($pWarehouse->getId()) {
                $pWarehouse->setQty($pWarehouse->getQty() + $qty);
                $pWarehouse->save();
            }
        }

        return $this;
    }

    /**
     * Return warehouse qty when order refund
    */

    public function refundOrderInventory($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $items = array();
        foreach ($creditmemo->getAllItems() as $item) {
            $return = false;
            if ($item->hasBackToStock()) {
                if ($item->getBackToStock() && $item->getQty()) {
                    $return = true;
                }
            } elseif (Mage::helper('cataloginventory')->isAutoReturnEnabled()) {
                $return = true;
            }

            if ($return) {
                $orderItem = $item->getOrderItem();
                $warehouseId = $orderItem->getWarehouseId();
                $productId = $orderItem->getProductId();
                if (!empty($warehouseId) && !(empty($productId))) {
                    $warehouseItem = Mage::helper('xwarehouse')->getWarehouseItem($productId,$warehouseId);
                    $warehouseItem = $warehouseItem->getFirstItem();
                    if ($warehouseItem->getId()) {
                        $parentOrderId = $item->getOrderItem()->getParentItemId();
                        /* @var $parentItem Mage_Sales_Model_Order_Creditmemo_Item */
                        $parentItem = $parentOrderId ? $creditmemo->getItemByOrderId($parentOrderId) : false;
                        $qty = $parentItem ? ($parentItem->getQty() * $item->getQty()) : $item->getQty();
                        $warehouseItem->setQty($warehouseItem->getQty() + $qty);
                        $warehouseItem->save();
                    }
                }
            }
        }
    }

    /**
     *
    */
    public function websiteForm($observer) {
        if (!Mage::helper('xwarehouse/abstract')->isEnable()) {
            return;
        }

        $block = $observer->getBlock();
        if (Mage::registry('store_type') == 'website') {
            $websiteModel = Mage::registry('store_data');
            $showWebsiteFieldset = true;
        } elseif (Mage::registry('store_type') == 'group') {
            $showWebsiteFieldset = $showStoreFieldset = false;
        } elseif (Mage::registry('store_type') == 'store') {
            $showWebsiteFieldset = $showGroupFieldset = false;
        }
        $form = $block->getForm();

        $fieldset = $form->getElement('website_fieldset');

        $warehouses = Mage::getModel('xwarehouse/warehouse')->getCollection();
        $values = array();
        $values[] = '';
        if (count($warehouses) > 0) {
            foreach($warehouses as $warehouse) {
                $values[$warehouse->getId()] = $warehouse->getName();
            }
        }
        if ($showWebsiteFieldset) {
            if ($postData = Mage::registry('store_post_data')) {
                $websiteModel->setData($postData['website']);
            }


            $fieldset->addField('warehouse', 'select', array(
                'name'      => 'website[warehouse]',
                'label'     => Mage::helper('core')->__('Warehouse'),
                'values'    => $values,
                'value'     => $websiteModel->getWarehouse(),
                'required'  => true,
            ));
        }

    }


    public function getWarehouseId() {
        if (!Mage::helper('xwarehouse/abstract')->isEnable()) {
            return false;
        }

        $session = $this->_getSession();
        if (Mage::app()->getStore()->isAdmin()) {
            return $session->getWarehouseId();
        }
        /**
         * Frontend
         */
        $warehouseId = $website = Mage::app()->getWebsite()->getWarehouse();
        if (!empty($warehouseId)) {
            return $warehouseId;
        }
        return false;
    }

    protected function _getRequest() {
        return Mage::app()->getRequest();
    }

    public function productCollectionLoadBefore($observer) {
        $collection = $observer->getCollection();
        //Get current website's warehouse id
        $warehouseId = Mage::app()->getWebsite()->getData('warehouse');
        $showOutOfStock = Mage::getStoreConfig('cataloginventory/options/show_out_of_stock');

        if (!empty($warehouseId)) {
            $select = $collection->getSelect();
            $warehouse = new Zend_Db_Select($collection->getResource()->getReadConnection());
            if ($showOutOfStock) {
                $warehouse
                    ->from($collection->getResource()->getTable('xwarehouse/warehouse_product'))
                    ->where('enable = 1 AND warehouse_id = ?', $warehouseId);
            } else {
                $warehouse
                    ->from($collection->getResource()->getTable('xwarehouse/warehouse_product'))
                    ->where('enable = 1 AND qty > 0 AND warehouse_id = ?', $warehouseId);
            }

            $select->joinLeft(array('w' =>  $warehouse),'w.product_id = e.entity_id');

        }

        $collection->clear();
    }

    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session_quote');
    }

    /**
     * Create new warehouse will add more column for table stock
    */

    public function xwarehouseWarehouseSaveBefore($observer) {
        $warehouse = $observer->getWarehouse();

        if (!$warehouse->getId()) {
            $resource = Mage::getSingleton('core/resource');
            $writeAdapter = $resource->getConnection('core_write');
            $resource = Mage::getModel('xwarehouse/warehouse')->getResource();
            $zendDb = Zend_Db::factory('Pdo_Mysql',$writeAdapter->getConfig());

            $zendDb->query("ALTER TABLE `".$resource->getTable('xwarehouse/stock')."` ADD COLUMN `".strtolower($warehouse->getLabel())."` varchar(99) NOT NULL default '0' COMMENT 'HELLO' ");
        } else {
            $warehouse = Mage::getModel('xwarehouse/warehouse')->load($warehouse->getId());
            $name = $warehouse->getLabel();
            if (!empty($name)) {
                Mage::register('warehouse_name',$name);
            }
        }
    }

    /**
     * Update table stock
     * Update global qty
     */

    public function warehouseProductSaveAfter($observer) {
        $item = $observer->getItem();
        if ($item->getEnable() == 1) {
            $qty = $item->getQty();
        }
        else {
            $qty = '-';
        }
        $warehouse = Mage::getModel('xwarehouse/warehouse')->load($item->getWarehouseId());
        // warehouse item is newly created

        $resource = Mage::getSingleton('core/resource');
        $readAdapter = $resource->getConnection('core_write');
        $resource = Mage::getModel('xwarehouse/warehouse_product')->getResource();
        $readAdapter->update($resource->getTable('xwarehouse/stock'),array(strtolower($warehouse->getLabel()) => $qty),'product_id = '.$item->getProductId());


    }

    /**
     * Delete from table stock
     */
    public function deleteWarehouseProductBefore($observer) {
        $warehouseItem = $observer->getItem();

        $pId = $warehouseItem->getProductId();
        $product = Mage::getModel('catalog/product')->load($pId);
        // update root stock
        if ($product->getId()) {
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
            $stock->setQty($stock->getQty() - ($warehouseItem->getQty() >= 0 ? $warehouseItem->getQty() : 0));
            $stock->save();
            Mage::getSingleton('cataloginventory/stock_status')
                ->updateStatus($product->getId());
        }
    }

    /**
     * Delete warehouse then delete stock column, warehouse item also
     */

    public function xwarehouseWarehouseDeleteAfter($observer) {
        $warehouse = $observer->getWarehouse();

        if (!empty($warehouse)) {
            $resource = Mage::getSingleton('core/resource');
            $writeAdapter = $resource->getConnection('core_write');
            $resource = Mage::getModel('xwarehouse/warehouse')->getResource();
            try {
            $zendDb = Zend_Db::factory('Pdo_Mysql',$writeAdapter->getConfig());

            $zendDb->query("ALTER TABLE ".$resource->getTable('xwarehouse/stock')." DROP COLUMN ".strtolower($warehouse->getLabel())." ");
            } catch(Exception $e) {
                Mage::getSingleton('admin/session')->addError('Can not delete warehouse');
                return;
            }

            // Delete warehouse product item
            $collection = Mage::getModel('xwarehouse/warehouse_product')->getCollection();
            $warehouseId = $warehouse->getId();
            $collection->addFieldToFilter('warehouse_id',array('eq' => $warehouseId));
            foreach($collection as $warehouseItem) {
                // delete warehouse item from database
                $warehouseItem->delete();
            }
        }
    }

    // Warehouse rename then chane name in table stock also
    public function xwarehouseWarehouseSaveAfter($observer) {
        $warehouse = $observer->getWarehouse();
        $name = Mage::registry('warehouse_name');
        if (!empty($name)) {
            if ($warehouse->getLabel() != $name) {
                $resource = Mage::getSingleton('core/resource');
                $writeAdapter = $resource->getConnection('core_write');
                $resource = Mage::getModel('xwarehouse/warehouse')->getResource();
                $zendDb = Zend_Db::factory('Pdo_Mysql',$writeAdapter->getConfig());

                $zendDb->query("ALTER TABLE  ".$resource->getTable('xwarehouse/stock')." CHANGE  `".strtolower($name)."`  `".strtolower($warehouse->getLabel())."` VARCHAR(99)");
            }
        }

        Mage::helper('xwarehouse')->initWarehouse($warehouse->getId());
    }
}