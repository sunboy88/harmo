<?php
/**
 * Author: Hieunt
 * Date: 4/4/13
 * Time: 1:59 PM
 */ 
class SM_XWarehouse_Model_Override_Catalog_Product extends Mage_Catalog_Model_Product {

    public function isAvailable()
    {
        $collection = Mage::getModel('xwarehouse/warehouse_product')->getCollection();
        $warehouseId = Mage::app()->getWebsite()->getData('warehouse');
        $isSalable = $this->getTypeInstance(true)->isSalable($this) ? $this->getTypeInstance(true)->isSalable($this)
            : Mage::helper('catalog/product')->getSkipSaleableCheck();
        if ($isSalable) {
            if (empty($warehouseId)) {
                return parent::isAvailable();
            }
            // Asking DB directly for better performance
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $select = $readConnection->select()
                ->from($collection->getTable('xwarehouse/warehouse_product'))
                ->where('product_id = :product_id')
                ->where('warehouse_id = :warehouse_id');

            $bind = array(
                'product_id'      => $this->getId(),
                'warehouse_id'    => $warehouseId
            );
            $data = $readConnection->fetchAll($select, $bind);
            $isSalable = $this->getTypeInstance(true)->isSalable($this);
            if (!empty($data)) {
                if ($data[0]['enable'] != 1 || $data[0]['qty'] <= 0) {
                    return false;
                }
            }


        }

        return $isSalable;
    }

    public function getSession() {
        return Mage::getSingleton('core/session');
    }

}