<?php
/**
 * Author: Hieunt
 * Date: 4/4/13
 * Time: 11:48 AM
 */ 
class SM_XWarehouse_Model_Override_Catalog_Category extends Mage_Catalog_Model_Category {

    /**
     * Get category products collection
     *
     * @return Varien_Data_Collection_Db
     */
    public function getProductCollection()
    {
        if (!Mage::helper('xwarehouse/abstract')->isEnable()) {
            return parent::getProductCollection();
        }

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($this->getStoreId())
            ->addCategoryFilter($this);
        $warehouseId = Mage::app()->getWebsite()->getData('warehouse');
        if (!empty($warehouseId)) {
            $select = $collection->getSelect();

            $warehouse = new Zend_Db_Select($collection->getResource()->getReadConnection());
            $warehouse
                ->from($collection->getResource()->getTable('xwarehouse/warehouse_product'))
                ->where('enable = 1 AND warehouse_id = ?', $warehouseId);

            $select->join(array('w' =>  $warehouse),'w.product_id = e.entity_id');
        }
        return $collection;
    }
}