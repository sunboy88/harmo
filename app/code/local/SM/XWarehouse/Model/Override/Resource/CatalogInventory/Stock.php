<?php
/**
 * Author: Hieunt
 * Date: 4/16/13
 * Time: 3:28 PM
 */ 
class SM_XWarehouse_Model_Override_Resource_CatalogInventory_Stock extends Mage_CatalogInventory_Model_Resource_Stock {
    /**
     * Correct particular stock products qty based on operator
     *
     * @param Mage_CatalogInventory_Model_Stock $stock
     * @param array $productQtys
     * @param string $operator +/-
     * @return Mage_CatalogInventory_Model_Resource_Stock
     */
    public function correctItemsQty($stock, $productQtys, $operator = '-')
    {
        if (empty($productQtys)) {
            return $this;
        }
        $adapter = $this->_getWriteAdapter();
        $conditions = array();
        $warehouseId = $this->getWarehouseId();
        $unNameAble = array();
        foreach ($productQtys as $productId => $qty) {
            // Check if warehouse qty is bellow zero then totals quantity will not change
            if (!empty($warehouseId)) {
                $warehouseQty = Mage::helper('xwarehouse')->getWarehouseQty($productId,$warehouseId);
                if ($warehouseQty < $qty) {
                    continue;
                }
            }
            $unNameAble[$productId] = $qty;
        }
        if (empty($unNameAble)) {
            return $this;
        }



        foreach ($unNameAble as $productId => $qty) {
            $case = $adapter->quoteInto('?', $productId);
            $result = $adapter->quoteInto("qty{$operator}?", $qty);
            $conditions[$case] = $result;
        }

        $value = $adapter->getCaseSql('product_id', $conditions, 'qty');

        $where = array(
            'product_id IN (?)' => array_keys($unNameAble),
            'stock_id = ?'      => $stock->getId()
        );

        $adapter->beginTransaction();
        $adapter->update($this->getTable('cataloginventory/stock_item'), array('qty' => $value), $where);
        $adapter->commit();

        return $this;
    }

    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session_quote');
    }

    public function getWarehouseId() {
        if (!$this->isModuleEnable()) {
            return false;
        }

        $session = $this->_getSession();
        if (Mage::app()->getStore()->isAdmin()) {
            return $session->getWarehouseId();
        }
        /**
         * Frontend
         */
        $warehouseId = $website = Mage::app()->getWebsite()->getWarehouse();
        if (!empty($warehouseId)) {
            return $warehouseId;
        }
        return false;

    }

    public function isModuleEnable() {
        return Mage::helper('xwarehouse/abstract')->isEnable();
    }
}