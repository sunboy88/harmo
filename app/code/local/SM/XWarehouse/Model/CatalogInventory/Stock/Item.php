<?php
/**
 * Author: Hieunt
 * Date: 3/21/13
 * Time: 6:51 PM
 */ 
class SM_XWarehouse_Model_CatalogInventory_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item {

    static $totalItemInQuote = 0;
    public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0) {
        if (!$this->isModuleEnable()) {
            return parent::checkQuoteItemQty($qty, $summaryQty, $origQty);
        }

        $result = parent::checkQuoteItemQty($qty, $summaryQty, $origQty);

        if (!$this->getWarehouseId()) {
            return  $result;
        }
        $wId = $this->getWarehouseId();


        if ($errorCode = $result->getErrorCode()) {
            if (in_array($errorCode,array('qty_max','qty_min'))) {
                return $this;
            }

        }
        $result->addData($this->checkQtyIncrements($qty)->getData());
        if ($result->getHasError()) {
            return $result;
        }

        if (!$this->getManageStock()) {
            return $result;
        }
        // Out of root's stock
        if (!$this->getIsInStock()) {
            return $result;
        }

        if (!$this->checkWarehouseQty($this->getWarehouseId(),$qty) || !$this->checkWarehouseQty($this->getWarehouseId(),$summaryQty)) {
            $message = Mage::helper('cataloginventory')->__('%s is out of current warehouse stock, please try to take order in another warehouse', $this->getProductName());

            $result->setHasError(true)
                ->setMessage($message)
                ->setQuoteMessage($message)
                ->setQuoteMessageIndex('qty');
        } else if(!$this->checkQty($summaryQty) || !$this->checkQty($qty)){
            return $result;
        } else {
            if (($this->getWarehouseQty($wId) - $summaryQty) < 0) {
                if ($this->getQty() - $summaryQty < 0) {
                    return $result;
                } else {
                    if ($this->getIsChildItem()) {
                        $backorderQty = ($this->getWarehouseQty($wId) > 0) ? ($summaryQty - $this->getWarehouseQty($wId)) * 1 : $qty * 1;
                        if ($backorderQty > $qty) {
                            $backorderQty = $qty;
                        }

//                        $result->setItemBackorders($backorderQty);
                    } else {
                        $orderedItems = $this->getOrderedItems();
                        $itemsLeft = ($this->getWarehouseQty($wId) > $orderedItems) ? ($this->getWarehouseQty($wId) - $orderedItems) * 1 : 0;
                        $backorderQty = ($itemsLeft > 0) ? ($qty - $itemsLeft) * 1 : $qty * 1;

                        if ($backorderQty > 0) {
//                            $result->setItemBackorders($backorderQty);
                        }
//                        $this->setOrderedItems($orderedItems + $qty);
                    }

                    if ($this->getBackorders() == Mage_CatalogInventory_Model_Stock::BACKORDERS_YES_NOTIFY) {
                        if (!$this->getIsChildItem()) {
                            $result->setMessage(
                                Mage::helper('cataloginventory')->__('This product is not available in the requested quantity. %s of the items will be backordered.', ($backorderQty * 1))
                            );
                        } else {
                            $result->setMessage(
                                Mage::helper('cataloginventory')->__('"%s" is not available in the requested quantity. %s of the items will be backordered.', $this->getProductName(), ($backorderQty * 1))
                            );
                        }
                    } elseif (Mage::app()->getStore()->isAdmin()) {
                        $result->setMessage(
                            Mage::helper('cataloginventory')->__('The requested quantity for "%s" is not available.', $this->getProductName())
                        );
                    }

                    if (!$this->getIsChildItem()) {
                        $this->setOrderedItems($qty + (int)$this->getOrderedItems());
                    }
                }
            }
        }

        return $result;
    }


    function checkWarehouseQty($wId,$qty) {
		$configValue = Mage::getStoreConfig('cataloginventory/item_options/backorders',Mage::app()->getStore());
        if($configValue != 0){
            return true;
        }
        $warehouseQty = Mage::helper('xwarehouse')->getWarehouseQty($this->getProductId(),$wId);
        if ($qty <= $warehouseQty) {
            return true;
        }
        return false;
    }

    public function getWarehouseQty($wId) {
    	if ($qty = $this->_getSession()->getWarehouseQty()) {
            return $qty;
        }
        $warehouseQty = Mage::helper('xwarehouse')->getWarehouseQty($this->getProductId(),$wId);
        if (isset($warehouseQty)) {
	        if ($warehouseQty) {
	            $this->_getSession()->setWarehouseQty($warehouseQty);
	            return $warehouseQty;
	        }
        }
        return false;
    }

    /**
     * Deduced stock of warehouse when order placed
    */

    public function subtractQty($qty)
    {
        //Mage::log('deohieu');
        //Mage::log($this->canSubtractQty());
        if ($this->canSubtractQty() || Mage::app()->getStore()->isAdmin() ) {
            if (!is_null(Mage::registry('wh_result'))) {
                $result = Mage::registry('wh_result');                
                Mage::unregister('wh_result');
            }
            else {
                $result = array();
            }
            $result[] = array(
                'pid' => $this->getProductId(),
                'qty' => $qty,
                'wid' => $this->getWarehouseId()
            );
            Mage::log($result);   
            Mage::register('wh_result',$result);
        }
        parent::subtractQty($qty);
        return $this;

    }

    public function getWarehouseId() {
        // Mage::log(A1); 
        // if (!$this->isModuleEnable()) {
        //     return false;
        // }
        // Mage::log(A1); 

        $session = $this->_getSession();
        if (Mage::app()->getStore()->isAdmin()) {
            return $session->getWarehouseId();
        }
        // Mage::log(A1); 
        /**
         * Frontend
        */
        
        $warehouseId = 1;
        $usStores = array(4,14,15,16,17,18,19,20);
        if (in_array(Mage::app()->getStore()->getStoreId(),$usStores)) $warehouseId = 2;
        //$warehouseId = $website = Mage::app()->getWebsite()->getWarehouse();
               
        if (!empty($warehouseId)) {
            return $warehouseId;
        }
        return false;

    }

    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session_quote');
    }

    public function isModuleEnable() {
        return Mage::helper('xwarehouse/abstract')->isEnable();
    }

}