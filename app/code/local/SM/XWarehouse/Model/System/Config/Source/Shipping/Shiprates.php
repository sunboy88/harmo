<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 */

class SM_XWarehouse_Model_System_Config_Source_Shipping_Shiprates {
    public function toOptionArray()
    {
        $tableRate = Mage::getSingleton('xwarehouse/carrier_shiprates');
        $arr = array();
        foreach ($tableRate->getCode('condition_name') as $k=>$v) {
            $arr[] = array('value'=>$k, 'label'=>$v);
        }
        return $arr;
    }
}