<?php

class SM_XWarehouse_Model_Warehouse extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'xwarehouse_warehouse';

    protected $_eventObject = 'warehouse';

    protected $_isNew = false;

    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/warehouse');
    }

    public function getName() {
        return $this->getData('label');
    }
}