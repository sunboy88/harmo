<?php
/**
 * Author: HieuNT
 * Email: hieunt@smartosc.com
 * Company: SmartOSC
 */

class SM_XWarehouse_Model_Stock extends Mage_Core_Model_Abstract {
    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/stock');
    }
}