<?php

class SM_XWarehouse_Model_Warehouse_Product extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix      = 'warehouse_product';
    protected $_eventObject      = 'item';

    public function _construct()
    {
        parent::_construct();
        $this->_init('xwarehouse/warehouse_product');
    }
}