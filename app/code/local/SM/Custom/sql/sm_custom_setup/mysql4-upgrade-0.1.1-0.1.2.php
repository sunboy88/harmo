<?php
/**
 * User: Hieunt
 * Date: 2/19/13
 * Time: 2:09 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
try {
$installer = $this;

$installer->startSetup();

$installer->run("
UPDATE {$this->getTable('cataloginventory/stock_item')}
SET `qty`=0
WHERE 1=1;
");

$installer->run("
UPDATE {$this->getTable('cataloginventory/stock_status')}
SET `qty`=0
WHERE 1=1;
");

$installer->run("
UPDATE {$this->getTable('cataloginventory/stock_status_indexer_idx')}
SET `qty`=0
WHERE 1=1;
");

$installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}