<?php
/**
 * User: Hieunt
 * Date: 2/19/13
 * Time: 2:09 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
try {
$installer = $this;

$installer->startSetup();

$installer->run("
UPDATE {$this->getTable('xwarehouse/stock')}
SET `france`=0, `usa`=0
WHERE 1=1;
");

$installer->run("
UPDATE {$this->getTable('xwarehouse/warehouse_product')}
SET `qty`=0
WHERE 1=1;
");

$installer->endSetup();

} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}