<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kazu
 * Date: 04/11/2013
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */ 
class SM_Custom_Block_CatalogSearch_Advanced_Form extends Mage_CatalogSearch_Block_Advanced_Form {
    public function getSearchUrl()
    {
        return $this->getUrl('catalogsearch/advanced/originalresult');
    }
}