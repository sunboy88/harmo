<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kazu
 * Date: 04/11/2013
 * Time: 15:10
 * To change this template use File | Settings | File Templates.
 */
class SM_Custom_AdvancedController extends Mage_Core_Controller_Front_Action {
    public function originalresultAction(){
        $this->loadLayout();
        try {
            Mage::getSingleton('catalogsearch/advanced')->addFilters($this->getRequest()->getQuery());
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('catalogsearch/session')->addError($e->getMessage());
            $this->_redirectError(
                Mage::getModel('core/url')
                    ->setQueryParams($this->getRequest()->getQuery())
                    ->getUrl('*/*/')
            );
        }
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }
}