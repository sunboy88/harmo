<?php

class SM_Custom_Model_Procart_Product_Type_Configurable extends GoMage_Procart_Model_Product_Type_Configurable
{

    public function getSelectedAttributesInfo($product = null)
    {
        $attributes = parent::getSelectedAttributesInfo($product);
        if (!preg_match("/onepage/i", Mage::app()->getStore()->getCurrentUrl(false))) {
            if (!Mage::registry('gomage_procart_render_attributes')) {
                return $attributes;
            }
        }
        // Certainly, item value is html
        foreach($attributes as $index => $attribute) {
            $attributes[$index]['isHtml'] = true;
        }
        return $attributes;
    }
}