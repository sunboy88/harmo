<?php
class Crmore_Advanceshipping_Helper_Data extends Mage_Core_Helper_Abstract
{
	const INSURANCE_ACTIVED = 'advanceshipping/insurance/is_actived';
	const DOUBLE_SHIPPING_ACTIVED = 'advanceshipping/double_shipping/is_actived';
	
	const INSURANCE_RATE = 'advanceshipping/insurance/rate';
	const INSURANCE_AMOUNT_MINIMUM='advanceshipping/insurance/minimum_fee';
	const INSURANCE_INCLUDE_TAX = 'advanceshipping/insurance/tax';
	const INSURANCE_FEE_BASE_ON = 'advanceshipping/insurance/product_tax_class';
	const DISPLAY_INSURANCE_FEE = 'advanceshipping/insurance/display_insurance_fee';
	
	protected $_insurance_rate = 0;
	protected $_insurance_fee_minimum=0;
	protected $_insurance_include_tax = true;
	
	protected $_calculator = null;
    /**
     * Retrieve checkout session model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Retrieve checkout quote model object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }

	public function isDoubleShippingNeeded()
	{
		$quote = $this->getCheckout()->getQuote();
		$items = $quote->getAllVisibleItems();
		        $quoteArr = $quote->toArray();
		$count = 0;
		$helper=Mage::helper('catalog/product_configuration');
		if(count($items))
		{
			/*@var $_item Mage_Sales_Model_Quote_Item*/
			foreach ($items as $_item)
			{
				/*@var $_product Mage_Catalog_Model_Product*/
				$_product = Mage::getModel('catalog/product')->setStoreId($quote->getStoreId())->load($_item->getProductId());
				if(!$_product->isVirtual())
				{
					if($_product->isConfigurable())
					{
						$option=$_item->getOptionByCode('simple_product');
						if($option){
							$_product=$option->getProduct();
						}
					}
					if($this->isOutOfStock($_product))
					{
						$count++;
					}
				}
				
			}
		}
		if($count>0 && $count<count($items))
		{
			return true;
		}
		return false;
	}
    
    public function isDoubleShipping()
    {
        $quote = $this->getCheckout()->getQuote();
        $items = $quote->getAllVisibleItems();
        $instock = 0;
        $total = count($items);
        if($total)
        {
            /*@var $_item Mage_Sales_Model_Quote_Item*/
            foreach ($items as $_item)
            {
                /*@var $_product Mage_Catalog_Model_Product*/
                $_product = Mage::getModel('catalog/product')->setStoreId($quote->getStoreId())->load($_item->getProductId());
                if(!$_product->isVirtual())
                {
                    if($_product->isConfigurable())
                    {
                        $option=$_item->getOptionByCode('simple_product');
                        if($option){
                            $_product=$option->getProduct();
                        }
                    }
                    $stock = $this->isProductInOfStock($_product);
                    if(0 < $stock)
                    {
                        // if customer buy items larger than stock of the product, double shipping is enable
                        if($_item->getQty() > $stock) {
                            return true;
                        }
                        $instock++;
                    }
                }
                else {
                    $instock++;
                }
            }
        }
        if(1 <= $instock && $instock < $total && 2 <= $total)
        {
            return true;
        }
        return false;
    }

	private function isOutOfStock(Mage_Catalog_Model_Product $_product)
	{
		
		/*@var $stock Mage_Cataloginventory_Model_Stock_Item */
		$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);//->getIsOutOfStock();
		$stock_status = $stock->getIsInStock();
		if($stock_status===true)
		{
			return false;
		}
		$stock_status=intval($stock_status);
		if($stock_status==0)
		{
			return true;
		}		
		return false;
	}
    
    private function isProductInOfStock(Mage_Catalog_Model_Product $_product)
    {
        /*
        * Calculator Inventory of product
        */
        $_productId = $_product->getId();
        $warehouseId = Mage::app()->getWebsite()->getData('warehouse');
        /**
         * Get the resource model
         */
        $resource = Mage::getSingleton('core/resource');
        /**
         * Retrieve the read connection
         */
        $readConnection = $resource->getConnection('core_read');
        /**
         * Get the table name sm_product_warehouses
         */
        $tableName = $resource->getTableName('xwarehouse/warehouse_product');
        $select = $readConnection->select()
                                ->from($tableName,array('qty'))
                                ->where('product_id = ?',$_productId)
                                ->where('warehouse_id =?',$warehouseId);
        $query = (string) $select;
        $result = $readConnection->fetchOne($query);

        return (int) $result;
    }
    
	public function isDisplayIncludeTax()
	{
		return Mage::getStoreConfig(self::DISPLAY_INSURANCE_FEE);
	}
	public function getInsuranceAmountIncludeTax($quote=null,$isDouble=false)
	{
		if(is_null($quote))
		{
			$quote=$this->getQuote();
		}
		$result = $this->getAmountAndTaxIncluded($quote,$isDouble);
		if($this->isDisplayIncludeTax())
		{		
			return $result['amount']+$result['tax_amount'];
		}

		return $result['amount'];
	}
	public function getInsuranceAmount($quote=null,$isDouble=false)
	{
		if(is_null($quote))
		{
			$quote=$this->getQuote();
		}
		$result = $this->getAmountAndTaxIncluded($quote,$isDouble);		
		
		
		$minimum = $this->getMinimumInsuranceFee();
		
		//Edit by Phu, Nguyen Manh. Compare with minimum insurance
		if($minimum > $result['amount']){
			return $minimum;
		}else{
			return $result['amount'];
		}
		//End
	}
	protected function getInsuranceRate()
	{
		return doubleval(Mage::getStoreConfig(self::INSURANCE_RATE));
	}
	protected function getMinimumInsuranceFee()
	{
		return doubleval(Mage::getStoreConfig(self::INSURANCE_AMOUNT_MINIMUM));
	}
	public function isDoubleShippingEnabled()
	{
		return Mage::getStoreConfig(self::DOUBLE_SHIPPING_ACTIVED);
	}
	public function isInsuranceEnabled()
	{
		return Mage::getStoreConfig(self::INSURANCE_ACTIVED);
	}
	public function getConfiguredProductTaxClassId()
	{
		return intval(Mage::getStoreConfig(self::INSURANCE_FEE_BASE_ON));
	}
	public function getDoubleShippingSelected()
	{
		$shipping_multiple = $this->taxBaseOn($this->getQuote())->getShippingMultiple();
		if(!$shipping_multiple){
			$shipping_multiple=1;
		}
		return $shipping_multiple;	
	}
	public function getInsuranceSelected()
	{
		return $this->taxBaseOn($this->getQuote())->getInsurance();
	}
	public function isIncludedTax()
	{
		return intval(Mage::getStoreConfig(self::INSURANCE_INCLUDE_TAX));
	}
	
	protected function getTaxAmount(Mage_Sales_Model_Quote $quote,$productTaxClassId,$amount)
	{
		$includedTax = $this->isIncludedTax();
		if(is_null($this->_calculator)){ $this->_calculator=Mage::getSingleton('tax/calculation');}
		$taxAmount=0;
		$baseTaxAmount=0;
		
		$calc = $this->_calculator;
		$addressTaxRequest  = $calc->getRateRequest(
			$quote->getShippingAddress(),
			$quote->getBillingAddress(),
			$quote->getCustomerTaxClassId(),
			$quote->getStore()
		);
		$addressTaxRequest->setProductClassId($productTaxClassId);
		$rate          = $calc->getRate($addressTaxRequest);
        if($includedTax) {
            $baseTaxAmount = $calc->calcTaxAmount($amount, $rate, true, true);
        }
        else {
            $baseTaxAmount = 0;
        }


		$taxAmount     = $quote->getStore()->convertPrice($baseTaxAmount, false);
		if($includedTax)
		{
			$amount = $amount-$taxAmount;
		}
				
		$result= array('amount'=>$amount,'tax_amount'=>$taxAmount,'base_tax_amount'=>$baseTaxAmount);
		return $result;		
	}
	public function getAmountAndTaxIncluded(Mage_Sales_Model_Quote $quote,$isDouble=false)
	{
		$items = $quote->getAllVisibleItems();
		
		$arr = array();
		$configured_tax = $this->getConfiguredProductTaxClassId();
		$needTax = true;
		if($configured_tax===0)
		{
			$needTax=false;
		}
		$getTaxFromProduct = false;
		if($configured_tax==-1){
			$getTaxFromProduct=true;
			$arr[$configured_tax]=0;
		}
		foreach ($items as $_item)
		{
			/*@var $_item Mage_Sales_Model_Quote_Item*/
			$_product = Mage::getModel('catalog/product')->setStoreId($quote->getStoreId())->load($_item->getProductId());
			if(!$_product->isVirtual())
			{
				$_price = doubleval($_item->getBaseRowTotal());
				if($getTaxFromProduct){
					$tax_class_id = $_product->getTaxClassId();
					if($tax_class_id)
					{					
						if(!isset($arr[$tax_class_id])){
							$arr[$tax_class_id]=$_price;
						}else{
							$arr[$tax_class_id]+=$_price;
						}
					}
				}else{
					$arr[$configured_tax]+=$_price;
				}
			}
		}
		$result = array('amount'=>0,'tax_amount'=>0,'base_tax_amount'=>0);
		if(count($arr)){
			$rate = $this->getInsuranceRate();
			$rate = $rate/100;
			foreach ($arr as $taxClassId=>$amount)
			{
				$amount = $amount*$rate;
				$r = $this->getTaxAmount($quote, $taxClassId, $amount);
				$result['amount']+=$r['amount'];
				$result['tax_amount']+=$r['tax_amount'];
				$result['base_tax_amount']+=$r['base_tax_amount'];
			}
		}
		$minimum = $this->getMinimumInsuranceFee();
		if($isDouble)
		{
			foreach($result as $key=>$val)
			{
				$result[$key] = $val*2;
			}
			$minimum=$minimum*2;
		}

        if($minimum > ($result['amount'] + $result['tax_amount'])) {
            $result=$this->updateMinimumFee($quote, $arr, $minimum);
        }

		return $result;
	}
	public function updateMinimumFee($quote,$taxes,$minimum=2)
	{
		$result = array('amount'=>0,'tax_amount'=>0,'base_tax_amount'=>0);
		foreach($taxes as $taxClassId => $a)
		{
			$r = $this->getTaxAmount($quote,$taxClassId,$minimum);
			if(($r['amount']+$r['tax_amount'])>= $minimum)
			{
				$result['amount']=$r['amount'];
				$result['tax_amount']=$r['tax_amount'];
				$result['base_tax_amount']=$r['base_tax_amount'];
			}
		}
		return $result;
	}
	public function taxBaseOn($quote)
	{
		$basedOn = Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_BASED_ON, $quote->getStore());
		switch ($basedOn) {
			case 'billing':
				$address = $quote->getBillingAddress();
				break;
			case 'shipping':
				$address = $quote->getShippingAddress();
				break;
			default:
				$address = $quote->getShippingAddress();
				break;
		}
		return $address;
	}
}
?>