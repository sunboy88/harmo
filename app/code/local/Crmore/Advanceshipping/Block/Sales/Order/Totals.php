<?php
class Crmore_Advanceshipping_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals
{
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
    	parent::_initTotals();
    	
        $source = $this->getSource();

        /**
         * Add store rewards
         */
        			if (((float)$source->getInsuranceAmount()) != 0) {
	        			$label = $this->__('Insurance Fee');
			            $newTotals = new Varien_Object(array(
			                'code'  => 'insurance_amount',
			                'field' => 'insurance_amount',
			                'value' => $source->getInsuranceAmount(),
			                'label' => $label
			            ));
			            $this->addTotalBefore($newTotals,'grand_total');
        			}
        
        return $this;
    }
}
