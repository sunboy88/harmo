<?php
class Crmore_Advanceshipping_Block_Adminhtml_Sales_Totals extends Mage_Adminhtml_Block_Sales_Totals
{
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
    	parent::_initTotals();
    	
        $source = $this->getSource();

        /**
         * Add store rewards
         */
        //$totals = $this->_totals;
        //$newTotals = array();
        //if (count($totals)>0) {
        	//foreach ($totals as $index=>$arr) {
        		//if ($index == "grand_total") {
        			//if (((float)$this->getSource()->getInsuranceAmount())!= 0) {
	        			$label = $this->__('Insurence Fee');
			            $newTotals['insurance_amount'] = new Varien_Object(array(
			                'code'  => 'insurance_amount',
			                'field' => 'insurance_amount',
			                'value' => 5,//$source->getInsuranceAmount(),
							'base_value'=> 5,//$source->getBaseInsuranceAmount(),
			                'label' => $label
			            ));
        			//}
					
        		//}
        		//$newTotals[$index] = $arr;
        	//}
        	//$this->_totals = $newTotals;
        //}

        return $this;
    }
}
