<?php

/**
 * @category   Crmore
 * @package    Crmore_Advanceshipping
 */
class Crmore_Advanceshipping_Model_Sales_Quote_Address_Total_Insuranceamount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	protected $_calculator=null;
    const INSURANCE = 'advanceshipping/insurance/is_actived';
	
    public function __construct()
    {
        $this->setCode('insurance_amount');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        $this->_setAmount(0);
        $this->_setBaseAmount(0);

        $is_insurance_enabled = Mage::getStoreConfig(self::INSURANCE);
        $isInsurance = 0;
        if ($is_insurance_enabled) {
            $isInsurance = $address->getInsurance();
        }

        $isDoubleShipping = ($address->getShippingMultiple() == 2) ? true : false;
        $taxAmount = 0;
        $baseTaxAmount = 0;
        $insuranceAmount = 0;
        if ($isInsurance) {
            $result = Mage::helper('advanceshipping')->getAmountAndTaxIncluded($address->getQuote(), $isDoubleShipping);
            $taxAmount = $result['tax_amount'];
            $baseTaxAmount = $result['base_tax_amount'];
            $insuranceAmount = $result['amount'];
        }
        
        $quote = $address->getQuote();
        $address->setInsuranceTaxAmount($taxAmount);
        $address->setBaseInsuranceTaxAmount($baseTaxAmount);
        $address->setInsuranceAmount($insuranceAmount);
        $address->setBaseInsuranceAmount($quote->getStore()->convertPrice($insuranceAmount, false));
        $address->addTotalAmount('insurance_amount', $insuranceAmount);
        $address->addBaseTotalAmount('insurance_amount', $quote->getStore()->convertPrice($insuranceAmount, false));

        $this->_setAmount($insuranceAmount);
        $this->_setBaseAmount($insuranceAmount);

        if ($isInsurance) {
            $address->setExtraTaxAmount($address->getExtraTaxAmount() + $taxAmount);
            $address->setBaseExtraTaxAmount($address->getBaseExtraTaxAmount() + $baseTaxAmount);
//            $address->addTotalAmount('tax', $taxAmount);
//            $address->addBaseTotalAmount('tax', $baseTaxAmount);
            $rates = array();
            foreach ($address->getAppliedTaxes() as $rate) {
                $rate['amount'] = $rate['amount'] + $taxAmount;
                $rate['base_amount'] = $rate['base_amount'] + $baseTaxAmount;
                $rates[] = $rate;
            }
            $address->setAppliedTaxes($rates);
        }
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getInsuranceAmount();
        $includedTax = Mage::helper('advanceshipping')->isDisplayIncludeTax();
        if($includedTax)
        {
        	$amount+=$address->getInsuranceTaxAmount();
        }
        if (($amount!=0)) { 
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('advanceshipping')->__('Insurance Fee:'),
                'full_info' => array(),
                'value' => $amount,
            ));
        }
        return $this;
    }
	public function getLabel()
	{
	    return Mage::helper('advanceshipping')->__('Insurance Fee..:');
	}
}