<?php

class Crmore_Advanceshipping_Model_Sales_Order_Creditmemo_Total_Insuranceamount extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setInsuranceAmount(0);
        $creditmemo->setBaseInsuranceAmount(0);
		$creditmemo->setShippingMultiple(0);
        $amount = $creditmemo->getOrder()->getInsuranceAmount();
        $creditmemo->setInsuranceAmount($amount);
        
        $amount = $creditmemo->getOrder()->getBaseInsuranceAmount();
        $creditmemo->setBaseInsuranceAmount($amount);
        
		$shippingoption = $creditmemo->getOrder()->getShippingMultiple(); 
		$creditmemo->setShippingMultiple($shippingoption);

        $quoteId = $creditmemo->getOrder()->getQuoteId();
        $quote = Mage::getModel("sales/quote")->getCollection()
            ->addFieldToFilter('entity_id', array('eq'=>$quoteId))
            ->getFirstItem();
        $allAddresses = $quote ? $quote->getAllAddresses() : array();
        foreach ($allAddresses as $index => $address) {
            if($address->getAddressType() != Mage_Sales_Model_Quote_Address::TYPE_SHIPPING) {
                unset($allAddresses[$index]);
            }
        }
        $allAddresses = array_values($allAddresses);
        $insuranceTaxAmount = isset($allAddresses[0]) ? $allAddresses[0]->getInsuranceTaxAmount() : 0;
        $baseinsuranceTaxAmount = isset($allAddresses[0]) ? $allAddresses[0]->getBaseInsuranceTaxAmount() : 0;

        $creditmemo->setTaxAmount($creditmemo->getTaxAmount() + $insuranceTaxAmount);
        $creditmemo->setBaseTaxAmount($creditmemo->getBaseTaxAmount() + $baseinsuranceTaxAmount);

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getInsuranceAmount() + $insuranceTaxAmount);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseInsuranceAmount() + $baseinsuranceTaxAmount);

        return $this;
    }
}