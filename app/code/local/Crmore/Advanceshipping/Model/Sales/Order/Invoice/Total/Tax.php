<?php
class Crmore_Advanceshipping_Model_Sales_Order_Invoice_Total_Tax extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
	public function collect($invoice)
	{
		$order = $invoice->getOrder();
		if ($this->_canIncludePaymentTax($invoice)) {
	        $taxAmount           = $order->getInsuranceTaxAmount();
	        $baseTaxAmount       = $order->getBaseInsuranceTaxAmount();
	        
	        $invoice->setTaxAmount($invoice->getTaxAmount()+$taxAmount);
	        $invoice->setBaseTaxAmount($invoice->getBaseTaxAmount()+$baseTaxAmount);
	        $invoice->setInsuranceTaxAmount($taxAmount);
	        $invoice->setBaseInsuranceTaxAmount($baseTaxAmount);
	        
			$invoice->setGrandTotal($invoice->getGrandTotal() + $taxAmount);
        	$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseTaxAmount);
    	}
    	return $this;
	}
   	protected function _canIncludeShipping($invoice)
    {
        $includeShippingTax = true;
        /**
         * Check shipping amount in previous invoices
         */
        foreach ($invoice->getOrder()->getInvoiceCollection() as $previusInvoice) {
            if ($previusInvoice->getShippingAmount() && !$previusInvoice->isCanceled()) {
                $includeShippingTax = false;
            }
        }
        return $includeShippingTax;
    }	
}
?>