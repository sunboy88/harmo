<?php

class Crmore_Advanceshipping_Model_Sales_Order_Invoice_Total_Insuranceamount extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
    	$invoice->setInsuranceAmount(0);
        $invoice->setBaseInsuranceAmount(0);
		$invoice->setShippingMultiple(0);
      
    	$amount = $invoice->getOrder()->getInsuranceAmount();        
        $invoice->setInsuranceAmount($amount);
        $shippingoption = $invoice->getOrder()->getShippingMultiple(); 
		$invoice->setShippingMultiple($shippingoption);
        $baseamount = $invoice->getOrder()->getBaseInsuranceAmount();
        $invoice->setBaseInsuranceAmount($amount);

        $quoteId = $invoice->getOrder()->getQuoteId();
        $quote = Mage::getModel("sales/quote")->getCollection()
            ->addFieldToFilter('entity_id', array('eq'=>$quoteId))
            ->getFirstItem();
        $allAddresses = $quote ? $quote->getAllAddresses() : array();
        foreach ($allAddresses as $index => $address) {
            if($address->getAddressType() != Mage_Sales_Model_Quote_Address::TYPE_SHIPPING) {
                unset($allAddresses[$index]);
            }
        }
        $allAddresses = array_values($allAddresses);
        $insuranceTaxAmount = isset($allAddresses[0]) ? $allAddresses[0]->getInsuranceTaxAmount() : 0;
        $baseinsuranceTaxAmount = isset($allAddresses[0]) ? $allAddresses[0]->getBaseInsuranceTaxAmount() : 0;

        $invoice->setTaxAmount($invoice->getTaxAmount() + $insuranceTaxAmount);
        $invoice->setBaseTaxAmount($invoice->getBaseTaxAmount() + $baseinsuranceTaxAmount);

        $invoice->setGrandTotal($invoice->getGrandTotal() + $amount + $insuranceTaxAmount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseamount + $baseinsuranceTaxAmount);

        return $this;
    }
}