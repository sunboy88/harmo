<?php
class Crmore_Advanceshipping_Model_Sales_Order_Creditmemo_Total_Tax extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect($invoice)
	{
		if ($invoice = $creditmemo->getInvoice()) {
    		$paymentTaxAmount     = $invoice->getPaymentTaxAmount();
    		$basePaymentTaxAmount = $invoice->getBasePaymentTaxAmount();
    		$totalTax            += $paymentTaxAmount;
    		$baseTotalTax        += $basePaymentTaxAmount;
		} else {
    		$paymentTaxAmount     = $order->getPaymentTaxAmount();
    		$basePaymentTaxAmount = $order->getBasePaymentTaxAmount();
    		$totalTax            += $paymentTaxAmount;
    		$baseTotalTax        += $basePaymentTaxAmount;
		}
		$creditmemo->setPaymentTaxAmount($paymentTaxAmount);
		$creditmemo->setBasePaymentTaxAmount($basePaymentTaxAmount);
		return $this;
	}
   	protected function _canIncludeShipping($invoice)
    {
        $includeShippingTax = true;
        /**
         * Check shipping amount in previous invoices
         */
        foreach ($invoice->getOrder()->getInvoiceCollection() as $previusInvoice) {
            if ($previusInvoice->getShippingAmount() && !$previusInvoice->isCanceled()) {
                $includeShippingTax = false;
            }
        }
        return $includeShippingTax;
    }	
}
?>