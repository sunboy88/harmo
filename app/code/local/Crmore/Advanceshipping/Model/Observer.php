<?php
class Crmore_Advanceshipping_Model_Observer
{
	const DOUBLE_SHIPPING='advanceshipping/double_shipping/is_actived';
	const INSURANCE = 'advanceshipping/insurance/is_actived';
	const SHIPPING_IN_FEW =1;
	const SHIPPING_IN_MULTIPLE=2;
	
	public function calculateShippingOptions($observer)
	{
		$is_doubleshipping_enabled=Mage::getStoreConfig(self::DOUBLE_SHIPPING);
		$is_insurance_enabled = Mage::getStoreConfig(self::INSURANCE);
		if(!$is_doubleshipping_enabled && !$is_insurance_enabled)
		{
			return $this;
		}
		$request = $observer->getEvent()->getRequest();
		/* @var $quote Mage_Sales_Model_Quote */
		$quote =  $observer->getEvent()->getQuote();
		
		if(!$is_doubleshipping_enabled)
		{
			$is_doubleShipping = self::SHIPPING_IN_FEW;
		}else{
			$is_doubleShipping = $request->getPost('shipping_multiple',self::SHIPPING_IN_FEW);
		}
		if(!$is_insurance_enabled)
		{
			$is_insurance = 0;
		}else{
			$is_insurance = $request->getPost('insurance',0);
		}
		$address = $this->getAddressBaseOn($quote);
		$address->setInsurance(0);
		$address->setInsuranceAmount(0);
		$address->setBaseInsuranceAmount(0);
		
		if($is_insurance){
			if($is_doubleShipping==self::SHIPPING_IN_MULTIPLE){
				$insurance_fee =  Mage::helper('advanceshipping')->getInsuranceAmount($quote,true);
			}else{
				$insurance_fee =  Mage::helper('advanceshipping')->getInsuranceAmount($quote,false);
			}
			$address->setInsurance(1);
			$address->setInsuranceAmount($insurance_fee);
			$address->setBaseInsuranceAmount($insurance_fee);
		}
		
		if($is_doubleShipping==self::SHIPPING_IN_MULTIPLE)
		{
			$address->setShippingMultiple(self::SHIPPING_IN_MULTIPLE);
		}else{
			$address->setShippingMultiple(self::SHIPPING_IN_FEW);
		}
		$quote->save();
		return $this;		
	}
	protected function getAddressBaseOn(Mage_Sales_Model_Quote $quote)
	{
		$basedOn = Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_BASED_ON, $quote->getStore());
		switch ($basedOn) {
			case 'billing':
				$address = $quote->getBillingAddress();
				break;
			case 'shipping':
				$address = $quote->getShippingAddress();
				break;
			default:
				$address = $quote->getShippingAddress();
				break;
		}
		return $address;
	}
	public function convertFieldsToOrder($observer)
	{
		$order = $observer->getOrder();
		$address = $observer->getAddress();
		
		$order->setInsurance(0);
		$order->setShippingMultiple(1);
		$order->setInsuranceAmount(0);
		$order->setBaseInsuranceAmount(0);
		
		$insurance = $address->getInsurance();
		$shipping = $address->getShippingMultiple();
		$insuranceAmount = $address->getInsuranceAmount();
		$baseInsuranceAmount = $address->getBaseInsuranceAmount();
		$order->setInsurance($insurance);
		$order->setShippingMultiple($shipping);
		$order->setInsuranceAmount($insuranceAmount);
		$order->setBaseInsuranceAmount($baseInsuranceAmount);
		return $this;
	}
	public function setCustomerGroupId($observer)
	{
		return $this;
		$eu =array('DE','AT','BE','BG','CY','HR','DK','ES','EE','FI','FR','EL','HU','IE','IT','LV','LT','LU','MT','NL','PL','PT','CZ','RO','GB','SK','SL','SE');
		$group_id = false;
		$address = $observer->getEvent()->getCustomerAddress();
		$country_id = strtoupper($address->getCountryId());
		if(in_array($country_id, $eu)){
			$group_id=4;
		}else if($country_id=='US'){
			$group_id=5;
		}
		if($group_id!==false){
			$customer = $address->getCustomer();
			$current_group = intval($customer->getGroupId());
			if($group_id!=$current_group){

				$customer->setGroupId($group_id);

				try{
					$customer->save();
				}catch (Exception $e){
					Mage::logException($e);
				}
			}
		}
		return $this;
	}
	public function handleCollect($observer)
	{
		$quote = $observer->getEvent()->getQuote();
		$shippingAddress = $quote->getShippingAddress();
		$billingAddress = $quote->getBillingAddress();
		$saveQuote = false;
		if (!$shippingAddress->getCountryId()) {
			$country = Mage::getStoreConfig('shipping/origin/country_id');
			$state = Mage::getStoreConfig('shipping/origin/region_id');
			$postcode = Mage::getStoreConfig('shipping/origin/postcode');
			$method = Mage::getStoreConfig('shipping/origin/shippingmethod');
			
			$shippingAddress
				->setCountryId($country)
				->setRegionId($state)
				->setPostcode($postcode)
				->setShippingMethod($method)
				->setCollectShippingRates(true);
			$shippingAddress->save();
			
			$saveQuote = true;
		}
		if (!$billingAddress->getCountryId()) {
			$country = Mage::getStoreConfig('shipping/origin/country_id');
			$state = Mage::getStoreConfig('shipping/origin/region_id');
			$postcode = Mage::getStoreConfig('shipping/origin/postcode');
						
			$billingAddress
				->setCountryId($country)
				->setRegionId($state)
				->setPostcode($postcode);
				
			$saveQuote = true;
			
			$quote->save();
		}
		if ($saveQuote)
			$quote->save();
		return $this;
	}
	public function setExtraTax($observer)
	{
		$quote = $observer->getEvent()->getQuote();
		$address = Mage::helper('advanceshipping')->taxBaseOn($quote);
			
			$isInsurance = $address->getInsurance();
    	
	        $insuranceAmount = $address->getInsuranceAmount();
	        if(!$insuranceAmount){$insuranceAmount=0;}
	        
			$isDoubleShipping = ($address->getShippingMultiple()==2)?true:false;
			$taxAmount=0;
			$baseTaxAmount=0;
		
			if($isInsurance)
			{
	        	$result = Mage::helper('advanceshipping')->getAmountAndTaxIncluded($address->getQuote(),$isDoubleShipping);
	        	$taxAmount = $result['tax_amount'];
	        	$baseTaxAmount=$result['base_tax_amount'];
			}
			if($isInsurance)
			{
				$address->setTaxAmount($address->getTaxAmount());
				$address->setBaseTaxAmount($address->getBaseTaxAmount());

                $address->setExtraTaxAmount($address->getExtraTaxAmount() + $taxAmount);
                $address->setBaseExtraTaxAmount($address->getBaseExtraTaxAmount() + $baseTaxAmount);

//                $address->setGrandTotal($address->getGrandTotal()+$taxAmount);
//                $address->setBaseGrandTotal($address->getBaseGrandTotal()+$baseTaxAmount);
//
//                $quote->setGrandTotal((float) $quote->getGrandTotal() + $taxAmount);
//                $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $baseTaxAmount);

//                $address->setExtraTaxAmount($taxAmount);
//                $address->setBaseExtraTaxAmount($baseTaxAmount);
			}
			$address->save();
			return $this;
	}

    /**
     * make arbitrary items, total modifications
     * handling for event paypal_prepare_line_items
     * @param $observer
     */
    public function preparePaypalCart($observer)
    {
        /** @var $paypalCart Mage_Paypal_Model_Cart */
        $paypalCart = $observer->getEvent()->getPaypalCart();
        $insuranceAmount = $paypalCart->getSalesEntity()->getBaseInsuranceAmount();
        // add insurance amount (as handling fee) to shipping,
        $paypalCart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_SHIPPING, $insuranceAmount);
    }
}
?>