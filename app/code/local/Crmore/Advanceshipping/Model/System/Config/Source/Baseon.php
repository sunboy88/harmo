<?php
class Crmore_Advanceshipping_Model_System_Config_Source_Baseon extends Varien_Object
{
	public function toOptionArray()
    { 
    	return array(
            array('value'=>'0', 'label'=>Mage::helper('advanceshipping')->__('Subtotal Exclude Tax')),
            array('value'=>'1', 'label'=>Mage::helper('advanceshipping')->__('Subtotal Included Tax')),
        ); 
    }
}
?>