<?php
class Crmore_Advanceshipping_Model_System_Config_Source_Taxclass
{
public function toOptionArray()
    {
        $options = Mage::getModel('tax/class_source_product')->toOptionArray();
        array_shift($options);
        array_unshift($options, array('value'=>'-1','label'=>Mage::helper('advanceshipping')->__('Get From Products In Cart')));
        array_unshift($options, array('value'=>'0','label'=>Mage::helper('tax')->__('None')));
        //array_unshift($options, array('value'=>'', 'label' => Mage::helper('tax')->__('None')));
        return $options;
    }
}
?>