<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30/12/2013
 * Time: 14:56
 */
class Crmore_Advanceshipping_Model_Tax_Sales_Total_Quote_Shipping extends Mage_Tax_Model_Sales_Total_Quote_Shipping
{
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        defined('FREE_AMOUNT') || define('FREE_AMOUNT', 0);
        $quote = $address->getQuote();
        // if it's double shipping
        $isDoubleshipping = Crmore_Advanceshipping_Model_Observer::SHIPPING_IN_MULTIPLE == $address->getShippingMultiple();
        if ($address->getShippingMethod() && $isDoubleshipping) {
            switch (true) {
                // not free at second
                case FREE_AMOUNT == $address->getShippingAmount():
                    $amounts = array();
                    $rates = $address->collectShippingRates()->getGroupedAllShippingRates();
                    foreach ($rates as $carrier) {
                        foreach ($carrier as $rate) {
                            $amount = (float)$rate->getData('price');
                            FREE_AMOUNT < $amount ? $amounts[] = $amount : '';
                        }
                    }
                    $amount = (float)min($amounts);
                    $address->setShippingAmount($amount);
                    $address->setBaseShippingAmount($quote->getStore()->convertPrice($amount, false));
                    break;
                default:
                    $amount = $address->getShippingAmount() * 2;
                    $address->setShippingAmount($amount);
                    $address->setBaseShippingAmount($quote->getStore()->convertPrice($amount, false));
                    break;
            }
        }

        // calculate shipping tax and shipping ex tax
        parent::collect($address);
    }
}