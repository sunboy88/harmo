<?php
class Crmore_Advanceshipping_Model_Observer
{
	const DOUBLE_SHIPPING='advanceshipping/double_shipping/is_actived';
	const INSURANCE = 'advanceshipping/insurance/is_actived';
	const SHIPPING_IN_FEW =1;
	const SHIPPING_IN_MULTIPLE=2;
	
	public function calculateShippingOptions($observer)
	{
		$is_doubleshipping_enabled=Mage::getStoreConfig(self::DOUBLE_SHIPPING);
		$is_insurance_enabled = Mage::getStoreConfig(self::INSURANCE);
		if(!$is_doubleshipping_enabled && !$is_insurance_enabled)
		{
			return $this;
		}
		$request = $observer->getEvent()->getRequest();
		/* @var $quote Mage_Sales_Model_Quote */
		$quote =  $observer->getEvent()->getQuote();
		
		if(!$is_doubleshipping_enabled)
		{
			$is_doubleShipping = self::SHIPPING_IN_FEW;
		}else{
			$is_doubleShipping = $request->getPost('shipping_multiple',self::SHIPPING_IN_FEW);
		}
		if(!$is_insurance_enabled)
		{
			$is_insurance = 0;
		}else{
			$is_insurance = $request->getPost('insurance',0);
		}
		$quote->getBillingAddress()->setInsurance(0);
		$quote->getBillingAddress()->setInsuranceAmount(0);
		$quote->getBillingAddress()->setBaseInsuranceAmount(0);
		
		if($is_insurance){
			if($is_doubleShipping==self::SHIPPING_IN_MULTIPLE){
				$insurance_fee =  Mage::helper('advanceshipping')->getInsuranceAmount($quote,true);
			}else{
				$insurance_fee =  Mage::helper('advanceshipping')->getInsuranceAmount($quote,false);
			}
			
			$quote->getBillingAddress()->setInsurance(1);
			$quote->getBillingAddress()->setInsuranceAmount($insurance_fee);
			$quote->getBillingAddress()->setBaseInsuranceAmount($insurance_fee);
		}
		
		if($is_doubleShipping==self::SHIPPING_IN_MULTIPLE)
		{
			$quote->getBillingAddress()->setShippingMultiple(self::SHIPPING_IN_MULTIPLE);
		}else{
			$quote->getBillingAddress()->setShippingMultiple(self::SHIPPING_IN_FEW);
		}
		$quote->save();
		return $this;		
	}
	public function convertFieldsToOrder($observer)
	{
		$order = $observer->getOrder();
		$address = $observer->getAddress();
		
		$order->setInsurance(0);
		$order->setShippingMultiple(1);
		$order->setInsuranceAmount(0);
		$order->setBaseInsuranceAmount(0);
		
		$insurance = $address->getInsurance();
		$shipping = $address->getShippingMultiple();
		$insuranceAmount = $address->getInsuranceAmount();
		$baseInsuranceAmount = $address->getBaseInsuranceAmount();
		$order->setInsurance($insurance);
		$order->setShippingMultiple($shipping);
		$order->setInsuranceAmount($insuranceAmount);
		$order->setBaseInsuranceAmount($baseInsuranceAmount);
		return $this;
	}
	public function setCustomerGroupId($observer)
	{
		return $this;
		$eu =array('DE','AT','BE','BG','CY','HR','DK','ES','EE','FI','FR','EL','HU','IE','IT','LV','LT','LU','MT','NL','PL','PT','CZ','RO','GB','SK','SL','SE');
		$group_id = false;
		$address = $observer->getEvent()->getCustomerAddress();
		$country_id = strtoupper($address->getCountryId());
		if(in_array($country_id, $eu)){
			$group_id=4;
		}else if($country_id=='US'){
			$group_id=5;
		}
		if($group_id!==false){
			$customer = $address->getCustomer();
			$current_group = intval($customer->getGroupId());
			if($group_id!=$current_group){

				$customer->setGroupId($group_id);

				try{
					$customer->save();
				}catch (Exception $e){
					Mage::logException($e);
				}
			}
		}
		return $this;
	}
	public function handleCollect($observer)
	{
		$quote = $observer->getEvent()->getQuote();
		$shippingAddress = $quote->getShippingAddress();
		$billingAddress = $quote->getBillingAddress();
		$saveQuote = false;
		if (!$shippingAddress->getCountryId()) {
			$country = Mage::getStoreConfig('shipping/origin/country_id');
			$state = Mage::getStoreConfig('shipping/origin/region_id');
			$postcode = Mage::getStoreConfig('shipping/origin/postcode');
			$method = Mage::getStoreConfig('shipping/origin/shippingmethod');
			
			$shippingAddress
				->setCountryId($country)
				->setRegionId($state)
				->setPostcode($postcode)
				->setShippingMethod($method)
				->setCollectShippingRates(true);
			$shippingAddress->save();
			
			$saveQuote = true;
		}
		if (!$billingAddress->getCountryId()) {
			$country = Mage::getStoreConfig('shipping/origin/country_id');
			$state = Mage::getStoreConfig('shipping/origin/region_id');
			$postcode = Mage::getStoreConfig('shipping/origin/postcode');
						
			$billingAddress
				->setCountryId($country)
				->setRegionId($state)
				->setPostcode($postcode);
				
			$saveQuote = true;
			
			$quote->save();
		}
		if ($saveQuote)
			$quote->save();
		return $this;
	}
}
?>