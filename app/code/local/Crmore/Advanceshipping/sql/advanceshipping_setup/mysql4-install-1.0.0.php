<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('order', 'insurance', array('type'=> 'int'));
$installer->addAttribute('order', 'insurance_amount', array('type'=> 'decimal'));
$installer->addAttribute('order', 'insurance_tax_amount', array('type'=> 'decimal'));
$installer->addAttribute('order', 'base_insurance_amount', array('type'=>'decimal'));
$installer->addAttribute('order', 'base_insurance_tax_amount', array('type'=>'decimal'));
$installer->addAttribute('order', 'shipping_multiple', array( 'type'=> 'int'));

$installer->addAttribute('invoice', 'insurance', array('type'=> 'int'));
$installer->addAttribute('invoice', 'insurance_amount', array('type'=>'decimal'));
$installer->addAttribute('invoice', 'insurance_tax_amount', array('type'=>'decimal'));
$installer->addAttribute('invoice', 'base_insurance_amount', array('type'=>'decimal'));
$installer->addAttribute('invoice', 'base_insurance_tax_amount', array('type'=>'decimal'));
$installer->addAttribute('invoice', 'shipping_multiple', array( 'type'=> 'int'));

$installer->addAttribute('creditmemo', 'insurance', array('type'=> 'int'));
$installer->addAttribute('creditmemo', 'insurance_amount',array('type'=>'decimal'));
$installer->addAttribute('creditmemo', 'insurance_tax_amount',array('type'=>'decimal'));
$installer->addAttribute('creditmemo', 'base_insurance_amount', array('type'=>'decimal'));
$installer->addAttribute('creditmemo', 'base_insurance_tax_amount', array('type'=>'decimal'));
$installer->addAttribute('creditmemo', 'shipping_multiple', array( 'type'=> 'int'));

try{

	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `shipping_multiple` INT COMMENT 'Shipping in multiple parcel';");
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `insurance` INT COMMENT 'Charge to insurance shipping';");
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `insurance_amount` decimal(10,4) COMMENT 'Insurace Amount';");
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `insurance_tax_amount` decimal(10,4) COMMENT 'Insurace Amount';");
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `base_insurance_amount` decimal(10,4) COMMENT 'Insurace Amount';");
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote_address')}` ADD `base_insurance_tax_amount` decimal(10,4) COMMENT 'Insurace Amount';");
	
}catch(Exception $e){
	Mage::logException($e);
	if(strpos($e, 'Column already exists') === false){
		throw $e;
	}
}
$installer->endSetup();
?>