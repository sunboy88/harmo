<?php
$installer = $this;

$installer->startSetup();

$installer->run("

		DROP TABLE IF EXISTS {$this->getTable('exproduct_tip')};
		CREATE TABLE {$this->getTable('exproduct_tip')} (
		`tip_id` int(11) unsigned NOT NULL auto_increment,
		`product_id`  int(11) NOT NULL default '0',
                `title`  varchar(255) NOT NULL default '',
                `content` mediumtext DEFAULT NULL,	
		PRIMARY KEY (`tip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('exproduct_tone')};
		CREATE TABLE {$this->getTable('exproduct_tone')} (
		`tone_id` int(11) unsigned NOT NULL auto_increment,
		`product_id`  int(11) NOT NULL default '0',
                `title`  varchar(255) NOT NULL default '',
                `hole` int(11) NOT NULL default '0',
                `blow` varchar(255) NOT NULL default '',
                `blow_file` varchar(255) NOT NULL default '',
                `draw` varchar(255) NOT NULL default '',
                `draw_file` varchar(255) NOT NULL default '',
		PRIMARY KEY (`tone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('exproduct_sound')};
		CREATE TABLE {$this->getTable('exproduct_sound')} (
		`sound_id` int(11) unsigned NOT NULL auto_increment,
		`product_id`  int(11) NOT NULL default '0',
                `title`  varchar(255) NOT NULL default '',
                `file_mp3` varchar(255) NOT NULL default '',
                `file_ogg` varchar(255) NOT NULL default '',
		PRIMARY KEY (`sound_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();