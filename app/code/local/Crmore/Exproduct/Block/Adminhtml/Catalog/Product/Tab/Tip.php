<?php
class Crmore_Exproduct_Block_Adminhtml_Catalog_Product_Tab_Tip
    extends Crmore_Exproduct_Block_Adminhtml_Tip_Grid
{
    protected $_aclSection = 'manage_banner';
    protected $_isTabGrid = true;
    public function isMassActionAllowed()
    {
        return false;
    }
     protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }
        Mage::register('product', $product);
        Mage::register('current_product', $product);
        return $product;
    }
    public function getSelectedLinks()
    {
         $productId  = (int) $this->getRequest()->getParam('id');
            if($productId){
            $collection = Mage::getModel('exproduct/tip')->getCollection()->addFieldToFilter('product_id',array('eq'=>$productId));
            $arrayIds = array();
            foreach ($collection as $co)
            {
                $arrayIds[]=$co->getTipId();
            }
            
            return $arrayIds;}
            else
                return null;
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
