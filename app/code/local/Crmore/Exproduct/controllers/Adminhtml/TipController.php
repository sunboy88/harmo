<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Lanot
 * @package     Lanot_EasyBanner
 * @copyright   Copyright (c) 2012 Lanot
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Crmore_Exproduct_Adminhtml_TipController
    extends Mage_Adminhtml_Controller_Action
{
   protected function _initAction() {

		$this->loadLayout()
			->_setActiveMenu('exproduct/tip')
			->_addBreadcrumb(Mage::helper('exproduct')->__('Manage Tip'),
							Mage::helper('exproduct')->__('Manage Tip'));
		return $this;

	}



	protected function _setTitle() {

		return $this->_title($this->__('List of Tip'))->_title($this->__('Tips'));

	}


 protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }
        Mage::register('product', $product);
        Mage::register('current_product', $product);
        return $product;
    }
	public function indexAction() {

		$this->_setTitle();

		$this->_initAction()

			->renderLayout();

	}
        public function gridAction() {
                $this->_initProduct();
		 $this->loadLayout();
        $this->getLayout()->getBlock('admin.exproduct.tip')
                ->setProductId(Mage::registry('product')->getId())
                ->setUseAjax(true);
        $this->renderLayout();

			

	}    
	public function newAction()

	{

		$this->_forward('edit');

	}

	public function editAction()

	{

		$id=$this->getRequest()->getParam('id',0);

		$model = Mage::getModel('exproduct/tip')->load($id);

		

		if($id && $model->getId())

		{

			Mage::register('tip_data', $model);

		}

		$this->_initAction();

		$this->_addContent($this->getLayout()->createBlock('exproduct/adminhtml_tip_edit'));

		$this->renderLayout();

	}

	public function saveAction()

	{

		$id=$this->getRequest()->getParam('id',0);

		$model = Mage::getModel('exproduct/tip')->load($id);

		$logo='';

		
		try{

			$data = $this->getRequest()->getPost('tip');
			$model->setData($data);
			if($id)
			{
				$model->setId($id);
			}
			$model->save();
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('exproduct')->__('Successfull'));

		}catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exproduct')->__('Fail to save!'));
		}
		if($this->getRequest()->getParam('back'))
		{
			$this->_redirect('*/*/edit', array('id'=>$model->getId()));
			return;
		}
		$this->_redirect('*/*/');
	}

	public function deleteAction()

	{

		$id=$this->getRequest()->getParam('id',0);

		$model = Mage::getModel('exproduct/tip')->load($id);

		if($model->getId())

		{

			try{

				$model->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('exproduct')->__('Successfull delete'));

			}catch (Exception $e)

			{

				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exproduct')->__('Fail to delete!'));

			}

		}else{

			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exproduct')->__('Item not found'));

		}

		$this->_redirect('*/*/');

	}

	public function massDeleteAction()

	{

		$ids = $this->getRequest()->getPost('brands');

		if(count($ids))

		{

			foreach ($ids as $id)

			{

				$model = Mage::getModel('exproduct/tip')->load($id);

				if($model->getId())

				{

					$logo = Mage::getBaseDir('media').Crmore_Brands_Model_Brands::LOGO_PATH.$model->getLogo();

					if(is_file($logo) && file_exists($logo))

					{

						@unlink($logo);

					}

					$model->delete();

				}

				unset($model);

			}

		}

		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('exproduct')->__('Removed'));

		$this->_redirect('*/*/');

	}
}
