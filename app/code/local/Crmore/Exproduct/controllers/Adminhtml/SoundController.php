<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Lanot
 * @package     Lanot_EasyBanner
 * @copyright   Copyright (c) 2012 Lanot
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Crmore_Exproduct_Adminhtml_SoundController
    extends Mage_Adminhtml_Controller_Action
{
   protected function _initAction() {

		$this->loadLayout()
			->_setActiveMenu('exproduct/sound')
			->_addBreadcrumb(Mage::helper('exproduct')->__('Manage Sound'),
							Mage::helper('exproduct')->__('Manage Sound'));
		return $this;

	}



	protected function _setTitle() {

		return $this->_title($this->__('List of Sound'))->_title($this->__('Sounds'));

	}


 protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }
        Mage::register('product', $product);
        Mage::register('current_product', $product);
        return $product;
    }
	public function indexAction() {

		$this->_setTitle();

		$this->_initAction()

			->renderLayout();

	}
        public function gridAction() {
                $this->_initProduct();
		 $this->loadLayout();
        $this->getLayout()->getBlock('admin.exproduct.sound')
                ->setProductId(Mage::registry('product')->getId())
                ->setUseAjax(true);
        $this->renderLayout();

			

	}    
	public function newAction()

	{

		$this->_forward('edit');

	}

	public function editAction()

	{

		$id=$this->getRequest()->getParam('id',0);

		$model = Mage::getModel('exproduct/sound')->load($id);

		

		if($id && $model->getId())

		{

			Mage::register('sound_data', $model);

		}

		$this->_initAction();

		$this->_addContent($this->getLayout()->createBlock('exproduct/adminhtml_sound_edit'));

		$this->renderLayout();

	}

	public function saveAction()

	{

		$id=$this->getRequest()->getParam('id',0);

		$model = Mage::getModel('exproduct/sound')->load($id);

		$logo='';

		$data = $this->getRequest()->getPost();
		try{
                        if(isset($_FILES['file_mp3']['name']) && $_FILES['file_mp3']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('file_mp3');
					
					// Any extention would work
	           		
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS .'sound';
					$uploader->save($path, str_replace(" ", "-", $_FILES['file_mp3']['name']) );
					
				
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['file_mp3'] = str_replace(" ", "-", 'sound/'.$_FILES['file_mp3']['name']);
			}
			if(isset($_FILES['file_ogg']['name']) && $_FILES['file_ogg']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('file_ogg');
					
					// Any extention would work
	           		
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS .'sound';
					$uploader->save($path, str_replace(" ", "-", $_FILES['file_ogg']['name']) );
					
				
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['file_ogg'] = str_replace(" ", "-", 'sound/'.$_FILES['file_ogg']['name']);
			}
			$model->setData($data);
			if($id)
			{
				$model->setId($id);
			}
			$model->save();
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('exproduct')->__('Successfull'));

		}catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exproduct')->__('Fail to save!'));
		}
		if($this->getRequest()->getParam('back'))
		{
			$this->_redirect('*/*/edit', array('id'=>$model->getId()));
			return;
		}
		$this->_redirect('*/*/');
	}

	public function deleteAction()

	{

		$id=$this->getRequest()->getParam('id',0);

		$model = Mage::getModel('exproduct/sound')->load($id);

		if($model->getId())

		{

			try{

				$model->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('exproduct')->__('Successfull delete'));

			}catch (Exception $e)

			{

				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exproduct')->__('Fail to delete!'));

			}

		}else{

			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('exproduct')->__('Item not found'));

		}

		$this->_redirect('*/*/');

	}

	public function massDeleteAction()

	{

		$ids = $this->getRequest()->getPost('brands');

		if(count($ids))

		{

			foreach ($ids as $id)

			{

				$model = Mage::getModel('exproduct/sound')->load($id);

				if($model->getId())

				{

					$logo = Mage::getBaseDir('media').Crmore_Brands_Model_Brands::LOGO_PATH.$model->getLogo();

					if(is_file($logo) && file_exists($logo))

					{

						@unlink($logo);

					}

					$model->delete();

				}

				unset($model);

			}

		}

		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('exproduct')->__('Removed'));

		$this->_redirect('*/*/');

	}
}
