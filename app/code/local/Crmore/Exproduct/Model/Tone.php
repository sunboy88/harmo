<?php

class Crmore_Exproduct_Model_Tone extends Mage_Core_Model_Abstract
{
    const STATUS_FREE_CARD_TRUE = 1;
    const STATUS_FREE_CARD_FALSE = 0;
    public function _construct()
    {
        parent::_construct();
        //Mage::log(var_export(get_class_methods(get_class(Crmore_Lyoness_Model_Comh)), TRUE),NULL,'Model.log');
        $this->_init('exproduct/tone');
    }
    protected $_selectedLinks = null;
    public function getSelectedLinks()
    {
        if (null === $this->_selectedLinks) {
            $this->_selectedLinks = $this->getResource()->getSelectedLinks($this);

        }
        return $this->_selectedLinks;
    }
}
?>
