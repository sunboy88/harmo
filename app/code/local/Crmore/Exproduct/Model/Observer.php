<?php
  
class Crmore_Exproduct_Model_Observer
{
    /**
     * Flag to stop observer executing more than once
     *
     * @var static bool
     */
    static protected $_singletonFlag = false;
 
    /**
     * This method will run when the product is saved from the Magento Admin
     * Use this function to update the product model, process the
     * data or anything you like
     *
     * @param Varien_Event_Observer $observer
     */
    public function saveProductTabData(Varien_Event_Observer $observer)
    {
        if (!self::$_singletonFlag) {
            self::$_singletonFlag = true;
             
            $product = $observer->getEvent()->getProduct();
            
            try {
                /**
                 * Perform any actions you want here
                 *
                 */
                $data = $this->_getRequest()->getPost('in_selected');
               
                $customFieldValue =  $this->_getRequest()->getPost('custom_field');
 
                /**
                 * Uncomment the line below to save the product
                 *
                 */
                //$product->save();
                $model = Mage::getModel('exproduct/tip')->getCollection();
				$model->load();
				foreach($model as $m)
				{
					$m->setProductId(0);
					$m->save();
				}
				if(is_array($data) && count($data))
				{
	                foreach ($data as $p)
	                {
	                    $model = Mage::getModel('exproduct/tip')->load($p);
	                    $model->setProductId($product->getId());
	                    $model->save();
	                }
				}
                
                $datas = $this->_getRequest()->getPost('in_selected_s');
                if($datas)
                {
					$modelco = Mage::getModel('exproduct/sound')->getCollection();
					$modelco->load();
					foreach($modelco as $m)
					{
						$m->setProductId(0);
						$m->save();
					}
                    foreach ($datas as $p)
                        {
                            $model = Mage::getModel('exproduct/sound')->load($p);
                            $model->setProductId($product->getId());
                            $model->save();
                        }
                }
                $datat = $this->_getRequest()->getPost('in_selected_t');
                if($datat)
                {
					$modelco = Mage::getModel('exproduct/tone')->getCollection();
					$modelco->load();
					foreach($modelco as $m)
					{
						$m->setProductId(0);
						$m->save();
					}
                    foreach ($datat as $p)
                        {
                            $model = Mage::getModel('exproduct/tone')->load($p);
                            $model->setProductId($product->getId());
                            $model->save();
                        }
                }
				 
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
    }
      
    /**
     * Retrieve the product model
     *
     * @return Mage_Catalog_Model_Product $product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }
     
    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }
}