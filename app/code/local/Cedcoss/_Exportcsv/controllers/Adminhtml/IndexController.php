<?php 

class Cedcoss_Exportcsv_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
	public function exportAction()
	{	
		$collection = Mage::getModel('sales/order_item')
		->getCollection()
		->addAttributeToSelect('*');
		$i=1;
		$array[] = array('Export Store ','Export Order Number', 'Export Brand', 'Export SKU','Export Qty');
		foreach($collection as $item)
		{	$product=Mage::getModel('catalog/product')->load($item->getProductId());
			$order=Mage::getModel('sales/order')->load($item->getOrderId());
			if($order->getStatus()=='processing'||$order->getStatus()=='new_awaitingstockfronthestore'||$order->getStatus()=='holded')
			{	
				if(!is_null($item->getQtyBackordered())||true)
				{
					$stock = Mage::getModel('xwarehouse/stock')->getCollection()->addFieldToFilter('product_id',$item->getProductId())->getFirstItem();
					if(Mage::getModel('core/store')->load($order->getStoreId())->getWebsiteId()==1)
					 {
						
						$site='USA Warehouse';
						$qty=$stock->getUsa();
					 }
					 else
					 {
						$site='France Warehouse';
						$qty=$stock->getFrance();
						
					 }
					/*  echo 'sku: '.$product->getSku().' fr: '.$stock->getFrance().' us:'.$stock->getUsa().' '.$site.'</br>'; */
					 $brand=$product->getAttributeText('manufacturer');
					 if($qty<0)
					 {
						$qty=$qty<0?($qty*(-1)):$qty;
						$array[] = array($site,$order->getIncrementId(),$brand,$product->getSku().' '.$product->getAttributeText('manufacturer'),$qty);
					 }
				}
			}
			
		}
		function customeroutputCSV($data) {
			$outputBuffer = fopen("php://output", 'w');
			foreach($data as $val) {
				fputcsv($outputBuffer, $val);
			}
			fclose($outputBuffer);
		}

		$file_name = 'export.csv';

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=".$file_name);
		header("Pragma: no-cache");
		header("Expires: 0");
		customeroutputCSV($array);
	}
}


?>