<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Block_Export_Edit_Tab_Configuration extends Mage_Adminhtml_Block_Widget_Form
{
    protected $_mediumDateFormat;

    public function getConfig()
    {
        return Mage::getSingleton('aitexporter/config')->getExportConfig(Mage::registry('current_store')->getId());
    }

    public function getSelected($url, $value)
    {
        $config = $this->getConfig();
        $keys = explode("/", $url);
        
        foreach($keys as $key)
        {
            if(isset($config[$key]))
            {
                $config = $config[$key];
            }
        }

        if($config == $value)
        {
            return 'selected="selected"';
        }
        return '';
    }
    
    public function getValue($url, $default='')
    {
        $config = $this->getConfig();
        $keys   = explode("/", $url);
        $value  = '';

        foreach($keys as $key)
        {
            if(isset($config[$key]))
            {
                $config = $config[$key];
                $value = $config;
            }
        }
        
        if (is_array($value))
        { 
            return $default;
        }

        return $this->htmlEscape(strlen($value) > 0 ? $value : $default);
    }
    
    public function getEmailSenders()
    {
        return Mage::getModel('adminhtml/system_config_source_email_identity')->toOptionArray();
    }
    
    public function getEmailTemplates()
    {
        return Mage::getModel('adminhtml/system_config_source_email_template')
            ->setPath('aitexporter_template')
            ->toOptionArray();
    }
    
    public function getOrderStatuses()
    {
        $data = Mage::getModel('adminhtml/system_config_source_order_status')->toOptionArray();
        if($data[0]['value'] == '')
        {
            $data[0] = array('value' => '', 'label' => Mage::helper('aitexporter')->__('All Statuses'));
        }

        return $data;
    }
    
    public function getShortDateFormat()
    {
        if (!$this->_shortDateFormat) 
        {
            $this->_shortDateFormat = Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        }

        return $this->_shortDateFormat;
    }

    public function getMediumDateFormat()
    {
        if (!$this->_mediumDateFormat) 
        {
            $this->_mediumDateFormat = Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        }

        return $this->_mediumDateFormat;
    }
    
    public function getLastOrderId()
    {
        $orders = Mage::getModel('sales/order')->getCollection()
            ->setOrder('increment_id','DESC')
            ->setPageSize(1)
            ->setCurPage(1);
        $lastOrderId = $orders->getFirstItem()->getIncrementId();

        return $lastOrderId;
    }

    public function getLastExportedOrderId()
    {
        $order = Mage::getModel('sales/order')->load(Mage::getModel('aitexporter/export_order')->getLastExportedOrderId());

        return $order->getIncrementId();
    }
    
    public function getCronFrequency()
    {
        return Mage::getModel('aitexporter/system_config_source_cron')->toOptionArray();
    }
    
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        
        $this->setForm($form);
        
        $this->setTemplate('aitexporter/export.phtml');
        
        return parent::_prepareForm();
    }

    public function isXslExists()
    {
        return (boolean)(Mage::getSingleton('aitexporter/config')->getXsl(Mage::registry('current_store')->getId()));
    }
}