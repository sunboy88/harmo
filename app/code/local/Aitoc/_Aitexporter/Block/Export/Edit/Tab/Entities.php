<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Block_Export_Edit_Tab_Entities extends Mage_Adminhtml_Block_Widget_Form
{
    public function getConfig()
    {
        return Mage::getSingleton('aitexporter/config')->getExportConfig(Mage::registry('current_store')->getId());
    }

    public function getChecked($url)
    {
        $config = $this->getConfig();
        
        $keys = explode("/", $url);
        
        foreach($keys as $key)
        {
            if(isset($config[$key]))
            {
                $config = $config[$key];
            }
        }
        
        if($config == 1)
        {
            return 'checked="' . $config . '"';
        }
        return '';
    }

    public function getValue($url, $default='')
    {
        $config = $this->getConfig();
        $keys   = explode("/", $url);
        $value  = '';

        foreach($keys as $key)
        {
            if(isset($config[$key]))
            {
                $config = $config[$key];
                $value = $config;
            }
        }
        
        if (is_array($value))
        { 
            return $default;
        }

        return $this->htmlEscape(strlen($value) > 0 ? $value : $default);
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        
        $this->setForm($form);

        $this->setTemplate('aitexporter/export_entities.phtml');
        
        return parent::_prepareForm();
    }
}