<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Block_Export_Edit_Tab_Orderfields extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        
        $this->setForm($form);
        
        $this->setTemplate('aitexporter/orderfields.phtml');
        
        return parent::_prepareForm();
    }
    
    public function getOrderFields()
    {
        return Mage::getModel('aitexporter/export_type_order')->getOrderFields();
    }
    
    public function getChecked($url)
    {
        $config = Mage::getSingleton('aitexporter/config')->getExportConfig(Mage::registry('current_store')->getId());
        
        $keys = explode("/", $url);
        
        foreach($keys as $key)
        {
            if(isset($config[$key]))
            {
                $config = $config[$key];
            }
        }

        if($config == 1)
        {
            return 'checked="' . $config . '"';
        }
        return '';
    }
    
    public function getAttributeCodeEavFlat($field)
    {
        return Mage::getModel('aitexporter/export_type_order')->getAttributeCodeEavFlat($field);
    }
}