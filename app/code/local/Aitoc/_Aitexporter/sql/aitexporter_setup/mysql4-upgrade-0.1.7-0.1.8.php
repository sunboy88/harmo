<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;


$installer->startSetup();

$installer->run('

ALTER TABLE `'.$this->getTable('aitexporter_import').'` 
    ADD COLUMN `add_count` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
    ADD COLUMN `update_count` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
    ADD COLUMN `fail_count` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0;

');

$installer->endSetup();