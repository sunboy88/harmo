<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Model_Export_Order extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();

        $this->_init('aitexporter/export_order');
    }

    public function getLastExportedOrderId($storeId = 0)
    {
        $exportedOrders = $this->getCollection()
            ->loadLastOrder()
            //->loadByCron()
            ->loadByStoreId($storeId);

        return $exportedOrders->getFirstItem()->getOrderId();
    }

    public function assignOrders(Aitoc_Aitexporter_Model_Export $export)
    {
        $this->getResource()->assignOrders($export);

        return $this;
    }
}