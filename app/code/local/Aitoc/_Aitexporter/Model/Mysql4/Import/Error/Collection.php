<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Model_Mysql4_Import_Error_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    private $_storeId;

    public function _construct()
    {
        $this->_init('aitexporter/import_error');
    }
    
    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;

        return $this;
    }

    public function loadOrderErrors($orderIncrementId)
    {
        $currentImport = Mage::registry('current_import');
        if ($currentImport)
        {
            $this
                ->addFieldToFilter('import_id', $currentImport->getId())
                ->addFieldToFilter('order_increment_id', $orderIncrementId)
                ->addFieldToFilter('type', Aitoc_Aitexporter_Model_Import_Error::TYPE_ERROR)
                ;
        }

        return $this;
    }
}