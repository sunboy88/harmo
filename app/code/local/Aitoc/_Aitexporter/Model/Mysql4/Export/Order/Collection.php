<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Model_Mysql4_Export_Order_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    private $_isExportJoint = false;

    public function _construct()
    {
        $this->_init('aitexporter/export_order');
    }

    protected function _joinExport()
    {
        if (!$this->_isExportJoint)
        {
            $this->getSelect()->join(
                array('export' => $this->getResource()->getTable('aitexporter/export')), 
                Mage::helper('aitexporter/version')->collectionMainTableAlias().'.export_id = export.export_id', 
                array());

            $this->_isExportJoint = true;
        }

        return $this;
    }

    public function loadByCron()
    {
        $this->_joinExport();

        $this->getSelect()->where('export.is_cron = ?', 1);

        return $this;
    }

    public function loadByStoreId($storeId = 0)
    {
        $this->_joinExport();

        $this->getSelect()->where('export.store_id = ?', $storeId);

        return $this;
    }

    public function loadLastOrder()
    {
        $this->setOrder('order_id', 'DESC')
            ->setPageSize(1)
            ->setCurPage(1);

        return $this;
    }
}