<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Model_Observer
{
    protected $_order = null;


    protected function _getFreqStr($configParam)
    {
        $strToTimeArray = Mage::getModel('aitexporter/system_config_source_cron')->toStrToTimeArray();
        return isset($strToTimeArray[$configParam])?$strToTimeArray[$configParam]:false;
    }


    public function cronExport()
    {

        $processor = Mage::getSingleton('aitexporter/processor');
        if($processor->getProcess())
        {   //another process is running
            return false;
        }

        $stores    = Mage::app()->getStores();
        $stores[0] = new Varien_Object(array('id' => 0));

        foreach ($stores as $store)
        {
            $config = Mage::getSingleton('aitexporter/config')->getExportConfig($store->getId());
    
            if (empty($config['auto']['cron_frequency']))
            {
                // Cron is not configured to run
                continue;
            }

            $strTime = $this->_getFreqStr($config['auto']['cron_frequency']);

            $export = Mage::getModel('aitexporter/export')->getCollection()
                ->loadLastCronExport($store->getId());
            
            if($export->getSize() != 0)
            {
                $export = $export->getFirstItem();
                
                $currentDbTime = strtotime($export->getDbTime());
                
                if ($export->getDt() && (strtotime('-'.$strTime, $currentDbTime) < strtotime($export->getDt())))
                {
                    // Too early to export
                    continue;
                }
            }

            $expModel = Mage::getModel('aitexporter/export');
            $expModel
                ->setConfig($config)
                ->setStoreId($store->getId())
                ->setIsCron(1)
                ->save();
            $options = array(
                'id' => $expModel->getId()
            );
            $processor->setProcess('export::initExport', $options)->save();
        }
    }
    
    public function exportOrderAfterPlace(Varien_Event_Observer $observer)
    {
        if(version_compare(Mage::getVersion(),'1.4.0.0','<'))
        {
            if(($observer->getEvent()->getName() != 'sales_order_save_after') &&
                ($observer->getEvent()->getName() != 'aitcheckoutfields_aitexporter'))
            {
                return false;
            }
        }
        else
        {
            if(($observer->getEvent()->getName() != 'sales_order_save_commit_after') &&
                ($observer->getEvent()->getName() != 'aitcheckoutfields_aitexporter'))
            {
                return false;
            }
        }
        
        if(Mage::helper('aitexporter/aitcheckoutfields')->isEnabled())
        {
            if($observer->getEvent()->getName() != 'aitcheckoutfields_aitexporter')
            {
                return false;
            }
        }
        
        if (Mage::registry('current_import'))
        {
            return false;
        }
        
        if($this->_order !== null || is_object($this->_order)) {
            return false;
        }

        $order    = $observer->getEvent()->getOrder();
        
        /* @var $order Mage_Sales_Model_Order */
        $origData = $order->getOrigData();

               
        if (!empty($origData))
        {
            if(!Mage::helper('aitexporter/aitcheckoutfields')->isEnabled())
            {
                return false;
            }
        }
        
        //to process order on postdispatch action setting up flag that checkout action was active and order is created
        //some modules may add values to order data that are not set before first sales_order_save_commit_after action and
        //may call this action several times, because of that we store order to process it after all possible actions took place
        $this->_order = $order;
    }
    
    public function onControllerActionPostdispatch($observer)
    {
        if($this->_order == null) {
            //order is not set - we don't process it
            return false;
        }
        $order = $this->_order;
        /* @var $order Mage_Sales_Model_Order */
        
        if($order->getExportedAfterPlace())
        {
            return false;
        }

        $processor = Mage::getSingleton('aitexporter/processor');
        if($processor->getProcess())
        {   //another process is running
            return false;
        }

        // Prevent re-exporting on Mage_Core_Model_Resource_Transaction::_runCallbacks()
        $order->setExportedAfterPlace(true);

        $storeIds = array(0, $order->getStoreId());
        foreach ($storeIds as $storeId)
        {
            /* @var $order Mage_Sales_Model_Order */
            $config = Mage::getSingleton('aitexporter/config')->getExportConfig($storeId);
            if (empty($config['auto']['after_checkout']))
            {
                continue;
            }

            $config['filter'] = array(
            	'order_id' => $order->getId(), 
                );
            $export = Mage::getModel('aitexporter/export');
            $export
                ->setConfig($config)
                ->setStoreId($storeId)
                ->save();

            $options = array(
                'id' => $export->getId()
            );

            $processor->setProcess('export::initExport', $options)->setForward(1)->save()->run();
               
        }        
    }

    public function exportOrderAfterInvoice(Varien_Event_Observer $observer)
    {
        if (Mage::registry('current_import'))
        {
            return false;
        }

        $invoice = $observer->getEvent()->getInvoice();
        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        $order = $invoice->getOrder();
        /* @var $order Mage_Sales_Model_Order */
        $storeIds = array(0, $order->getStoreId());
        $origData = $invoice->getOrigData();

        if (!empty($origData) || $order->getExportedAfterInvoice())
        {
            return false;
        }

        foreach ($order->getItemsCollection() as $item)
        {
            /* @var $item Mage_Sales_Model_Order_Item */
            if (!$item->isDummy() && $item->getQtyOrdered() && $item->getQtyOrdered() != $item->getQtyInvoiced())
            {
                return false;
            }
        }

        $processor = Mage::getSingleton('aitexporter/processor');
        if($processor->getProcess())
        {   //another process is running
            return false;
        }

        $order->setExportedAfterInvoice(true);

        foreach ($storeIds as $storeId)
        {
            $config = Mage::getSingleton('aitexporter/config')->getExportConfig($storeId);
            if (empty($config['auto']['after_invoice']))
            {
                continue;
            }

            $config['filter'] = array(
            	'order_id' => $order->getId(), 
                );

            $export = Mage::getModel('aitexporter/export');
            $export
                ->setConfig($config)
                ->setStoreId($storeId)
                ->save();
            $options = array(
                'id' => $export->getId()
            );
            $processor->setProcess('export::initExport', $options)->setForward(1)->save()->run();
        }
    }
}