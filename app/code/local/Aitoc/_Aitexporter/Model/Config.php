<?php
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Model_Config
{
    const EXPORT_PATH     = 'sales/aitexporter/export';
    const EXPORT_XSL_PATH = 'sales/aitexporter/export_xsl';
    const IMPORT_PATH     = 'sales/aitexporter/import';

    private $_exportConfig2store = array();

    public function getExportConfig($storeId = 0)
    {
        $storeId = (int)$storeId;

        if (!isset($this->_exportConfig2store[$storeId]))
        {
            $scope = $storeId ? 'stores' : 'default';

            $configModel = Mage::getModel('core/config_data')->getCollection()
                ->addScopeFilter($scope, $storeId, 'sales/aitexporter')
                ->addFieldToFilter('path', self::EXPORT_PATH)
                ->getFirstItem();

            $config = Mage::getConfig()->getNode('default/'.self::EXPORT_PATH);
            if ($configModel->getValue())
            {
                $config = unserialize($configModel->getValue());
            }

            $this->_exportConfig2store[$storeId] = $config;
        }

        return $this->_exportConfig2store[$storeId];
    }

    public function getImportConfig()
    {
        return unserialize(Mage::getStoreConfig(self::IMPORT_PATH));
    }

    /**
     * 
     * @param array $export
     */
    public function saveExportConfig($export, $storeId = 0)
    {
        $config = Mage::getConfig();
        /* @var $config Mage_Core_Model_Config */

        $scope = $storeId ? 'stores' : 'default';

        $config->saveConfig(self::EXPORT_PATH, serialize($export), $scope, $storeId)->reinit();
        
        if(!empty($export['auto']['cron_frequency']))
        {
            $config->saveConfig('crontab/jobs/aitexporter_export_orders/schedule/cron_expr', $export['auto']['cron_frequency'], 'default', 0)->reinit();
        }
        else
        {
            $config->deleteConfig('crontab/jobs/aitexporter_export_orders/schedule/cron_expr', 'default', 0)->reinit();
        }
        
        Mage::app()->reinitStores();

        $this->_exportConfig2store = array();

        return $this;
    }

    /**
     * 
     * @param array $export
     */
    public function saveImportConfig($export)
    {
        $config = Mage::getConfig();
        /* @var $config Mage_Core_Model_Config */

        $config->saveConfig(self::IMPORT_PATH, serialize($export))->reinit();

        return $this;
    }

    public function getXsl($storeId = 0)
    {
        $scope = $storeId ? 'stores' : 'default';

        $configModel = Mage::getModel('core/config_data')->getCollection()
            ->addScopeFilter($scope, $storeId, 'sales/aitexporter')
            ->addFieldToFilter('path', self::EXPORT_XSL_PATH)
            ->getFirstItem();

        $xsl = '';
        if ($configModel->getValue())
        {
            $xsl = $configModel->getValue();
        }

        return $xsl;
    }

    /**
     * 
     * @param string $xslContent
     */
    public function saveXsl($xslContent, $storeId = 0)
    {
        $config = Mage::getConfig();
        /* @var $config Mage_Core_Model_Config */

        $scope = $storeId ? 'stores' : 'default';

        $config->saveConfig(self::EXPORT_XSL_PATH, $xslContent, $scope, (int)$storeId)->reinit();
        Mage::app()->reinitStores();

        return $this;
    }
}