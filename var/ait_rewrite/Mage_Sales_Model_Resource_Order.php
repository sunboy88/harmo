<?php
/* DO NOT MODIFY THIS FILE! THIS IS TEMPORARY FILE AND WILL BE RE-GENERATED AS SOON AS CACHE CLEARED. */

/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     aVmvxS6NyPLyQ3BRUKPrkB1Q0n73QdKaipmrCsh4Tw
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitexporter_Model_Rewrite_SalesModelMysql4Order extends Mage_Sales_Model_Resource_Order
{
	/**
	 * To save correct create and update dates.
	 * 
	 * @override
	 */
	protected function _prepareDataForSave(Mage_Core_Model_Abstract $object)
	{
		if(Mage::registry('current_import'))
		{
            if(version_compare(Mage::getVersion(), '1.6.0.0', '>=')) {
                return Mage_Core_Model_Resource_Db_Abstract::_prepareDataForSave($object);
            } else {
			    return Mage_Core_Model_Mysql4_Abstract::_prepareDataForSave($object);
            }
		}
		else
		{
			return parent::_prepareDataForSave($object);
		}
	}
}


class Amasty_Orderstatus_Model_Mysql4_Sales_Order extends Aitoc_Aitexporter_Model_Rewrite_SalesModelMysql4Order
{
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        parent::_beforeSave($object);
        $order = $object;
        if ($order->getId())
        {
            $currentOrder = Mage::getModel('sales/order')->load($order->getId());
            // if state changed, and status too, will set previous status if it was status created by extension
            if ($currentOrder->getState() != $order->getState() && $currentOrder->getStatus() != $order->getStatus())
            {
                $currentStatus = str_replace($currentOrder->getState() . '_', '', $currentOrder->getStatus());
                $statusModel = Mage::getModel('amorderstatus/status')->load($currentStatus, 'alias');
                if ($statusModel->getId() && !$statusModel->getIsSystem())
                {
                    // checking if we should apply status to the current state
                    $parentStates = array();
                    if ($statusModel->getParentState())
                    {
                        $parentStates = explode(',', $statusModel->getParentState());
                    }
                    // checking if the status corresponds to the state of the new order
                    if (!$parentStates || in_array($order->getState(), $parentStates))
                    {
                        // replacing status back
                        $order->setStatus($order->getState() . '_' . $currentStatus);
                    }
                }
            }
        }
        return $this;
    }
}

