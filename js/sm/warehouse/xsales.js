/**
 * User: Hieunt
 * Date: 3/21/13
 * Time: 4:32 PM
 */
var xAdminOrder = Class.create(AdminOrder,{
    initialize : function(data){
        if(!data) data = {};
        this.loadBaseUrl    = false;
        this.customerId     = data.customer_id ? data.customer_id : false;
        this.storeId        = data.store_id ? data.store_id : false;
        this.warehouseId    = data.warehouseId ? data.warehouseId : false;
        this.currencyId     = false;
        this.currencySymbol = data.currency_symbol ? data.currency_symbol : '';
        this.addresses      = data.addresses ? data.addresses : $H({});
        this.shippingAsBilling = data.shippingAsBilling ? data.shippingAsBilling : false;
        this.gridProducts   = $H({});
        this.gridProductsGift = $H({});
        this.billingAddressContainer = '';
        this.shippingAddressContainer= '';
        this.isShippingMethodReseted = data.shipping_method_reseted ? data.shipping_method_reseted : false;
        this.overlayData = $H({});
        this.giftMessageDataChanged = false;
        this.productConfigureAddFields = {};
        this.productPriceBase = {};
        this.collectElementsValue = true;
        Event.observe(window, 'load',  (function(){
            this.dataArea = new OrderFormArea('data', $(this.getAreaId('data')), this);
            this.itemsArea = Object.extend(new OrderFormArea('items', $(this.getAreaId('items')), this), {
                addControlButton: function(button){
                    var controlButtonArea = $(this.node).select('.form-buttons')[0];
                    if (typeof controlButtonArea != 'undefined') {
                        var buttons = controlButtonArea.childElements();
                        for (var i = 0; i < buttons.length; i++) {
                            if (buttons[i].innerHTML.include(button.label)) {
                                return ;
                            }
                        }
                        button.insertIn(controlButtonArea, 'top');
                    }
                }
            });

            var searchButton = new ControlButton(Translator.translate('Add Products')),
                searchAreaId = this.getAreaId('search');
            searchButton.onClick = function() {
                $(searchAreaId).show();
                var el = this;
                window.setTimeout(function () {
                    el.remove();
                }, 10);
            }

            this.dataArea.onLoad = this.dataArea.onLoad.wrap(function(proceed) {
                proceed();
                this._parent.itemsArea.setNode($(this._parent.getAreaId('items')));
                this._parent.itemsArea.onLoad();
            });

            this.itemsArea.onLoad = this.itemsArea.onLoad.wrap(function(proceed) {
                proceed();
                if (!$(searchAreaId).visible()) {
                    this.addControlButton(searchButton);
                }
            });
            this.areasLoaded();
            this.itemsArea.onLoad();
        }).bind(this));
    },

    setCustomerAfter : function () {

        this.customerSelectorHide();
        if (this.warehouseId && this.storeId) {
            $(this.getAreaId('data')).callback = 'dataLoaded';
            this.loadArea(['data'], true);
        }

        else if(!this.warehouseId && !this.storeId) {
            this.warehouseSelectorShow();
            this.dataHide();
        }
        else if(this.warehouseId) {
            this.storeSelectorShow();
            this.dataHide();
        }
    },

    hoho : function() {
        alert('dsdsd');
    },

    warehouseSelectorHide : function(){
        this.hideArea('warehouse-selector');
    },

    warehouseSelectorShow : function(){
        this.showArea('warehouse-selector');
    },

    setWarehouseId : function(id){
        this.warehouseId = id;
        this.warehouseSelectorHide();
        this.storeSelectorShow();
        this.loadArea(['header'], true);
    },

    prepareParams : function(params){
        if (!params) {
            params = {};
        }
        if (!params.customer_id) {
            params.customer_id = this.customerId;
        }
        if (!params.store_id) {
            params.store_id = this.storeId;
        }
        if (!params.warehouse_id) {
            params.warehouse_id = this.warehouseId;
        }
        if (!params.currency_id) {
            params.currency_id = this.currencyId;
        }
        if (!params.form_key) {
            params.form_key = FORM_KEY;
        }
        var data = this.serializeData('order-billing_method');
        if (data) {
            data.each(function(value) {
                params[value[0]] = value[1];
            });
        }
        return params;
    }

});