jQuery(document).ready(function(){
    jQuery('.itext').click(function(){
        jQuery(this).hide();
        jQuery(this).parent().children('.editable').show();
        jQuery(this).parent().children('.editable').focus();
        jQuery(this).parent().children('.editable').change(function(){
            jQuery(this).parent().children('span').html(jQuery(this).val());
        });
    })

    jQuery('.editable').blur(function(){
        jQuery(this).parent().children('.itext').show();
        jQuery(this).parent().children('.editable').hide();
        addElement(jQuery(this).clone().hide());
    })

});



