jQuery17 = $.noConflict();



jQuery17(document).ready(function() {
	var ww = document.body.clientWidth;

	jQuery17(".nav li a").each(function() {
		if (jQuery17(this).next().length > 0) {
			jQuery17(this).addClass("parent");
		};
	})
	
	jQuery17(".toggleMenu").click(function(e) {
		e.preventDefault();
		jQuery17(this).toggleClass("active");
		jQuery17(".nav").toggle();
	});
	adjustMenu();
})

jQuery17(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});

var adjustMenu = function() {
	var ww = document.body.clientWidth;
	if (ww < 820) {
		jQuery17(".toggleMenu").css("display", "inline-block");
		if (!jQuery17(".toggleMenu").hasClass("active")) {
			jQuery17(".nav").hide();
		} else {
			jQuery17(".nav").show();
		}
		jQuery17(".nav li").unbind('mouseenter mouseleave');
		jQuery17(".nav li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			jQuery17(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 821) {
		jQuery17(".toggleMenu").css("display", "none");
		jQuery17(".nav").show();
		jQuery17(".nav li").removeClass("hover");
		jQuery17(".nav li a").unbind('click');
		jQuery17(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	jQuery17(this).toggleClass('hover');
		});
	}
}

