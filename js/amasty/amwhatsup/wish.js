
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2010-2013 Amasty (http://www.amasty.com)
 * @package Amasty_Social
 */
amastyWhatsupWish = {
    timeout: "",
    after: "",
    position : "",
    create: function(isCheckout, url, after, timeout, activity,position) {
        amastyWhatsupWish.timeout = timeout;
        amastyWhatsupWish.after = after;
        amastyWhatsupWish.position = position;
        if ((activity.general == 0) && (activity.wishlist == 1) && (isCheckout == false))
        {
            amastyWhatsupWish.getWish(url);

        }

    },
    getWish: function(url) {
        new Ajax.Request(url, {
            method: 'post',
            onSuccess: function(data) {
                if (data) {
                    var response = eval('(' + data.responseText + ')');
                }
                var timeout = amastyWhatsupWish.after * 1000;
                setTimeout(function() {
                    var noty_id = noty({
                        text: response,
                        layout: amastyWhatsupWish.position,
                        timeout: amastyWhatsupWish.timeout * 1000,
                    }

                    )
                },
                        timeout
                        );
            }}
        );
    }



}


