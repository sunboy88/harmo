
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2010-2013 Amasty (http://www.amasty.com)
 * @package Amasty_Social
 */
amastyWhatsup = {
    timeout: false,
    scroll: false,
    after: false,
    alreadyNoty: false,
    timeoutObj: false,
    position: false, 
    
    create: function(
            after,
            scroll,
            timeout,
            category,
            lastvievedUrlForCategory,
            lastProductUrlForCategory,
            lastProductSaleUrl,
            ProductSalesCount,
            checkActivity,
            position
            ) {
        amastyWhatsup.after = after;
        amastyWhatsup.scroll = scroll;
        amastyWhatsup.timeout = timeout;
        amastyWhatsup.position = position;
        if (checkActivity.general == 1) {
            return false;
        }
        var category_id = category;
        if (category_id) {
            if (checkActivity.lastInCategory == 1 && checkActivity.lastVievedInCategory == 1) {
                var num = Math.floor((Math.random() * 10) + 1);
                if (num <= 5) {
                    amastyWhatsup.getLastSoldInCategoryProduct(category_id, lastProductUrlForCategory, true);
                }
                else {
                    amastyWhatsup.getLastVievedInCategory(category_id, lastvievedUrlForCategory, true);
                }
            }
            if (checkActivity.lastInCategory == 1 && checkActivity.lastVievedInCategory == 0) {
                amastyWhatsup.getLastSoldInCategoryProduct(category_id, lastProductUrlForCategory, true);
            }
            if (checkActivity.lastInCategory == 0 && checkActivity.lastVievedInCategory == 1) {
                amastyWhatsup.getLastVievedInCategory(category_id, lastvievedUrlForCategory, true);
            }
        }
        var product_id = amastyWhatsup.getProductId();
        if (product_id) {
            if ((checkActivity.lastPurchased == 1) && (checkActivity.lastInPeriod == 1)) {
                var num = Math.floor((Math.random() * 10) + 1);
                if (num <= 5) {
                    amastyWhatsup.getLastProductSale(lastProductSaleUrl, product_id, true);
                }
                else {
                    amastyWhatsup.getProductSalesCount(ProductSalesCount, product_id, true);
                }
            }
            if ((checkActivity.lastPurchased == 1) && (checkActivity.lastInPeriod == 0)) {
                amastyWhatsup.getLastProductSale(lastProductSaleUrl, product_id, true);
            }
            if ((checkActivity.lastPurchased == 0) && (checkActivity.lastInPeriod == 1)) {
                amastyWhatsup.getProductSalesCount(ProductSalesCount, product_id, true);
            }

            Event.observe(window, 'scroll', function() {
                if (amastyWhatsup.scroll != 0) {
                    var height = window.document.documentElement.clientHeight;
                    var scroll = document.documentElement.scrollTop;
                    var procent = (height / 100) * amastyWhatsup.scroll;
                    if (scroll > procent) {

                        if ((checkActivity.lastPurchased == 1) && (checkActivity.lastInPeriod == 1)) {
                            var num = Math.floor((Math.random() * 10) + 1);
                            if (num <= 5) {
                                amastyWhatsup.getLastProductSale(lastProductSaleUrl, product_id, true);
                            }
                            else {
                                amastyWhatsup.getProductSalesCount(ProductSalesCount, product_id, true);
                            }
                        }
                        if ((checkActivity.lastPurchased == 1) && (checkActivity.lastInPeriod == 0)) {
                            amastyWhatsup.getLastProductSale(lastProductSaleUrl, product_id, true);
                        }
                        if ((checkActivity.lastPurchased == 0) && (checkActivity.lastInPeriod == 1)) {

                            amastyWhatsup.getProductSalesCount(ProductSalesCount, product_id, true);
                        }
                        jQuery(window).unbind("scroll");
                        Event.stopObserving(window, "scroll");
                    }
                }
            });
        }
    },
    getProductSalesCount:
            function(url, product_id, delay) {

                new Ajax.Request(url, {
                    method: 'post',
                    parameters: {product_id: product_id},
                    onSuccess: function(data) {
                        amastyWhatsup.notyMessageTimeout(data.responseText, delay);
                    }
                });
            },
    getLastProductSale:
            function(url, product_id, delay) {
                new Ajax.Request(url, {
                    method: 'post',
                    parameters: {product_id: product_id},
                    onSuccess: function(data) {
                        amastyWhatsup.notyMessageTimeout(data.responseText, delay);
                    }
                });
            },
    getProductId:
            function() {
                var hidden = $$('input[type=hidden][name=product]').first()
                if (hidden) {
                    var product_id = hidden.value;
                    return product_id;
                }
                else
                    return false;
            },
    getLastSoldInCategoryProduct:
            function(id, url, delay) {

                new Ajax.Request(url, {
                    method: 'post',
                    parameters: {category_id: id},
                    onSuccess: function(data) {
                        amastyWhatsup.notyMessageTimeout(data.responseText, delay);
                    }
                });
            },
    getLastVievedInCategory: function(id, url, delay) {
        new Ajax.Request(url, {
            method: 'post',
            parameters: {category_id: id},
            onSuccess: function(data) {
                amastyWhatsup.notyMessageTimeout(data.responseText, delay);
            }
        });


    }
    ,
    notyMessage:
            function(data) {
                if (data && !amastyWhatsup.alreadyNoty) {
                    var noty_id = noty({
                        text: data,
                        layout: amastyWhatsup.position,
                        timeout: amastyWhatsup.timeout * 1000,
                        onShow: function() {
                            amastyWhatsup.alreadyNoty = true;
                            clearTimeout(amastyWhatsup.timeoutObj);
                        },
                    });
                }
            },
    notyMessageTimeout:
            function(data, delay) {
                if (data) {
                    var data = eval('(' + data + ')');
                }
                if (data && !amastyWhatsup.alreadyNoty) {
                    if (!delay) {
                        amastyWhatsup.notyMessage(data);
                        return false;
                    }
                    var timeout = amastyWhatsup.after * 1000;
                    amastyWhatsup.timeoutObj = setTimeout(function() {
                        var noty_id = noty({
                            text: data,
                            layout: amastyWhatsup.position,
                            timeout: amastyWhatsup.timeout * 1000,
                            callback: {
                                onShow: function() {
                                    amastyWhatsup.alreadyNoty = true;
                                },
                            }
                        });
                    },
                            timeout);
                }
            }

};



