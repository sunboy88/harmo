
amastyWhatsupBlock = {
    activityUrl: "",
    maxdateTime: false,
    create: function(activityUrl) {
        amastyWhatsupBlock.activityUrl = activityUrl;
        new Ajax.Request(activityUrl, {
            method: 'post',
            onSuccess: function(data) {
                amastyWhatsupBlock.updateBlock(data.responseText, false);

                if (amastyWhatsupBlock.dateTime < data.dateTime) {
                    amastyWhatsupBlock.dateTime = data.dateTime;

                }
                setInterval(function() {
                    amastyWhatsupBlock.checkActivity(amastyWhatsupBlock.activityUrl);
                }, 10000);
            }
        });
    },
    updateBlock: function(data, needToDelete) {
        if (data) {
            var data = eval('(' + data + ')');
        }
        var flag = false;

        jQuery.each(data, function(key, value) {
            flag = true;
            var message = value.message;
            if (value.dateTime > amastyWhatsupBlock.maxdateTime)
            {
                amastyWhatsupBlock.maxdateTime = value.dateTime;
            }

            if(value.message !== undefined ) {
            if (needToDelete) {

                jQuery(".block-active > .block-content > ol > li").last().remove();
                var string = "<li style='list-style:none;display:none;padding-left:10px;padding-top:5px;padding-bottom:5px;'>" + message + "</li>";
                jQuery('.block-active > .block-content > ol').prepend(string);
            }
             else {
                 var string = "<li style='list-style:none;display:none;padding-left:10px;padding-top:5px;padding-bottom:5px;'>" + message + "</li>";
                 jQuery('.block-active > .block-content > ol').append(string);
             }
           
            if ((jQuery(".block-active > .block-content > ol > li").length % 2) == 1) {
                jQuery(".block-active > .block-content > ol > li").first().addClass("odd");
            }
            else {
                jQuery(".block-active > .block-content > ol > li").first().addClass("even");
            }

            }});
        if (flag) {
            jQuery(".block-active > .block-content > ol > li").slideDown(2000);
        }
    },
    checkActivity: function(activityUrl) {
        new Ajax.Request(activityUrl, {
            method: 'post',
            parameters: {dateTime: amastyWhatsupBlock.maxdateTime},
            onSuccess: function(data) {
                amastyWhatsupBlock.updateBlock(data.responseText, true)
            }
        });



    }
}






