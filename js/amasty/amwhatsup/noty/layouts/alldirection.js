
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2010-2013 Amasty (http://www.amasty.com)
 * @package Amasty_Social
 */
amastyWhatsupWish = {
    timeout: "",
    after: "",
    position : "",
    create: function(isCheckout, url, after, timeout, activity,position) {
        amastyWhatsupWish.timeout = timeout;
        amastyWhatsupWish.after = after;
        amastyWhatsupWish.position = position;
        if ((activity.general == 0) && (activity.wishlist == 1) && (isCheckout == false))
        {
            amastyWhatsupWish.getWish(url);

        }

    },
    getWish: function(url) {
        new Ajax.Request(url, {
            method: 'post',
            onSuccess: function(data) {
                if (data) {
                    var response = eval('(' + data.responseText + ')');
                }
                var timeout = amastyWhatsupWish.after * 1000;
                setTimeout(function() {
                    var noty_id = noty({
                        text: response,
                        layout: amastyWhatsupWish.position,
                        timeout: amastyWhatsupWish.timeout * 1000,
                    }

                    )
                },
                        timeout
                        );
            }}
        );
    }



}
;(function($) {

	$.noty.layouts.bottomCenter = {
		name: 'bottomCenter',
		options: { // overrides options

		},
		container: {
			object: '<ul id="noty_bottomCenter_layout_container" />',
			selector: 'ul#noty_bottomCenter_layout_container',
			style: function() {
				$(this).css({
					bottom: 20,
					left: 0,
					position: 'fixed',
					width: '310px',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 10000000
				});

				$(this).css({
					left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px'
				});
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none',
			width: '310px'
		},
		addClass: ''
	};

})(jQuery);
;(function($) {

	$.noty.layouts.bottomLeft = {
		name: 'bottomLeft',
		options: { // overrides options
			
		},
		container: {
			object: '<ul id="noty_bottomLeft_layout_container" />',
			selector: 'ul#noty_bottomLeft_layout_container',
			style: function() {
				$(this).css({
					bottom: 20,
					left: 20,
					position: 'fixed',
					width: '310px',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 10000000
				});

				if (window.innerWidth < 600) {
					$(this).css({
						left: 5
					});
				}
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none',
			width: '310px'
		},
		addClass: ''
	};

})(jQuery);
;(function($) {

	$.noty.layouts.bottomRight = {
		name: 'bottomRight',
		options: { // overrides options
			
		},
		container: {
			object: '<ul id="noty_bottomRight_layout_container" />',
			selector: 'ul#noty_bottomRight_layout_container',
			style: function() {
				$(this).css({
					bottom: 65,
					right: 20,
					position: 'fixed',
					width: '310px',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 10000000
				});

				if (window.innerWidth < 600) {
					$(this).css({
						right: 5
					});
				}
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none',
			width: '310px'
		},
		addClass: ''
	};

})(jQuery);
;(function($) {

	$.noty.layouts.topCenter = {
		name: 'topCenter',
		options: { // overrides options

		},
		container: {
			object: '<ul id="noty_topCenter_layout_container" />',
			selector: 'ul#noty_topCenter_layout_container',
			style: function() {
				$(this).css({
					top: 20,
					left: 0,
					position: 'fixed',
					width: '310px',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 10000000
				});

				$(this).css({
					left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px'
				});
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none',
			width: '310px'
		},
		addClass: ''
	};

})(jQuery);
;(function($) {

	$.noty.layouts.topLeft = {
		name: 'topLeft',
		options: { // overrides options
			
		},
		container: {
			object: '<ul id="noty_topLeft_layout_container" />',
			selector: 'ul#noty_topLeft_layout_container',
			style: function() {
				$(this).css({
					top: 20,
					left: 20,
					position: 'fixed',
					width: '310px',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 10000000
				});

				if (window.innerWidth < 600) {
					$(this).css({
						left: 5
					});
				}
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none',
			width: '310px'
		},
		addClass: ''
	};

})(jQuery);
;(function($) {

	$.noty.layouts.topRight = {
		name: 'topRight',
		options: { // overrides options
			
		},
		container: {
			object: '<ul id="noty_topRight_layout_container" />',
			selector: 'ul#noty_topRight_layout_container',
			style: function() {
				$(this).css({
					top: 20,
					right: 20,
					position: 'fixed',
					width: '310px',
					height: 'auto',
					margin: 0,
					padding: 0,
					listStyleType: 'none',
					zIndex: 10000000
				});

				if (window.innerWidth < 600) {
					$(this).css({
						right: 5
					});
				}
			}
		},
		parent: {
			object: '<li />',
			selector: 'li',
			css: {}
		},
		css: {
			display: 'none',
			width: '310px'
		},
		addClass: ''
	};

})(jQuery);