(function($jmt) {
	$jmt.fn.capslide = function(options) {
		var opts = $jmt.extend({}, $jmt.fn.capslide.defaults, options);
		return this.each(function() {
			$jmtthis = $jmt(this);
			var o = $jmt.meta ? $jmt.extend({}, opts, $jmtthis.data()) : opts;
			
			if(!o.showcaption)	$jmtthis.find('.ic_caption').css('display','none');
			$jmtthis.find('.product-name').css('display','none');
				
			var _img = $jmtthis.find('img:first');
			var w = _img.css('width');
			var h = _img.css('height');
			$jmt('.ic_caption',$jmtthis).css({'color':o.caption_color,'background-color':o.caption_bgcolor,'bottom':'0px','width':w});
			$jmt('.product-name',$jmtthis).css({'color':o.caption_color,'background-color':o.caption_bgcolor,'top':'0px','width':w});
			$jmt('.overlay',$jmtthis).css('background-color',o.overlay_bgcolor);
			$jmtthis.css({'width':w , 'height':h, 'border':o.border});
			$jmtthis.hover(
				function () {
					if((navigator.appVersion).indexOf('MSIE 7.0') > 0)
					$jmt('.overlay',$jmt(this)).show();
					else
					$jmt('.overlay',$jmt(this)).fadeIn();
					if(!o.showcaption)
						$jmt(this).find('.ic_caption').slideDown(500);
					
						$jmt('.product-name',$jmt(this)).slideDown(500);	
				},
				function () {
					if((navigator.appVersion).indexOf('MSIE 7.0') > 0)
					$jmt('.overlay',$jmt(this)).hide();
					else
					$jmt('.overlay',$jmt(this)).fadeOut();
					if(!o.showcaption)
						$jmt(this).find('.ic_caption').slideUp(200);
					
						$jmt('.product-name',$jmt(this)).slideUp(200);
				}
			);
		});
	};
	$jmt.fn.capslide.defaults = {
		caption_color	: 'white',
		caption_bgcolor	: '#fff',
		overlay_bgcolor : 'blue',
		border		: '1px solid #fff',
		showcaption	: true
	};
})(jQuery);