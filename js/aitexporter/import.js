
/**
 * Orders Export and Import - 10.06.13
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitexporter
 * @version      1.1.7
 * @license:     n/a
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
Event.observe(document, 'dom:loaded', function()
{
    function toggleParseType()
    {
        switch ($('parse_type').value)
        {
            case 'csv':
                $('parse_delimiter').parentNode.parentNode.show();
                $('parse_enclose').parentNode.parentNode.show();
                break;

            case 'xml':
            default:
                $('parse_delimiter').parentNode.parentNode.hide();
                $('parse_enclose').parentNode.parentNode.hide();
                break;
        }
    }

    Event.observe($('parse_type'), 'change', toggleParseType);
    toggleParseType();
});