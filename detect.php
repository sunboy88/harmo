<?php

define("GEOIP_DAT_FILE", MAGENTO_ROOT."/geoip/GeoIP.dat");
define("GEOIP_INC_FILE", MAGENTO_ROOT."/geoip/geoip.inc");
define("GEOIP_STORE_CONFIG",MAGENTO_ROOT."/geoip/config.php");
function getRedirect($code)
{

	require_once(GEOIP_STORE_CONFIG);
	
	if(isset($redirect[$code]))
	{
		return $redirect[$code];
	}
	return false;
}
function getClientIP(){
/*
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    //$remote  = $_SERVER['REMOTE_ADDR'];
    //$remote= "2.15.255.255"; //FR
		$remote= "118.70.67.134"; //FR

    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }else{
        $ip = $remote;
    }
		*/
		
		$ip = '';
		
	if (getenv('HTTP_CLIENT_IP'))
      $ip = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
      $ip = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
      $ip = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
      $ip = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
      $ip = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
      $ip = getenv('REMOTE_ADDR');
    else
      $ip = '2.15.255.255'; //FR

    return $ip;
}
function detectStore(){
	include(GEOIP_INC_FILE);
	$config=array(
		'redirect'=>false,
		'redirect_url'=>'',
		'mageRunCode'=>'',
		'mageRunType'=>''
	);
	$client_ip = getClientIP();

	$strTrackLog = '';
	// LOG ===============================================================
	$strTrackLog = $strTrackLog. "[Client IP: ".$client_ip."]-";
	// ===================================================================
	
	$_geoip = geoip_open(GEOIP_DAT_FILE ,GEOIP_STANDARD);
	$countryCode = geoip_country_code_by_addr($_geoip, $client_ip);
	geoip_close($_geoip);
	

	// LOG ===============================================================
	$strTrackLog = $strTrackLog."[CountryCode: ".$countryCode."]-";
	// ===================================================================
	
	
	$uri = $_SERVER['REQUEST_URI'];
	// LOG ===============================================================
	$strTrackLog = $strTrackLog."[URI header: ".$uri."]-";
	// ===================================================================

	if(strpos($uri,'admin')===false && strpos($uri, 'api')===false
	     && strpos($uri, 'ipn')===false && strpos($uri, 'paybox/system/notify')===false
	     && strpos($uri, 'checkout/onepage/success')===false
	     && strpos($uri, 'checkout/onepage')===false
	     )
	{
		if($countryCode)
		{
			$redirect = getRedirect($countryCode);
			

			// LOG ===============================================================
			$strTrackLog = $strTrackLog."[GEO Redirect Store View: ".$redirect['store']."]-";
			$strTrackLog = $strTrackLog."[GEO Redirect URL: ".$redirect['url']."]-";
			// ===================================================================
	
			if($redirect)
			{
				$host = strtolower($_SERVER['HTTP_HOST']);
				
				// LOG ===============================================================
				$strTrackLog = $strTrackLog."[HOST: ".$host."]-";
				// ===================================================================

				$stores = Mage::app()->getStores(false,true);
				$store=null;
				$url = '';
				$url_compare=false;
				
				if(isset($stores[$redirect['store']]))
				{
					
					$store = $stores[$redirect['store']];
					$url=$store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
					$url_compare=array();
					preg_match('@^(?:http://)?([^/]+)@i',$url, $url_compare);
					$url_compare=strtolower($url_compare[0]);
					$url_compare=str_replace('http://', '', $url_compare);			

					// LOG ===============================================================
					$strTrackLog = $strTrackLog."[Store URL: ".$url."]-";
					$strTrackLog = $strTrackLog."[URL Compare: ".$url_compare."]-";
					// ===================================================================
				}
				
				//language redirection ----- NEVER TRUE because has not set in config file  
				if(isset($redirect['language']) ){

					// LOG ===============================================================
					$strTrackLog = $strTrackLog."[Redirect Language: ".$redirect['language']."]-";
					// ===================================================================
					
					$url = $url_compare = $redirect['url']."?language=".$redirect['language'];//Mage::getUrl('*/*', array('language' =>$redirect['language'] ));
					$url_compare=str_replace('http://', '', $url_compare);

				}
				
				if($url_compare)
				{
					if(strcmp($host, $url_compare)!=0)
					{
						$config['redirect']=true;
						$config['redirect_url']=$url;

						// LOG ===============================================================
						$strTrackLog = $strTrackLog."[ HOST = GEO URL Config]-";
						// ===================================================================
						
					}else{
						$config['mageRunCode']=$redirect['store'];
						//$mageRunCode = $redirect['store'];
						$config['mageRunType']='store';
						//$mageRunType='store'; 

						// LOG ===============================================================
						$strTrackLog = $strTrackLog."[FINAL ===== NOT REDIRECT, OPEN BY GEO STORE CONFIG:".$config['mageRunCode']."]-";
						// ===================================================================
					}
				}
			}
		}

		if($_GET['test']==1){
				print_r($config);die;
			}

		//deepak for language redirection
		if(Mage::app()->getRequest()->getParam('language') || Mage::getModel("core/cookie")->get("my_lang")){
			$config['redirect']=false;
		}
		if(isset($redirect['us'])){
			$config['redirect']=false;
		}
		
		// LOG ===============================================================
		$strTrackLog = $strTrackLog."[Config Allow Redirect: ".$config['redirect']."]";
		// ===================================================================

		// LOG ===============================================================
		//Mage::log("=========================================================================");
		Mage::log($strTrackLog, null, 'redirect.log');
		//Mage::log("=========================================================================");
		// ===================================================================

		return $config;
	}else{
		
		// LOG ===============================================================
		//Mage::log("=========================================================================");
		Mage::log("GEOIP FALSE with [Client IP: ".$client_ip."]-[URI=".$uri,"]", null, 'redirect.log');
		//Mage::log("=========================================================================");
		// ===================================================================
		
		return false;
	}
}
?>